#ifndef RD53A_SOURCESCAN_H
#define RD53A_SOURCESCAN_H

#include "RD53Emulator/Handler.h"
#include "TH2I.h"

namespace RD53A{

  /**
   * A Source scan is designed to see hits from a source.
   * Many random triggers are sent with the hope to see a hit from the source.
   *
   * @brief RD53A SourceScan
   * @author Brian.Moser@cern.ch
   * @date August 2022
   */

  class SourceScan: public Handler{

  public:

    /**
     * Create the scan
     */
    SourceScan();

    /**
     * Delete the Handler
     */
    virtual ~SourceScan();

    /**
     * Set the value of the variables for this scan, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms. 
     */
    virtual void Analysis();

    /**
     * Method to call the stop of the scan from inside the loop
     * @param signal the signal thrown by the signal catch
     */
    static void StopScan(int32_t signal);

  private:

    static bool m_scanStop;

    float m_TriggerFrequency;
    uint32_t m_TriggerLatency;
    double m_frequency;
    double m_duration;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_enable;
    std::map<std::string,TH2I*> m_mask;
    std::map<std::string,TH1I*> m_tot;
    TH1I* m_triggers;
    
    uint32_t m_colMin;
    uint32_t m_colMax;

  };

}

#endif
