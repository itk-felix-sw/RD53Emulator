#ifndef RD53A_HEADERFRAME_H
#define RD53A_HEADERFRAME_H

#include "RD53Emulator/Frame.h"

namespace RD53A{

/**
 * This class represents an RD53A 32-bit header sub-frame.
 * The format is the following:
 *
 * | Bit   | 31 | 25 | 24 | 23 | 20 | 19 | 16 | 15 | 14 | 8 | 7 | 0 |
 * | ----  | -- | -- | -- | -- | -- | -- | -- | -- | -- | - | - | - |
 * | Rel   | 7  | 1  | 0  | 7  | 4  | 3  | 0  | 7  | 6  | 0 | 7 | 0 |
 * | Value | 0x01   || Trig ID    ||| Trig Tag   ||| BCID        ||||
 * | Size  | 7      || 5          ||| 5          ||| 15          ||||
 * | Byte  | 1          |||  2             |||| 3         ||| 4    ||
 *
 * A HeaderFrame can be added to the Decoder, and can be extracted from the Decoder in 32-bit mode.
 * 
 * @brief RD53A HeaderFrame
 * @author Carlos.Solans@cern.ch
 * @date June 2021
 **/

class HeaderFrame: public Frame{
  
 public:

  /**
   * Create an empty header frame
   **/
  HeaderFrame();

  /**
   * Create a header frame with the given details
   * @param tid 5-bit trigger ID
   * @param ttag 5-bit trigger tag
   * @param bcid 15-bit BCID
   **/
  HeaderFrame(uint32_t tid, uint32_t ttag, uint32_t bcid);

  /**
   * Delete the frame
   **/
  ~HeaderFrame();

  /**
   * Return a human readable representation of the data
   * @return The human readable representation of the data
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);
  
  /**
   * Get the type of Frame
   * @return the type of the Frame
   **/
  uint32_t GetType();

  /**
   * Define the header at once
   * @param triggerID the Trigger ID
   * @param triggerTag the Trigger Tag
   * @param BCID the Bunch Crossing ID
   **/
  void SetHeader(uint32_t triggerID, uint32_t triggerTag, uint32_t BCID);

  /**
   * Set the Trigger ID
   * @param triggerID the Trigger ID
   **/
  void SetTID(uint32_t triggerID);
  
  /**
   * Set the Trigger Tag
   * @param triggerTag the Trigger Tag
   **/
  void SetTTag(uint32_t triggerTag);
  
  /**
   * Set the BCID
   * @param BCID the BCID
   **/
  void SetBCID(uint32_t BCID);

  /**
   * Get the Trigger ID of a header in the Frame
   * @return the Trigger ID
   **/
  uint32_t GetTID();
  
  /**
   * Get the Trigger Tag of a header in the Frame
   * @return the Trigger Tag
   **/
  uint32_t GetTTag();
  
  /**
   * Get the BCID tag in the header
   * @return the BCID
   **/
  uint32_t GetBCID();

 private:
 
  uint32_t m_TID; //<< Trigger ID
  uint32_t m_TTag; //<< Trigger Tag
  uint32_t m_BCID; //<< Bunch Crossing ID

};

}

#endif 
