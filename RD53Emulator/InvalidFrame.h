#ifndef RD53A_INVALIDFRAME_H
#define RD53A_INVALIDFRAME_H

#include "RD53Emulator/Frame.h"

namespace RD53A{

/**
 * This class represents an invalid 32-bit RD53A frame.
 * @brief RD53A InvalidFrame
 * @author Carlos.Solans@cern.ch
 * @date June 2021
 **/

class InvalidFrame: public Frame{
  
 public:

  /**
   * Create an empty InvalidFrame.
   * All values are set to zero.
   **/
  InvalidFrame();

  /**
   * Delete the InvalidFrame.
   * Actually do nothing.
   **/
  ~InvalidFrame();

  /**
   * Return a human readable representation of the frame
   * @return The human readable representation of the frame
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this frame from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this frame to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);
  
  /**
   * Get the type of Frame
   * @return the type of the Frame
   **/
  uint32_t GetType();

 private:
 
  uint32_t m_bytes[4]; //<< The bytes of the frame

};

}

#endif 
