#ifndef RD53A_MASKCHECK_H
#define RD53A_MASKCHECK_H

#include "RD53Emulator/Handler.h"
#include "TH2I.h"
namespace RD53A{

  /**
   * This scan is used to check the loop over masks and core columns. 
   * This could be useful in order to verify that the changes made do not
   * alter these loops. 
   * It performs a digital scan and creates an occupancy map for each loop
   * over masks and core columns. 
   * In each loop only a subset of pixels should be activated and hence 
   * should register a hit, according to FrontEnd::SetMask and
   *  FrontEnd::SetCoreColumn.
   *
   * @brief RD53A MaskCheck
   * @author Alessandra.Palazzo@cern.ch
   * @date January 2021
   */

  class MaskCheck: public Handler{

  public:

    /**
     * Create the scan
     */
    MaskCheck();

    /**
     * Delete the Handler
     */
    virtual ~MaskCheck();

    /**
     * Set the value of the variables needed for this scan, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger and per loop.
     * An occupancy histogram is filled per loop over mask/core column, 
     * that is required to have the same amount of entries or less than the
     * number of triggers.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

  private:
    uint32_t m_mask_step;
    uint32_t m_cc_max;
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    TH2I ***m_enableMap;
  };

}

#endif
