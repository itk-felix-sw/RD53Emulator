#ifndef RD53A_CONFIGSCAN_H
#define RD53A_CONFIGSCAN_H

#include "RD53Emulator/Handler.h"
#include "TH2I.h"

namespace RD53A{

  /**
   * Only configure the front-ends
   *
   * @brief RD53A ConfigScan
   * @author Carlos.Solans@cern.ch
   * @date June 2021
   */

  class ConfigScan: public Handler{

  public:

    /**
     * Create the scan
     */
    ConfigScan();

    /**
     * Delete the scan
     */
    virtual ~ConfigScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

	std::map<std::string, uint32_t> registerMap;

  private:

  };

}

#endif
