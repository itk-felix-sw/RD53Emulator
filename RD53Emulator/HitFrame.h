#ifndef RD53A_HITFRAME_H
#define RD53A_HITFRAME_H

#include "RD53Emulator/Frame.h"

namespace RD53A{

/**
 * This class represents an RD53A 32-bit data.
 * The hit format is the following
 *
 * | Bit   | 31 | 26   | 25 | 24 | 23 | 20 | 19 | 16      | 15 | 12 | 11 | 8 | 7 | 4 | 3 | 0 |
 * | ----  | -- | --   | -- | -- | -- | -- | -- | --      | -- | -- | -- | - | - | - | - | - |
 * | Rel   | 7  | 2    | 1  | 0  | 7  | 4  |  3 |  0      | 7  | 4  | 3  | 0 | 7 | 4 | 3 | 0 |
 * | Value | Core Col || Core row       |||| Core region || Tot1   || Tot2  || Tot3 || Tot4 ||
 * | Size  | 6        || 6              |||| 4           || 4      || 4     || 4    || 4    ||
 * | Byte  | 1                ||||  2                  |||| 3             |||| 4          ||||
 *
 * The row and column can be decoded from the core_row, core_column,
 * and core_region like the following:
 *
 * @verbatim
    row = core_row[5:0]*8 + core_region[3:1]
    col = core_col[5:0]*8 + core_region[0]*4
   @endverbatim
 * or
 * @verbatim
    row = (core_row << 3) |  (core_region >> 1)
    col = (core_col << 3) | ((core_region & 0x1) << 2)
   @endverbatim
 *
 * The core_row, core_column, and the core_region can be
 * encoded from the row and column like the following:
 * 
 * @verbatim
   core_row = row[8:3]
   core_col = col[8:3]
   core_region = row[2:0]*2 + col[2]
   @endverbatim
 * or
 * @verbatim
   core_row = row>>3
   core_col = col>>3
   core_region = ((row & 0x7) << 1) | ((col >> 2) & 0x1)
   @endverbatim
 *
 * @brief RD53A HitFrame
 * @author Carlos.Solans@cern.ch
 * @date June 2021
 **/

class HitFrame: public Frame{
  
 public:

  /**
   * Create an empty HitFrame.
   * All values are set to zero.
   **/
  HitFrame();

  /**
   * Create a hit Frame with the given details.
   * @param core_col core column
   * @param core_row core row
   * @param core_region region in core
   * @param tot1 the tot value for pixel 1
   * @param tot2 the tot value for pixel 2
   * @param tot3 the tot value for pixel 3
   * @param tot4 the tot value for pixel 4
   **/
  HitFrame(uint32_t core_col, uint32_t core_row, uint32_t core_region, uint32_t tot1, uint32_t tot2, uint32_t tot3, uint32_t tot4);

  /**
   * Delete the HitFrame.
   * Actually do nothing.
   **/
  ~HitFrame();

  /**
   * Return a human readable representation of the hit
   * @return The human readable representation of the hit
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);
  
  /**
   * Get the type of Frame
   * @return the type of the Frame
   **/
  uint32_t GetType();

  /**
   * Add a hit to the Frame in a given position.
   * @param core_col core column
   * @param core_row core row
   * @param core_region region in core
   * @param tot1 the tot value for pixel 1
   * @param tot2 the tot value for pixel 2
   * @param tot3 the tot value for pixel 3
   * @param tot4 the tot value for pixel 4
   **/
  void SetHit(uint32_t core_col, uint32_t core_row, uint32_t core_region, uint32_t tot1, uint32_t tot2, uint32_t tot3, uint32_t tot4);

  /**
   * Set the hit data
   * @param quad_col quad column (0 to 99)
   * @param row row (0 to 191)
   * @param tot array of tot values for the 4 pixels
   **/
  void SetHit(uint32_t quad_col, uint32_t row, uint32_t * tot);

  /**
   * Set the core column of the Hit
   * @param core_col the core column (0 to 49)
   **/
  void SetCoreCol (uint32_t core_col);
  
  /**
   * Set the core row of the Hit
   * @param core_row the core row (0 to 23)
   **/
  void SetCoreRow (uint32_t core_row);
  
  /**
   * Set the core region of the Hit
   * @param core_reg the core region (0 to 15)
   **/
  void SetCoreReg (uint32_t core_reg);

  /**
   * Set the TOT with given idx (0,1,2,3) of the Hit
   * @param idx TOT index (0,1,2,3)
   * @param tot the 4-bit TOT value (0 to 15)
   **/
  void SetTOT(uint32_t idx, uint32_t tot);

  /**
   * Get the core column of the Hit
   * @return the core column (0 to 49)
   **/
  uint32_t GetCoreCol();

  /**
   * Get the core row of the Hit
   * @return the core row (0 to 23)
   **/
  uint32_t GetCoreRow();

  /**
   * Get the core region of the Hit
   * @return the core region (0 to 15)
   **/
  uint32_t GetCoreReg();

  /**
   * Get the TOT with given idx (0,1,2,3) of the Hit
   * @param idx TOT index (0,1,2,3)
   * @return the 4-bit TOT value (0 to 15)
   **/
  uint32_t GetTOT(uint32_t idx);

  /**
   * Get the column of the first TOT of a hit in the Frame
   * by merging the core column, and core region values
   * @return the core column (0 to 399)
   **/
  uint32_t GetCol();

  /**
   * Get the row of a hit in the Frame
   * by merging the core row, and core region values
   * @return the row (0 to 191)
   **/
  uint32_t GetRow();

 private:
 
  uint32_t m_ccol;   //<< 6-bit core column (0 to 49)
  uint32_t m_crow;   //<< 6-bit core row (0 to 23)
  uint32_t m_creg;   //<< 4-bit core region (0 to 15)
  uint32_t m_tot[4]; //<< Time over threshold array of 4

};

}

#endif 
