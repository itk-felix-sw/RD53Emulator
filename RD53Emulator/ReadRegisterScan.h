#ifndef RD53A_READREGISTERSCAN_H
#define RD53A_READREGISTERSCAN_H

#include "RD53Emulator/Handler.h"
#include "RD53Emulator/HitTree.h"
#include "TH2I.h"

namespace RD53A{

  /**
   * 
   * @author Carlos.Solans@cern.ch
   * @author ismet.siral@cern.ch
   * @date November 2022
   */

  class ReadRegisterScan: public Handler{

  public:

    /**
     * Create the scan
     */
    ReadRegisterScan();

    /**
     * Delete the Handler
     */
    virtual ~ReadRegisterScan();

    /**
     * Set the value of the variables for this scan, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled, that is required
     * to have the same amount of entries or less than the number of triggers.
     */
    virtual void Run();

    /**
     * Write and delete histograms. 
     * Create the enable map, in which pixels have value 1 if the number
     * of hits is equal to the number of triggers, 0 otherwise. 
     * Pixels with enable=0 will be masked.
     */
    virtual void Analysis();

  private:
  };

}

#endif
