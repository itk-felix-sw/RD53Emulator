#ifndef RD53A_PIXELTHRESHOLDTUNE_H
#define RD53A_PIXELTHRESHOLDTUNE_H

#include "RD53Emulator/Handler.h"
#include "TH2.h"
#include "TF1.h"
#include "TGraph.h"

namespace RD53A{

  /**
   * 
   * 
   * 
   * @brief RD53A PixelThresholdTune
   * @author Alessandra.Palazzo@cern.ch
   * @date January 2021
   */

  class PixelThresholdTune: public Handler{

  public:

    /**
     * Create the scan
     */
    PixelThresholdTune();

    /**
     * Delete the Handler
     */
    virtual ~PixelThresholdTune();

    /**
     * Set the value of the variables needed for this tuning.
     * Create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the occupancy map per trigger and per
     * frontend.
     * An occupancy map is filled for each value of the DAC.
     */
    virtual void Run();

    /**
     * Delete histograms.
     * Interpolate the values of the local DACs which allow to obtain the
     * desired target and save them in the configuration file.
     */
    virtual void Analysis();

  private:
    uint32_t m_mask_step;
    uint32_t m_cc_max;
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    uint32_t m_chargeMin;
    uint32_t m_chargeMax;
    uint32_t m_chargeStep;
    uint32_t m_nSteps;
    uint32_t m_thresholdTarget;
    std::vector<int> m_chargeVec;
    float m_meanThFit;
    float m_sigmaThFit;
    uint32_t m_colMin;
    uint32_t m_colMax;
    uint32_t m_vcalMed;
    std::string m_whichfe;
    std::map<std::string,std::vector<int>> m_mem_thres;
    std::map<std::string,TH2I**> m_occ;
    std::map<std::string,TGraph****> m_sCurve;
    std::map<std::string,TH1F****> m_histoGaus;
    std::map<std::string,TH2F**> m_meanThresholdHisto;
    std::map<std::string,TH2F**> m_meanThresholdFit;
    std::map<std::string,TH2F**> m_sigmaThresholdHisto;
    std::map<std::string,TH2F**> m_sigmaThresholdFit;
  };

}

#endif
