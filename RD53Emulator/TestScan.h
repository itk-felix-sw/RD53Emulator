#ifndef RD53A_TESTSCAN_H
#define RD53A_TESTSCAN_H

#include "RD53Emulator/Handler.h"
#include "TH2I.h"

namespace RD53A{

  /**
   * This is a test scan. It is used to debug the code.
   *
   * @brief RD53A TestScan
   * @author Carlos.Solans@cern.ch
   * @date November 2020
   */

  class TestScan: public Handler{

  public:

    /**
     * Create the scan
     */
    TestScan();

    /**
     * Delete the scan
     */
    virtual ~TestScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

  private:
    uint32_t m_mask_step;
    uint32_t m_cc_max;
    uint32_t m_ntrigs;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_mask;
    std::map<std::string,TH1I*> m_tid;
    std::map<std::string,TH1I*> m_ttag;
    std::map<std::string,TH1I*> m_bcid;
  };

}

#endif
