#ifndef RD53A_HITTREE_H
#define RD53A_HITTREE_H

#include "RD53Emulator/Hit.h"
#include "TTree.h"
#include <string>
#include <chrono>

namespace RD53A{

/**
 * HitTree is a tool to store RD53A Hit data as a ROOT TTree.
 * A new file can be openned with HitTree::Open.
 * New hits are added with HitTree::Add.
 * After adding the parameters, it is necessary
 * to call MaltaTree::Fill.
 *
 * Hits can be written like the following:
 *
 * @verbatim
   HitTree * ht = new HitTree();
   ht->Open("file.root","RECREATE");

   for(...){
     Hit * hit=...
     ht->Set(hit);
     ht->Fill();
   }

   ht->Close();
   delete ht;
   @endverbatim
   *
   * Hits can be read like the following:
   *
   * @verbatim
   HitTree * ht = new HitTree();
   ht->Open("file.root","READ");

   while(ht->Next()){
     Hit * hit = ht->Get();
     float timer = ht->GetTimer();
   }

   ht->Close();
   delete ht;
   @endverbatim
   *
   * @brief Tool to store Hit as a ROOT TTree.
   * @author Carlos.Solans@cern.ch
   * @date February 2022
   **/

class HitTree{

 public:

  /**
   * @brief Default constructor, emtpy.
   **/
  HitTree();

  /**
   * @brief Create an empty HitTree.
   **/
  HitTree(std::string name, std::string title);

  /**
   * @brief Delete the HitTree. Close any open file.
   **/
  ~HitTree();

  /**
   **/
  void Create(std::string name,std::string title);

  /**
   * @brief Write the tree
   **/
  void Write();

  /**
   * @brief Add the contents of Hit to the current entry.
   *        This also adds a time stamp (timer) to the entry.
   * @param hit The Hit that needs to be stored in the tree entry.
   **/
  void Fill(Hit * hit);

  /**
   * @brief Get the current entry as a Hit object.
   * @return A Hit object.
   **/
  Hit * Get();

  /**
   * @brief Get the timestamp of the entry.
   * @return The timestamp of the run number.
   **/
  float GetTimer();

  /**
   * @brief Move the internal pointer to the next entry in the tree.
   * @return 0 if there are no more entries to read, -1 if there was an error.
   **/
  int Next();

  /**
   * @brief Set the injected charge associated to the hits (will stay until reset).
   * @param vcal The injected charge.
   **/
  void setVcal(uint32_t vcal);


 private:

  std::string m_name;
  std::string m_title;
  TTree * m_tree;
  bool m_readonly;
  std::chrono::steady_clock::time_point m_t0;

  uint32_t evnum;
  uint32_t row;
  uint32_t col;
  uint32_t tid;
  uint32_t ttag;
  uint32_t bcid;
  float    timer;
  uint32_t m_vcal;

  Hit m_hit;
  uint64_t m_entry;
};

}

#endif
