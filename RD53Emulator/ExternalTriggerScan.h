#ifndef RD53A_EXTERNALTRIGGERSCAN_H
#define RD53A_EXTERNALTRIGGERSCAN_H

#include "RD53Emulator/Handler.h"
#include "TH2I.h"

namespace RD53A{

  /**
   * A Source scan is designed to see hits from a source.
   * Many random triggers are sent with the hope to see a hit from the source.
   *
   * @brief RD53A ExternalTriggerScan
   * @author Brian.Moser@cern.ch
   * @date August 2022
   */

  class ExternalTriggerScan: public Handler{

  public:

    /**
     * Create the scan
     */
    ExternalTriggerScan();

    /**
     * Delete the Handler
     */
    virtual ~ExternalTriggerScan();

    /**
     * Set the value of the variables for this scan, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms. 
     */
    virtual void Analysis();

    /**
     * Method to call the stop of the scan from inside the loop
     * @param signal the signal thrown by the signal catch
     */
    static void StopScan(int32_t signal);

  private:

    static bool m_scanStop;

    float m_TriggerFrequency;
    double m_frequency;
    double m_duration;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_occHigh;
    std::map<std::string,TH2I*> m_enable;
    std::map<std::string,TH2I*> m_mask;
    std::map<std::string,TH1I*> m_tot;
    std::map<std::string,TH1I*> m_bcid;
    TH1I* m_triggers;
    std::map<std::string,TH2I*> m_deltaBCID;
    
    uint32_t m_colMin;
    uint32_t m_colMax;

  public:
    uint32_t m_TriggerLatency;

  };

}

#endif
