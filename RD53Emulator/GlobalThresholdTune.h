#ifndef RD53A_GLOBALTHRESHOLDTUNE_H
#define RD53A_GLOBALTHRESHOLDTUNE_H

#include "RD53Emulator/Handler.h"
#include "TH2.h"

namespace RD53A{

  /**
   * A global threshold tuning identifies the value of the global threshold DAC that 
   * generates the desired threshold in each frontend.
   * The tuning takes as input the target threshold, which corresponds to the 
   * injection charge.
   * The tuning consists of a loop of analog scans, performed with different 
   * values of the global threshold DACs. At the end of the loop, the value
   * that allows to obtain the desired threshold will be saved in the
   * configuration file.
   * The DAC value for which the mean occupancy is 50% of m_ntrigs corresponds                                                                                         
   * to the threshold target. Therefore we stop the scan when                                                                                                          
   * meanTh>0.5 * num_triggers and then we find the DAC value for which
   * meanTh=0.5 * num_triggers by linear interpolation between the last DAC value                                                                                           * and the previous one.      
   *
   * @brief RD53A GlobalThresholdTune
   * @author Alessandra.Palazzo@cern.ch
   * @author Ismet.Siral@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date January 2021
   */

  class GlobalThresholdTune: public Handler{

  public:

    /**
     * Create the scan
     */
    GlobalThresholdTune();

    /**
     * Delete the Handler
     */
    virtual ~GlobalThresholdTune();

    /**
     * Set the value of the variables for this tuning, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the occupancy map per trigger.
     * An occupancy map is filled for each value of the DAC.
     */
    virtual void Run();

    /**
     * Delete histograms. 
     * Interpolate the value of the global DAC which allows to obtain the 
     * desired target and save it in the configuration file. 
     */
    virtual void Analysis();

  private:
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    uint32_t m_vcalMed;
    uint32_t m_vcalDiff;
    uint32_t m_vcalHigh;
    uint32_t m_whichfe;
    uint32_t m_colMin;
    uint32_t m_colMax;
    uint32_t m_DACMax;
    uint32_t m_DACMin;
    uint32_t m_DACStep;
    uint32_t m_nSteps;
    uint32_t m_iStep;
    std::map<std::string,TH2I**> m_occ;
  };

}

#endif
