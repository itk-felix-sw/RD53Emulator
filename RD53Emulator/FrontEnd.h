#ifndef RD53A_FRONTEND_H
#define RD53A_FRONTEND_H

#include "RD53Emulator/Decoder.h"
#include "RD53Emulator/Encoder.h"
#include "RD53Emulator/Command.h"
#include "RD53Emulator/Configuration.h"
#include "RD53Emulator/Matrix.h"
#include "RD53Emulator/Hit.h"
#include "RD53Emulator/TemperatureSensor.h"
#include "RD53Emulator/RadiationSensor.h"

#include <queue>
#include <vector>
#include <mutex>


namespace RD53A{

/**
 * This class controls an RD53A pixel detector that is identified by
 * a chip ID (FrontEnd::GetChipID, FrontEnd::SetChipID),
 * and has a name (FrontEnd::GetName, FrontEnd::SetName),
 * by making use of a Command Encoder, a Frame Decoder, a Field Configuration, and a Pixel Matrix.
 *
 * The Encoder encodes the operations on the FrontEnd like
 * loading the global configuration (FrontEnd::SetGlobalConfig),
 * loading the per pixel configuration (FrontEnd::SetPixelConfig),
 * setting different mask stages for calibration,
 * and triggering the detector (FrontEnd::Trigger),
 * into the corresponding Command objects.
 * These are converted into a byte stream on request (FrontEnd::ProcessCommands),
 * and available for the communication layer (FrontEnd::GetBytes, FrontEnd::GetLength).
 *
 * The Decoder decodes the data from the communication layer (FrontEnd::HandleData),
 * and assigns the read-back registers (RegisterFrame) to the global Configuration,
 * and converts the rest of the data (DataFrame) into Hit objects that are stored in a FIFO.
 * The status of the FIFO can be checked (FrontEnd::HasHits), and polled (FrontEnd::GetHit, FrontEnd::NextHit).
 *
 * The Configuration contains the global Register objects that can be accessed directly,
 * or through the virtual Field objects in which the Register objects are divided.
 *
 * The Matrix represents the pixel matrix configuration.
 *
 * The calibration injection circuit is enabled per double column (FrontEnd::SetCalColumn).
 * The injection can be further enabled or disabled per Pixel.
 * Injection takes place only for cal enabled pixels when S0 or S1 switch from low to high.
 * 
 * @verbatim

   FrontEnd fe;

   fe.SetGlobalConfig(...);
   fe.ProcessCommands();
   fe.Trigger();

   uint8_t * out_bytes = fe.GetBytes();
   uin32_t out_length = fe.GetLength();

   fe.HandleData(in_bytes, in_length);

   while(fe.HasHits()){
     Hit * hit = fe.GetHit();
     hit.NextHit();
   }

   @endverbatim
 *
 * @todo Improve class documentation
 * @brief RD53A FrontEnd controller.
 * @author Carlos.Solans@cern.ch
 * @author Enrico.Junior.Schioppa@cern.ch
 * @date September 2020
 **/

  class FrontEnd {

  public:

    /**
     * Create a new RD53A FrontEnd
     * Allocates the Encoder and Decoder byte stream converters, and a Configuration object.
     **/
    FrontEnd();

    /**
     * Delete the Encoder, Decoder, and Configuration objects.
     */
    ~FrontEnd();

    /**
     * Enable the verbose mode
     * @param enable Enable verbose mode if true
     **/
    void SetVerbose(bool enable);

    /**
     * Get the ID for this emulated front-end.
     * @return The ID for this emulated front-end from 0 to 7.
     **/
    uint32_t GetChipID();

    /**
     * Set the ID for this emulated front-end from 0 to 7.
     * @return The ID for this emulated front-end from 0 to 7.
     **/
    void SetChipID(uint32_t chipid);

    /**
     * Set the common name of this FrontEnd.
     * @param name The common name for this FrontEnd.
     **/
    void SetName(std::string name);

    /**
     * Get the common name of this FrontEnd.
     * @return The common name for this FrontEnd.
     **/
    std::string GetName();

    /**
     * Get the global Configuration pointer for this FrontEnd.
     * It allows direct access to all the global 16-bit Register objects,
     * and the virtual Field objects associated like the HitOr
     * (FrontEnd::GetConfig()->GetField(Configuration::REGION_ROW)->GetValue()).
     * @return The global Configuration pointer
     */
    Configuration * GetConfig();

    /**
     * Call the backup method of the Configuration object
     **/
    void BackupConfig();

    /**
     * Restore the backup of the Configuration object
     **/
    void RestoreBackupConfig();

    /**
     * Clear the Command Encoder and the Record Decoder
     */
    void Clear();
    
    /**
     * Prepare commands to be sent to the FrontEnd for Reset.
     * Commands must be sent to the FrontEnd after calling this method.  
     * There are 6 reset steps. Steps 0,1,2 are mandatory.
     * - Write the Configuration::GLOBAL_PULSE_RT field with
     *   reset the channel synchronizer (Configuration::GP_RESET_CHN)
     *   reset the command decoder (Configuration::GP_RESET_CMD)
     *   reset the global configuration (Configuration::GP_RESET_CONFIG)
     *   reset the monitoring data (Configuration::GP_RESET_MON)
     *   reset the aurora encoding and start channel bonding sequence (Configuration::GP_RESET_AURORA)
     *   reset the serializer (Configuration::GP_RESET_SERIALIZER)
     *   reset the ADC (Configuration::GP_RESET_ADC)
     * - Write a Pulse command in broadcast mode so that the 
     *   settings in Configuration::GLOBAL_PULSE_RT take effect
     * - Write a sequence of 32 Sync commands
     * - Write the Configuration::GLOBAL_PULSE_RT field with
     *   Start the monitoring (Configuration::GP_START_MON)
     *   Start Sync front-end auto-zero (Configuration::GP_START_ZERO_LEVEL)
     * - Write a Pulse command in broadcast mode so that the 
     *   settings in Configuration::GLOBAL_PULSE_RT take effect
     * - Write ECR followed by a Noop
     * - Write BCR followed by a Noop
     * 
     * @param step Reset sequence index from 0 to 6
     */
    void Reset(uint32_t step);
    
    /**
     * Check if the FrontEnd is active
     * An inactive FrontEnd should not be used further during a scan.
     * @return True if it is active
     */
    bool IsActive();

    /**
     * Force the FrontEnd to be active or inactive.
     * An inactive FrontEnd should not be used further during a scan.
     * @param active Activate if True, else deactivate the FrontEnd
     */
    void SetActive(bool active);

    /**
     * Enable all the pixels by looping over the Pixels in the Matrix
     * and enabling Pixel::SetEnable and Pixel::SetInject
     **/
    void EnableAll();
    /**
     * Disables all the pixels by looping over the Pixels in the Matrix
     * and disabling Pixel::SetEnable and Pixel::SetInject
     **/
    void DisableAll();

    /**
     * Set the global threshold for the given type of front-end
     * @param type The type of front-end (Sync, Linear, Diff)
     * @param threshold The global threshold
     */
    void SetGlobalThreshold(uint32_t type, uint32_t threshold);

    /**
     * Get the global threshold for the given type of front-end
     * @param type The type of front-end (Sync, Linear, Diff)
     * @return The global threshold
     */
    uint32_t GetGlobalThreshold(uint32_t type);

    /**
     * Get the global threshold for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @return The global threshold
     */
    uint32_t GetGlobalThreshold(uint32_t col, uint32_t row);
    
    /**
     * Set the output bit (Pixel::Enable) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @param enable Enable the Pixel output if true
     */
    void SetPixelEnable(uint32_t col, uint32_t row, bool enable);

    /**
     * Get the output bit (Pixel::Enable) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @return True if the Pixel output is enabled
     */
    bool GetPixelEnable(uint32_t col, uint32_t row);

    /**
     * Set the inject bit (Pixel::Inject) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @param enable Enable the Pixel injection if true
     */
    void SetPixelInject(uint32_t col, uint32_t row, bool enable);

    /**
     * Get the inject bit (Pixel::Inject) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @return True if the Pixel injection is enabled
     */
    bool GetPixelInject(uint32_t col, uint32_t row);

    /**
     * Get the hitbus bit (Pixel::Hitbus) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @param enable Enable the Pixel hitbus if true
     */
    void SetPixelHitbus(uint32_t col, uint32_t row, bool enable);

    /**
     * Get the hitbus bit (Pixel::Hitbus) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @return True if the Pixel hitbus is enabled
     */
    bool GetPixelHitbus(uint32_t col, uint32_t row);

    /**
     * Set the in pixel threshold (Pixel::TDAC and Pixel::Sign) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @param threshold The pixel threshold
     */
    void SetPixelThreshold(uint32_t col, uint32_t row, int32_t threshold);

    /**
     * Get the in pixel threshold (Pixel::TDAC and Pixel::Sign) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @return The pixel threshold
     */
    int32_t GetPixelThreshold(uint32_t col, uint32_t row);

    /**
     * Set the pixel gain (Pixel::Gain) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @param enable Pixel gain
     */
    void SetPixelGain(uint32_t col, uint32_t row, bool enable);

    /**
     * Set the pixel gain (Pixel::Gain) for the given Pixel
     * @param col The Pixel column
     * @param row The Pixel row
     * @return The pixel gain
     */
    bool GetPixelGain(uint32_t col, uint32_t row);

    /**
     * Store the mask bit (Pixel::SetMask) for the given Pixel.
     * @param col The pixel column
     * @param row The pixel row
     * @param mask If true, mask the pixel
     */
    void SetPixelMask(uint32_t col, uint32_t row, uint32_t mask);

    /**
     * Return the mask (Pixel::SetMask) for the given Pixel.
     * @param col The pixel column
     * @param row The pixel row
     * @return True if the Pixel is masked
     */
    uint32_t GetPixelMask(uint32_t col, uint32_t row);

    /**
     * Function to apply the masking from the matrix configuration
     */
    void ApplyMasking();

    /**
     * Set the global registers of this FrontEnd by passing a map of configuration values.
     * This is the default way to load the global configuration into memory.
     * In order to convert the configuration values into Commands call FrontEnd::ConfigGlobal
     * after calling this method.
     * The strings used for this map can be found in Configuration::m_name2index.
     * @param config A map of strings to unsigned 32 bit values
     */
    void SetGlobalConfig(std::map<std::string,uint32_t> config);

    /**
     * Get the global registers of this FrontEnd by passing a map of configuration values.
     * This is the default way to retrieve the global configuration into memory.
     * @return A map of strings to unsigned 32 bit values
     */
    std::map<std::string,uint32_t> GetGlobalConfig();

    /**
     * Convert the updated global registers into a WrReg Command array, and add them to the Encoder.
     */
    void ProcessCommands();
    
    /**
     * Convert the updated global registers into a WrReg Command array, and add them to the Encoder.
     */
    void WriteGlobal();

    /**
     * Add a single register to the Encoder as a WrReg command.
     * @param addr The register address
     */
    void WriteRegister(uint32_t addr);

    /**
     * read a single register from the Encoder as a RdReg command.
     * @param addr The register address
     */
    void ReadRegister(uint32_t addr);

    /**
     * Request the reading of the global registers
     * @todo Might need to put the registers that have been requested into a queue
     * and compare with them in the HandleData
     */
    void ReadGlobal();

    /**
     * Write the in-pixel configuration of all the pixels in the front-end
     */
    void WritePixels();

    /**
     * Write the in-pixel configuration of the selected pixel pair to the front-end
     * @param double_col The pixel column pair (0 to 199)
     * @param row The pixel row (0 to 191)
     */
    void WritePixelPair(uint32_t double_col, uint32_t row);

    /**
     * Read the in-pixel configuration of all the pixels in the front-end
     * @todo Might need to implement a lookup table for the registers that come out
     */
    void ReadPixels();

    /**
     * Read the in-pixel configuration of the selected pixel pair to the front-end
     * @param double_col The pixel column pair (0 to 199)
     * @param row The pixel row (0 to 191)
     */
    void ReadPixelPair(uint32_t double_col, uint32_t row);

    /**
     * Enable a subset of core columns for the scan.
     * The mode selects how many core columns are selected at a time.
     * The index runs from 0 to the value of mode effectively
     * changing the core column that is selected.
     *
     * @param mode How many core columns are selected at a time (0 to 40)
     * @param index Sequence number between 0 and mode
     * @param frontend the type of front-end affected: syn, lin, diff
     */
    void SetCoreColumn(uint32_t mode, uint32_t index, std::string frontend);

    /**
     * Enable or disable the core column identified by ccol.
     *
     * @param ccol Core column index (0 to 39)
     * @param enable Boolean desired status for the ccol. Enable if true, else disable.
     */
    void EnableCoreColumn(uint32_t ccol, bool enable);

    /**
     * Enable or disable the calibration injection circuit on a double column.
     * There is one bit per double column (pixel pair).
     * Accordint to the manual, all the double columns of a core column
     * have to be enabled or disabled at the same time for the injection to work.
     * This effectively changes the COLPR registers:
     * - CAL_COLPR_SYNC_1: double columns: 0 to 15
     * - CAL_COLPR_SYNC_2: double columns: 16 to 31
     * - CAL_COLPR_SYNC_3: double columns: 32 to 47
     * - CAL_COLPR_SYNC_4: double columns: 48 to 63
     * - CAL_COLPR_LIN_1: double columns: 64 to 79
     * - CAL_COLPR_LIN_2: double columns: 80 to 95
     * - CAL_COLPR_LIN_3: double columns: 96 to 111
     * - CAL_COLPR_LIN_4: double columns: 112 to 127
     * - CAL_COLPR_LIN_5: double columns: 128 to 131
     * - CAL_COLPR_DIFF_1: double columns: 132 to 147
     * - CAL_COLPR_DIFF_2: double columns: 148 to 165
     * - CAL_COLPR_DIFF_3: double columns: 164 to 179
     * - CAL_COLPR_DIFF_4: double columns: 180 to 195
     * - CAL_COLPR_DIFF_5: double columns: 196 to 199
     *
     * @param double_col The double column to select (0 to 199)
     * @param enable Enable or Disable calibration pulse
     */
    void EnableCalDoubleColumn(uint32_t double_col, bool enable);

    /**
     * Enable of disable the calibration injection circuit per core column.
     * This method calls FrontEnd::EnableCalDoubleColumn for all the double columns in the core column
     *
     * @param core_col The core column to select (0 to 49)
     * @param enable Enable or Disable calibration pulse
     */
    void EnableCalCoreColumn(uint32_t core_col, bool enable);
 
    /**
     * Initialize the ADC of the chip to read the radiation an temperature sensors
     */
    void InitAdc();

    /**
     * Read a given radiation or temperature sensor
     * @param pos Position of the sensor (0 to 3)
     * @param read_radiation_sensor true if is a radiation sensor.
     */
    void ReadSensor(uint32_t pos, bool read_radiation_sensor);

    /**
     * Get a NTC temperature sensor
     * @param index The index of the sensor (0 to 3)
     * @return The TemperatureSensor pointer
     */
    TemperatureSensor * GetTemperatureSensor(uint32_t index);

    /**
     * Get a BJT radiation sensor
     * @param index The index of the sensor (0 to 3)
     * @return The RadiationSensor pointer
     */
    RadiationSensor * GetRadiationSensor(uint32_t index);

    /**
     * Prepare the trigger sequence for the scan like the following:
     * 32 Noop + 1 Cal + (delay/4) Noop + 1 Trigger + 10 Noop
     * @param delay The number of BCs between CAL and Trigger commands
     * @param trigMultiplier The number of consecutive trigger to send at each trigger command
     * @param bc1 Trigger on the first BC of the Trigger command.
     * @param bc2 Trigger on the second BC of the Trigger command.
     * @param bc3 Trigger on the third BC of the Trigger command.
     * @param bc4 Trigger on the fourth BC of the Trigger command.
     */
    void Trigger(uint32_t delay=0, uint32_t trigMultiplier=1, bool bc1=1, bool bc2=0, bool bc3=0, bool bc4=0);

    /**
     * Get the next Hit from the FIFO
     * @return The next available Hit
     */
    Hit * GetHit();

    /**
     * Pop the next Hit from the FIFO and delete it.
     * @return True if there FIFO is still not empty.
     */
    bool NextHit();

    /**
     * Check if the FIFO of Hits has any hits.
     * @return True if there FIFO has hits.
     */
    bool HasHits();

    /**
     * Delete all the hits that are still in the FIFO
     */
    void ClearHits();

    /**
     * Handle the data from the communication layer given by a byte array and its size.
     * The byte array will be decoded by the Decoder into a Record array,
     * and interpreted accordingly. The read-back values of the
     * @param recv_data Array of bytes from the communication layer.
     * @param recv_size Number of bytes in the byte array
     * @param (optional) register_data Set that the recieved data packet should be handled as register 
     */
    void HandleData(uint8_t *recv_data, uint32_t recv_size, bool register_data=false);

    /**
     * Encode the commands stored in the Encoder in order to obtain the byte array
     * that has to be sent to the communication layer.
     * Calling this method is mandatory after performing any configuration, in order to populate the byte array.
     * The communication with the actual front-end is handled externally to this class.
     */
    void Encode();

    /**
     * Get the output byte stream from the emulator.
     * @return Byte stream output from the emulator.
     **/
    uint8_t * GetBytes();

    /**
     * Get the length of the output byte stream from the emulator.
     * @return Length of the byte stream output from the emulator.
     **/
    uint32_t GetLength();


    /**
     * Set the total number of events per front-end at the end of the scan.
     * @param The number of events at the end of the scan.
     **/
    void SetNumberOfEvents(uint32_t totevents);

    /**
     * Get the total number of events per front-end at the end of the scan.
     * @return The number of events at the end of the scan.
     **/
    uint32_t GetTotalEventsNumber();

    /**
     * Set the name of the DCS node for that chip. 
     * @param The DCS node.
     **/    
    void SetDcsNode(std::string m_dcs_node);

    /**
     * Get the name of the DCS node for that chip. 
     * @return The DCS node.
     **/    
    std::string GetDcsNode();

  private:

    bool m_verbose;
    bool m_active;
    uint32_t m_chipid;
    std::string m_name;

    Decoder *m_decoder;
    Encoder *m_encoder;
    Encoder *m_trigger;

    Configuration *m_config;
    Matrix *m_matrix;
    std::deque<Hit*> m_hits;
    std::mutex m_mutex;
    
    std::vector<TemperatureSensor*> m_ntcs;
    std::vector<RadiationSensor*> m_bjts;

    uint32_t m_totalHits;
    Hit * m_hit;
    int32_t m_refBCID;
    std::string m_dcs_node;
  };

}
#endif
