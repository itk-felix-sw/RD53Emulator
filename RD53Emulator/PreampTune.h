#ifndef RD53A_PREAMPTUNE_H
#define RD53A_PREAMPTUNE_H

#include "RD53Emulator/Handler.h"
#include "TH2.h"

namespace RD53A{

  /**
   * A Preamplifier tuning identifies the values of the parameters 
   * IBIAS_KRUM_SYNC/KRUM_CURR_LIN/VFF_DIFF which allow to obtain the desired
   * ToT. These parameters determine the value of the current which discharges
   * the feedback capacitors in each frontend.
   * The tuning takes as input the value of the charge to be injected and the 
   * ToT target.                                                      
   * The tuning consists of a loop of ToT scans, performed with different
   * values of IBIAS_KRUM_SYNC/KRUM_CURR_LIN/VFF_DIFF. At the end of the loop, 
   * the values of the parameters which allow to obtain the desired ToT will be
   * saved in the configuration file.
   *
   * @brief RD53A AnalogScan
   * @author Alessandra.Palazzo@cern.ch
   * @date January 2021
   */

  class PreampTune: public Handler{

  public:

    /**
     * Create the scan
     */
    PreampTune();

    /**
     * Delete the Handler
     */
    virtual ~PreampTune();

    /**
     * Set the value of the variables for this tuning, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits and their ToT per trigger
     * and per frontend.
     * A ToT map is filled for each value of the parameter 
     * IBIAS_KRUM_SYNC/KRUM_CURR_LIN/VFF_DIFF.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     * Interpolate the value of the parameter which allows to obtain the 
     * desired ToT and save it in the configuration file. 
     */
    virtual void Analysis();

  private:
    uint32_t m_mask_step;
    uint32_t m_cc_max;
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    uint32_t m_DACMax;
    uint32_t m_DACMin;
    uint32_t m_DACStep;
    uint32_t m_nSteps;
    uint32_t m_vcalMed;
    uint32_t m_vcalDiff;
    uint32_t m_vcalHigh;
    uint32_t m_ToTtarget;
    std::string m_whichfe;
    uint32_t m_colMin;
    uint32_t m_colMax;
    float m_PreviousToTDiff;
    float m_ToTDiff;
    float m_PreviousMeanToT;
    float m_meanToTValue;
    bool m_endLoop;
    uint32_t m_iStep;
    std::map<std::string,std::vector<int>> m_mem_thres;
    std::map<std::string,TH2I**> m_occ;
    std::map<std::string,TH2I**> m_tot;
    std::map<std::string,TH2F**> m_meantot;
  };

}

#endif
