#ifndef RD53A_TOTSCAN_H
#define RD53A_TOTSCAN_H

#include "RD53Emulator/Handler.h"
#include "TH2.h"

namespace RD53A{

  /**
   * A ToTScan injects a signal in the analog circuit of the pixel, checks for
   * the corresponding hit in the data and computes the corresponding ToT. 
   * It also computes the sigma ToT map.
   * The analog injection is performed as in the AnalogScan. 
   * The ToT is computed using 4 bits. 
   *
   * @brief RD53A AnalogScan
   * @author Carlos.Solans@cern.ch
   * @author Enrico.Junior.Schioppa@cern.ch
   * @author Alessandra.Palazzo@cern.ch
   * @date December 2020
   */

  class ToTScan: public Handler{

  public:

    /**
     * Create the scan
     */
    ToTScan();

    /**
     * Delete the Handler
     */
    virtual ~ToTScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits and their ToT values per
     * trigger.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

  private:
    uint32_t m_mask_step;
    uint32_t m_cc_max;
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    uint32_t m_vcalMed;
    uint32_t m_vcalDiff;
    uint32_t m_vcalHigh;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_tot;
    std::map<std::string,TH2F*> m_meantot;
    std::map<std::string,TH2I*> m_tot2;
    std::map<std::string,TH2F*> m_sigmatot;
    std::map<std::string,TH1I*> m_totT;

    std::map<std::string,TH1F*> m_meantot_1d;
    std::map<std::string,TH1F*> m_sigmatot_1d;

  };

}

#endif
