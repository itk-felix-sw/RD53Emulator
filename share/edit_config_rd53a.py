#!/usr/bin/env python
##################################
# Compare json config files
#
# Carlos.Solans@cern.ch
# September 2021
##################################

import os
import sys
import json
import argparse

rd53a_globals=['AdcRead', 'AdcRefTrim', 'AdcTrim', 'AiPixCol', 'AiPixRow', 'AuroraCbSend',
               'AuroraCbWaitHigh', 'AuroraCbWaitLow', 'AuroraCcSend', 'AuroraCcWait',
               'AuroraInitWait', 'AutoReadA0', 'AutoReadA1', 'AutoReadA2', 'AutoReadA3',
               'AutoReadB0', 'AutoReadB1', 'AutoReadB2', 'AutoReadB3', 'BcCounter',
               'BflipErrCounter', 'BflipWarnCounter', 'CalColprDiff1', 'CalColprDiff2',
               'CalColprDiff3', 'CalColprDiff4', 'CalColprDiff5', 'CalColprLin1',
               'CalColprLin2', 'CalColprLin3', 'CalColprLin4', 'CalColprLin5',
               'CalColprSync1', 'CalColprSync2', 'CalColprSync3', 'CalColprSync4',
               'CdrCpIbias', 'CdrEnGck', 'CdrPdDel', 'CdrPdSel', 'CdrSelDelClk',
               'CdrSelSerClk', 'CdrVcoGain', 'ChSyncLock', 'ChSyncPhase', 'ChSyncUnlock',
               'ClkDelay', 'ClkDelaySel', 'CmdDelay', 'CmdErrCounter', 'CmlEn', 'CmlEnTap',
               'CmlInvTap', 'CmlTapBias0', 'CmlTapBias1', 'CmlTapBias2', 'DebugConfig',
               'DiffComp', 'DiffFbCapEn', 'DiffFol', 'DiffLcc', 'DiffLccEn', 'DiffPrecomp',
               'DiffPrmp', 'DiffVff', 'DiffVth1', 'DiffVth2', 'EnCoreColDiff1', 'EnCoreColDiff2',
               'EnCoreColLin1', 'EnCoreColLin2', 'EnCoreColSync', 'ErrMask', 'FifoFullCounter0',
               'FifoFullCounter1', 'FifoFullCounter2', 'FifoFullCounter3', 'GlobalPulseRt',
               'GpLvdsRoute', 'HitOr0MaskDiff0', 'HitOr0MaskDiff1', 'HitOr0MaskLin0',
               'HitOr0MaskLin1', 'HitOr0MaskSync', 'HitOr1MaskDiff0', 'HitOr1MaskDiff1',
               'HitOr1MaskLin0', 'HitOr1MaskLin1', 'HitOr1MaskSync', 'HitOr2MaskDiff0',
               'HitOr2MaskDiff1', 'HitOr2MaskLin0', 'HitOr2MaskLin1', 'HitOr2MaskSync',
               'HitOr3MaskDiff0', 'HitOr3MaskDiff1', 'HitOr3MaskLin0', 'HitOr3MaskLin1',
               'HitOr3MaskSync', 'HitOrCounter0', 'HitOrCounter1', 'HitOrCounter2',
               'HitOrCounter3', 'InjAnaMode', 'InjDelay', 'InjEnDig', 'InjVcalDiff',
               'InjVcalHigh', 'InjVcalMed', 'LatencyConfig', 'LinComp', 'LinFcBias',
               'LinKrumCurr', 'LinLdac', 'LinPaInBias', 'LinRefKrum', 'LinVth',
               'LockLossCounter', 'MonFrameSkip', 'MonitorEnable', 'MonitorImonMux',
               'MonitorVmonMux', 'OutPadConfig', 'OutputActiveLanes', 'OutputDataReadDelay',
               'OutputFmt', 'OutputSerType', 'PixAutoCol', 'PixAutoRow', 'PixBroadcastEn',
               'PixBroadcastMask', 'PixDefaultConfig', 'PixPortal', 'PixRegionCol',
               'PixRegionRow', 'RingOsc0', 'RingOsc1', 'RingOsc2', 'RingOsc3', 'RingOsc4',
               'RingOsc5', 'RingOsc6', 'RingOsc7', 'RingOscEn', 'SelfTrigEn', 'SensorCfg0',
               'SensorCfg1', 'SerSelOut0', 'SerSelOut1', 'SerSelOut2', 'SerSelOut3',
               'SkipTriggerCounter', 'SldoAnalogTrim', 'SldoDigitalTrim', 'SyncAutoZero',
               'SyncFastTot', 'SyncIbiasDisc', 'SyncIbiasKrum', 'SyncIbiasSf', 'SyncIbiasp1',
               'SyncIbiasp2', 'SyncIctrlSynct', 'SyncSelC2F', 'SyncSelC4F', 'SyncVbl',
               'SyncVrefKrum', 'SyncVth', 'TrigCounter', 'VcoBuffBias', 'VcoIbias', 'WrSyncDelaySync']

rd53a_pixels=['Enable','Hitbus','InjEn','TDAC']

parser = argparse.ArgumentParser()
parser.add_argument('file1',help='file1')
parser.add_argument('file2',help='file2')
parser.add_argument('-r','--register',help="Register name", required=True)
parser.add_argument('-v','--value',help="Register value", type=int, required=True)
parser.add_argument('-p','--pixels', help="pixel range [px,py], or [px1,py1,px2,py2]", type=int, nargs="*")

args = parser.parse_args()

n1=os.path.basename(args.file1)

print ("Open config file: %s" % n1)

j1=json.load(open(args.file1))

if args.register in rd53a_globals:
    j1["RD53A"]["GlobalConfig"][args.register]=args.value
    pass

elif args.register in rd53a_pixels and args.pixels:
    if len(args.pixels)>1:
        px1 = args.pixels[0]
        py1 = args.pixels[1]
        px2 = px1+1
        py2 = py1+1
        if len(args.pixels)>3:
            px2=args.pixels[2]
            py2=args.pixels[3]
            pass
        for col in range(px1,px2):
            for row in range(py1,py2):
                print("Change px,py: %i,%i %s=%i" % (col,row,args.register,args.value))
                j1["RD53A"]["PixelConfig"][col][args.register][row]=args.value
                pass
            pass
        pass
    pass

elif not args.pixels:
    print("Missing pixel range")
    sys.exit()
else:
    print("Register: %s NOT FOUND" % args.register)
    sys.exit()
    
print ("Write file: %s" % args.file2)
with open(args.file2, 'w') as fw: json.dump(j1,fw)
    
print("Have a nice day")
