import ROOT as r
import argparse
import os
import math

r.gROOT.SetBatch(True)
#r.gROOT.LoadMacro("/home/itkpix/atlasstyle/AtlasStyle.C+")
#r.SetAtlasStyle()

parser = argparse.ArgumentParser()
parser.add_argument('--start', help="start index of runs to consider", type=int, required=True)
parser.add_argument('--stop' , help="stop index of runs to consder"  , type=int, required=True)
parser.add_argument('--fe'   , help="name of the output to plot"     , type=str, required=True)

args = parser.parse_args()

start_idx = args.start
stop_idx  = args.stop
fe_name   = args.fe

l_scans = []

hfinal = None 

veto_ids = []
#for i in range(1998, 2011):  veto_ids.append(i)
#veto_ids.append(2203)


for i in range(start_idx, stop_idx+1):
    #if i in veto_ids:
    #    continue
    l_scans.append("%06i_ExternalTriggerScanLinDiff"%i)

print (l_scans)

idx = 0
for folder in l_scans:
    print ("-----------------------------------------------------------------")
    print (" folder: "+folder)
    if not os.path.isfile("/home/itkpix/data/"+folder+"/output.root"):
        print (" root file not found in folder: "+folder+" .... skipping")
        continue
        
    f = r.TFile("/home/itkpix/data/"+folder+"/output.root", "read")
    if f.IsZombie():
        print (" root file for folder "+folder+" is broken ........ skipping")
        continue
    else:
        pass
        #print("use file")
    f.ls()
    h = f.Get("occ_"+fe_name)
    if not h:
        continue
    h.SetDirectory(0)
    
    intTot=float(h.Integral(0,-1,0,-1)     )
    if intTot==0: continue
    intSyn=float(h.Integral(  0,128, 0,-1) )
    intLin=float(h.Integral(128,264, 0,-1) )
    intDif=float(h.Integral(264, -1, 0,-1) )

    print (" - "+folder+" : "+str(intTot)+" , "+str(round(intSyn/intTot,3))+" , "+str(round(intLin/intTot,3))+" , "+str(round(intDif/intTot,3)) )

    ##if intLin/intTot<0.8:  continue

    if idx == 0:
        hfinal = h.Clone()
        hfinal.SetDirectory(0)
        idx+=1
    else:
        hfinal.Add(h)


maxHits = hfinal.GetMaximum()
upperRange = maxHits/100.
hHits = r.TH1D("hits", "", int(math.log(upperRange))*100, 0.5, math.log(upperRange)+0.5)

#remove the outliers
nBins = 0
integral = 0
for y in range(1, hfinal.GetNbinsY()+1):
    for x in range(1, hfinal.GetNbinsX()+1):
        z = hfinal.GetBinContent(x, y)
        if z > 0.5:
            nBins += 1
            integral += z
            if z >= upperRange:
                z = upperRange-0.5
            hHits.Fill(math.log(z))

#now find the threshold
nHits = hHits.Integral(0, -1)
thres = 0
for i in range(2, hHits.GetNbinsX()+1):
    partInt = hHits.Integral(0, i)
    if partInt/nHits > 0.999:
        thres = hHits.GetXaxis().GetBinCenter(i) - 2*hHits.GetXaxis().GetBinWidth(i)
        break

thres = math.exp(thres)

for y in range(1, hfinal.GetNbinsY()+1):
    for x in range(1, hfinal.GetNbinsX()+1):
        z = hfinal.GetBinContent(x, y)
        #if z > thres:
        #    hfinal.SetBinContent(x, y, 0)

c = r.TCanvas("c","",0,0,1600,800)
r.gPad.SetRightMargin(0.2);
#c.SetLogz()
hfinal.GetZaxis().SetTitle("Hits")
# r.gStyle.SetTitleFont(42)
# r.gStyle.SetTextFont(42)
# r.gStyle.SetLabelFont(42)
hfinal.GetZaxis().SetTitleFont(42)
hfinal.SetMaximum(50)
hfinal.Draw("colz")
c.SaveAs("C_"+fe_name+".pdf")

#c2 = r.TCanvas()
#c2.SetLogx()
#c2.SetLogy()
#hHits.Draw()
#c2.SaveAs("C_hits_"+fe_name+".pdf")

os.system("evince "+"C_"+fe_name+".pdf &")
#os.system("evince "+"C_hits_"+fe_name+".pdf &")
