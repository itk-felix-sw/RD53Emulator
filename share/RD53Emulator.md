# RD53A software for ITK-FELIX {#PackageRD53Emulator}

This package contains all the software necesary to control, read-out and emulate the RD53A pixel detector.

## The RD53A pixel detector

The RD53A is a pixel detector with 400 columns and 192 rows split into 3 different front-end regions.
The Synchronous front-end from columns 0 to 127, the Linear front-end from columns 128 to 263, and 
the differential front-end from column 264 to column 399.

The global RD53A::Configuration is stored in 137 16-bit registers that are accessed by address. Each RD53A::Pixel contains up to 8 in-pixel bits, and arranged in a RD53A::Matrix of 8x8 pixel cores, that are addressed by pixel core column, pixel core row, and a region in the core that contains 4 pixels. Pixel configuration commands contain information for 2 consecutive pixels, for which an additiona pixel pair bit is required. The data from the RD53 is also identified by core columns, rows, and regions in rows, and contain the TOT for the 4 pixels in the region.

The command path of the RD53A can handle up to 160 Mbps of RD53A::Command commands. The RD53A::WrReg is a command to write a global RD53A::Register, and the RD53A::RdReg is a command to read-back a global RD53A::Register, that will appear in the data path in the form of a RD53A::RegisterFrame. In-pixel bits are written by sending a RD53A::WrReg command to a RD53A::Configuration::PIX_PORTAL address. The corresponding pixel pair is selected by the RD53A::Configuration::REGION_COL and RD53A::Configuration::REGION_ROW.

The data path of the RD53A contains RD53A::DataFrame or RD53A::RegisterFrame frames. Each RD53A::DataFrame can contain up to 2 RD53A::Hit or header sub-frames.

## Calibration procedure

There is a different global threshold (RD53A::FrontEnd::GetGlobalThreshold, RD53A::FrontEnd::SetGlobalThreshold) setting for each front-end flavour (RD53A::Configuration::VTH_SYNC for the Syncrhonous front-end, RD53A::Configuration::VTH_LIN for the Linear front-end, and both RD53A::Configuration::VTH1_DIFF, and RD53A::Configuration::VTH2_DIFF for the Differential front-end). 

The in-pixel threshold setting (RD53A::Pixel) is controlled by the 4-bit TDAC value (RD53A::Pixel::GetTDAC, RD53A::Pixel::SetTDAC) only available in the Linear and Differential front-ends.

The calibration injection charge is controlled by difference in the setting between RD53A::Configuration::VCAL_HIGH and RD53A::Configuration::VCAL_MED. Typically the RD53A::Configuration::VCAL_MED is left at 500 ADC counts while the RD53A::Configuration::VCAL_HIGH ranges from RD53A::Configuration::VCAL_MED to 4095. The injected charge in electrons is provided by Tools::injToCharge.

The steps of a scan are RD53A::Handler::InitRun, RD53A::Handler::Connect, RD53A::Handler::Config, RD53A::Handler::PreRun, RD53A::Handler::SaveConfig, RD53A::Handler::Run, RD53A::Handler::Analysis, RD53A::Handler::Disconnect, and RD53A::Handler::Save. The actual calibration procedure takes place inside the RD53A::Handler::Run method.

### Available calibration scans

- RD53A::TestScan
- RD53A::DigitalScan
- RD53A::AnalogScan
- RD53A::ThresholdScan
- RD53A::ToTScan
- RD53A::GlobalThresholdTune
- RD53A::PixelThresholdTune
- RD53A::PreampTune
- RD53A::TestScan
- RD53A::MaskCheck
- RD53A::SensorScan
- RD53A::NoiseScan

## Implementation paradigm

The RD53A::FrontEnd is implemented with a set of classes (RD53A::Encoder, RD53A::Decoder, RD53A::Configuration, RD53A::Matrix, RD53A::Pixel) that represent the functionalities of the RD53A. For example, the RD53A::Encoder encodes a byte array from a RD53A::Command array, and it can also decode a byte array into a RD53A::Command array. This allows closure tests, and the possibility of emulating the front-end behaviour in the RD53A::Emulator class.

The communication layer is meant to be external to the RD53A::FrontEnd which only deals with byte arrays to send commands and to receive data from the communication layer. Same is true for the RD53A::Emulator which receives commands as byte arrays and sends data as byte arrays. 

The RD53A::Handler implements the communication layer with FELIX through NETIO for any number of RD53A::FrontEnd objects. The processes inside the RD53A::Handler (RD53A::Handler::Connect, RD53A::Handler::Config, RD53A::Handler::Run) are single threaded, thus smart grouping of RD53A::FrontEnd objects into multiple RD53A::Handler instances, will allow concurrent processing. Needless to say, the tuning procedure of a pixel detector module is a sequence of tuning steps, but the tuning of many modules is a completely independent process, that can be highly parallelized. The available calibration scans and tuning procedures are those that extend from the RD53A::Handler.


### Connectivity file

```
{
  "connectivity" : [
    {"name": "emu-rd53a-1", "config" : "emu-rd53a-1.json", "rx" :  0, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12350, "data_port": 12360, "enable" : 0, "locked" : 0},
    {"name": "emu-rd53a-2", "config" : "emu-rd53a-2.json", "rx" :  4, "tx" :  4, "host": "127.0.0.1", "cmd_port": 12350, "data_port": 12360, "enable" : 0, "locked" : 0},
    ]
}
```


## How to run a scan

1. Prepare the configuration file for example for emu-rd53a-1.json
2. Prepare the connectivity file: make sure the emu-rd53a-1.json is referenced inside as emu-rd53a-1
3. Run the scan_manager_rd53a
   
   ```
   scan_manager_rd53a -n connectivity-file.json -s scan-name
   ```
   
## How to run the software emulator

1. Run the felix_emulator_rd53a

   
