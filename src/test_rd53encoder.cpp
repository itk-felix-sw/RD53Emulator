#include <iostream>
#include <ctime>
#include "RD53Emulator/Trigger.h"
#include "RD53Emulator/Encoder.h"
#include "RD53Emulator/WrReg.h"
#include "RD53Emulator/Pulse.h"
#include "RD53Emulator/RdReg.h"
#include "RD53Emulator/BCR.h"
#include "RD53Emulator/Cal.h"

using namespace std;
using namespace RD53A;

int main() {
  
  cout << "----> Test RD53 Trigger" << endl;
  Trigger *rd = new Trigger();
  /// Input Trigger /// 
  uint32_t inPattern=0x2B; 
  // Setting Input Pattern
  rd->SetPattern(inPattern);  
  cout << rd->ToString() << endl;

  /////////////////////////////////////////////////
  cout << "----> Test RD53 Write Register" << endl;
  WrReg *rdWrReg = new WrReg();
  uint32_t chipID=0x1B;
  uint32_t address=0x2C;
  // Setting Input Pattern
  rdWrReg->SetChipId(chipID);
  rdWrReg->SetAddress(address);
  rdWrReg->SetValue(0xFF,0);
  cout << rdWrReg->ToString() << endl;

  WrReg *rdWrReg2 = new WrReg();
  // Setting Input Pattern
  rdWrReg2->SetChipId(chipID);
  rdWrReg2->SetAddress(address);
  rdWrReg2->SetValue(0xFF,1);
  cout << rdWrReg2->ToString() << endl;

  WrReg *rdWrReg3 = new WrReg();
  // Setting Input Pattern
  rdWrReg3->SetChipId(chipID);
  rdWrReg3->SetAddress(address);
  rdWrReg3->SetValue(0xFF,2);
  cout << rdWrReg3->ToString() << endl;  
  
  WrReg *rdWrReg4 = new WrReg();
  // Setting Input Pattern
  rdWrReg4->SetChipId(chipID);
  rdWrReg4->SetAddress(address);
  rdWrReg4->SetValue(0xFF,3);
  cout << rdWrReg4->ToString() << endl;
  
  WrReg *rdWrReg5 = new WrReg();
  // Setting Input Pattern
  rdWrReg5->SetChipId(chipID);
  rdWrReg5->SetAddress(address);
  rdWrReg5->SetValue(0xFF,4);
  cout << rdWrReg5->ToString() << endl;  
  
  WrReg *rdWrReg6 = new WrReg();
  // Setting Input Pattern
  rdWrReg6->SetChipId(chipID);
  rdWrReg6->SetAddress(address);
  rdWrReg6->SetValue(0xFF,5);
  cout << rdWrReg6->ToString() << endl;    
  /////////////////////////////////////////////////
  cout << "----> Test RD53 Pulse" << endl;
  Pulse *rdPulse = new Pulse(0xB,0xC);
  cout << rdPulse->ToString() << endl; 

  /////////////////////////////////////////////////
  cout << "----> Test RD53 Read Register" << endl;
  RdReg *rdReadReg = new RdReg();
  chipID=0x1B;
  address=0x2C;
  // Setting Input Pattern
  rdReadReg->SetChipId(chipID);
  rdReadReg->SetAddress(address);
  cout << rdReadReg->ToString() << endl; 

  /////////////////////////////////////////////////
  cout << "----> Test RD53 BCR" << endl;
  BCR *rdABCR = new BCR();
  cout << rdABCR->ToString() << endl;   

  /////////////////////////////////////////////////
  cout << "----> Test RD53 Cal" << endl;
  Cal *rdACal = new Cal();
  chipID=0x1B;
  uint32_t edgeMode=0;  
  uint32_t edgeDelay = 0;
  uint32_t edgeWidth = 0;
  uint32_t auxMode = 0;
  uint32_t auxModeDelay = 0;
  rdACal->SetChipId(chipID);
  rdACal->SetEdgeMode(edgeMode);
  rdACal->SetEdgeMode(edgeDelay);
  rdACal->SetEdgeDelay(edgeWidth);
  rdACal->SetAuxMode(auxMode);
  rdACal->SetAuxDelay(auxModeDelay);
  cout << rdACal->ToString() << endl;    
  
     
  /////////////////////////////////////////////////
  cout << "Test RD53 Encoder" << endl;
  //create encoder
  Encoder *enc = new Encoder();
  //add a command
  cout << "Adding commands to the encoder" << endl;
  enc->AddCommand(rd);
  enc->AddCommand(rdWrReg);
  enc->AddCommand(rdWrReg2);  
  enc->AddCommand(rdWrReg3);
  enc->AddCommand(rdWrReg4);
  enc->AddCommand(rdWrReg5);
  enc->AddCommand(rdWrReg6);
  enc->AddCommand(rdPulse);
  enc->AddCommand(rdReadReg);
  enc->AddCommand(rdACal);
  
  //check the contents
  cout << "Encoded commands: " << endl;
  for(uint32_t i=0;i<enc->GetCommands().size();i++){
    cout << "-- " << enc->GetCommands()[i]->ToString() << endl; 
  }
  
  cout << "Encode" << endl;
  enc->Encode();
  
  cout << "Get the bytes (for FELIX)" << endl;
  uint8_t *bytes = enc->GetBytes();
  uint32_t len = enc->GetLength();
  cout << " Length: " << len << endl;

  cout << "Set the bytes (for emulator)" << endl;
  enc->SetBytes(bytes,len);

  cout << "Decode" << endl;
  enc->Decode();
  
  cout << "Decoded commands: " << endl;
  for(uint32_t i=0;i<enc->GetCommands().size();i++){
    cout << "-- " << enc->GetCommands()[i]->ToString() << endl; 
  }
  
  
  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency;
  
  cout << "Econding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    enc->Encode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    enc->Decode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;
  
  
  cout << "Cleaning the house" << endl;
  delete enc;
  cout << "Have a nice day" << endl;
  return 0;
}

