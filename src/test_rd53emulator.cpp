#include <cmdl/cmdargs.h>
#include <netio/netio.hpp>
#include <map>
#include <signal.h>
#include "RD53Emulator/RD53ADecoder.h"
#include "RD53Emulator/RD53AEncoder.h"

using namespace std;

netio::context *g_ctx;

void handler(int){
  cout << "You pressed ctrl+c" << endl;
  g_ctx->event_loop()->stop();
}

int main(int argc, char *argv[]){
  
  cout << "#####################################" << endl
       << "# Welcome to test_rd53emulator      #" << endl
       << "#####################################" << endl;
  
  CmdArgBool    cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr     cServer(   's',"server","number","host name. Default localhost");
  CmdArgInt     cCmdPort(  'c',"port","number","Command port number. Default 12350");
  CmdArgInt     cDataPort( 'd',"port","number","Data port number. Default 12360");
  CmdArgInt     cCmdElink( 't',"cmd-elink","number","Command elink. Default 0");
  CmdArgInt     cDataElink('r',"data-elink","number","Data elink. Default 0");
  CmdArgStr     cBackend(  'b',"backend","type","posix,rdma. Default posix");
  
  CmdLine cmdl(*argv,&cVerbose,&cServer,&cCmdPort,&cDataPort,&cCmdElink,&cDataElink,&cBackend,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string server(cServer.flags()&CmdArg::GIVEN?cServer:"localhost");
  uint32_t cmd_port=(cCmdPort.flags()&CmdArg::GIVEN?cCmdPort:12350);
  uint32_t data_port=(cDataPort.flags()&CmdArg::GIVEN?cDataPort:12360);
  uint32_t cmd_elink=(cCmdElink.flags()&CmdArg::GIVEN?cCmdElink:0);
  uint32_t data_elink=(cDataElink.flags()&CmdArg::GIVEN?cDataElink:0);

  netio::context ctx(backend.c_str());
  netio::sockcfg cfg = netio::sockcfg::cfg();
  cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, 16);
  cfg(netio::sockcfg::PAGESIZE, 2000);
  
  RD53ADecoder *dec = new RD53ADecoder();

  netio::low_latency_subscribe_socket * data_sock
    = new netio::low_latency_subscribe_socket(&ctx, [&](netio::endpoint& ep, netio::message& msg){ 
        cout << "Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
        std::vector<uint8_t> data = msg.data_copy();
        //We should remove any potential header before decoding
        dec->SetBytes(&data[0],msg.size());
        cout << "Decode" << endl;
        dec->Decode();  
        cout << "Encoded frames: " << endl;
        for(uint32_t i=0;i<dec->GetFrames().size();i++){
          cout << "-- " << dec->GetFrames()[i]->ToString() << endl; 
        }        
      });

  cout << "Subscribe to elink: " << data_elink << " at " << server << ":" << data_port << endl;
  data_sock->subscribe(data_elink, netio::endpoint(server, data_port));
  
  netio::low_latency_send_socket * cmd_sock = new netio::low_latency_send_socket(&ctx);
  cmd_sock->connect(netio::endpoint(server, cmd_port));
  
  RD53AEncoder *enc = new RD53AEncoder();
  RD53ARdReg *rd_1 = new RD53ARdReg();
  rd_1->SetChipId(0xB);
  rd_1->SetAddress(0x2C);
  RD53ARdReg *rd_2 = new RD53ARdReg();
  rd_2->SetChipId(0xC);
  rd_2->SetAddress(0x3C);
  RD53ARdReg *rd_3 = new RD53ARdReg();
  rd_3->SetChipId(0xD);
  rd_3->SetAddress(0xF);    
  enc->AddCommand(rd_1);
  enc->AddCommand(rd_2);
  //enc->AddCommand(rd_3);
  enc->Encode();  
  
  std::thread bg_thread([&ctx](){ctx.event_loop()->run_forever();});

  std::thread cmd_thread([&](){
      while(true){
        cout << "Send command: to " << cmd_elink << endl;
        netio::message *msg = new netio::message(enc->GetBytes(), enc->GetLength());
        cout <<"  ----> Size : " << msg->size() << endl; 
        cmd_sock->send(*msg);
        delete msg;

        sleep(2);
      }
    });
  
  cout << "Start loop" << endl;
  signal (SIGINT,handler);
  signal (SIGILL,handler);
  signal (SIGTERM,handler);
  g_ctx = &ctx;
  bg_thread.join();
  
  cout << "Cleaning the house" << endl;
  data_sock->unsubscribe(data_elink, netio::endpoint(server, data_port));  
  cmd_sock->disconnect();
  delete cmd_sock;
  delete data_sock;
  delete dec;

  cout << "Have a nice day" << endl;
  return 0;
}
