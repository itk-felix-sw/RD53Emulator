#include <cmdl/cmdargs.h>
#include <iostream>
#include <RD53Emulator/FrontEnd.h>
#include <RD53Emulator/Emulator.h>
#include <RD53Emulator/Hit.h>

using namespace std;
using namespace RD53A;

int main(int argc, char *argv[]){

  CmdArgIntList cPix( 'p',"pixel","pixel","pixel col [0,399] and row [0,191] ");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");

  CmdLine cmdl(*argv,&cVerbose,&cPix,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  if(cPix.count()<2 or (cPix.count()>=2 and (cPix[0]>399 or cPix[1]>191))){
    cout << "Pixel needs col and row in range col [0,399] and row [0,191]" << endl;
    return 0;
  }

  cout << "Create FrontEnd" << endl;
  FrontEnd * fe = new FrontEnd();
  fe->SetVerbose(cVerbose);

  fe->SetVerbose(false);
  fe->WritePixels();
  fe->ProcessCommands();
  fe->Clear();
  fe->SetVerbose(cVerbose);

  cout << "Enable pixel" << endl;
  fe->SetPixelEnable(cPix[0],cPix[1],true);
  fe->EnableCoreColumn(0,true);
  fe->WritePixels();
  fe->Trigger(58,10,1,1,1,1);
  fe->Trigger(58,10,1,1,1,1);

  cout << "Process commands" << endl;
  fe->ProcessCommands();

  cout << "Get cmd byte stream" << endl;
  uint8_t * bcmds = fe->GetBytes();
  uint32_t lcmds = fe->GetLength();

  cout << "Create Emulator" << endl;
  Emulator * emu = new Emulator();
  emu->SetVerbose(cVerbose);

  cout << "Handle commands" << endl;
  emu->HandleCommand(bcmds,lcmds);

  cout << "Process queue" << endl;
  emu->ProcessQueue();

  cout << "Get data byte stream" << endl;
  uint8_t * bdata = emu->GetBytes();
  uint32_t ldata = emu->GetLength();

  cout << "Handle data" << endl;
  fe->HandleData(bdata,ldata);

  cout << "Display hits" << endl;
  while(fe->HasHits()){
    cout << fe->GetHit()->ToString() << endl;
    fe->NextHit();
  }

  cout << "Cleanup the house" << endl;

  delete fe;
  delete emu;

  cout << "Have a nice day" << endl;
  return 0;
}
