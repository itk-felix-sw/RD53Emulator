#include <RD53Emulator/HitTree.h>

using namespace std;
using namespace RD53A;

HitTree::HitTree(){
  evnum=0;
  row=0;
  col=0;
  tid=0;
  ttag=0;
  bcid=0;  
  m_vcal=0;
  m_entry=0;
}

HitTree::HitTree(string name,string title){
  m_name=name;
  m_title=title;
  evnum=0;
  row=0;
  col=0;
  tid=0;
  ttag=0;
  bcid=0;
  m_vcal=0;
  m_entry=0;
  Create(name,title);
}

void HitTree::Create(string name, string title){
  m_tree = new TTree(name.c_str(),title.c_str());
  m_tree->Branch("evnum",&evnum,"evnum/i");
  m_tree->Branch("row",&row,"row/i");
  m_tree->Branch("col",&col,"col/i");
  m_tree->Branch("tid",&tid,"tid/i");
  m_tree->Branch("ttag",&ttag,"ttag/i");
  m_tree->Branch("bcid",&bcid,"bcid/i");
  m_tree->Branch("timer",&timer,"timer/f");
  m_tree->Branch("vcal", &m_vcal, "vcal/i"); 
  m_t0 = chrono::steady_clock::now();
}

HitTree::~HitTree(){}

void HitTree::Fill(Hit * hit){
  evnum = hit->GetEvNum();
  row = hit->GetRow();
  col = hit->GetCol();
  tid = hit->GetTID();
  ttag = hit->GetTTag();
  bcid = hit->GetBCID();
  timer = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-m_t0).count()/1000.;
  m_tree->Fill();
}

int HitTree::Next(){
  if(m_tree->LoadTree(m_entry)<0) return 0;
  m_tree->GetEntry(m_entry);
  m_entry++;
  return m_entry;
}

Hit * HitTree::Get(){
  m_hit.SetEvNum(evnum);
  m_hit.SetRow(row);
  m_hit.SetCol(col);
  m_hit.SetTID(tid);
  m_hit.SetTTag(ttag);
  m_hit.SetBCID(bcid);
  return &m_hit;
}

void HitTree::Write(){
  m_tree->Write();
}

void HitTree::setVcal(uint32_t vcal){
  m_vcal = vcal;
}
