#include "RD53Emulator/ReadRegisterScan.h"
#include "RD53Emulator/Tools.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TSysEvtHandler.h"

#include <iostream>
#include <chrono>
#include <signal.h>

using namespace std;
using namespace RD53A;



ReadRegisterScan::ReadRegisterScan(){
}

ReadRegisterScan::~ReadRegisterScan(){}


void ReadRegisterScan::PreRun(){

  for(auto fe : GetFEs()){
    fe->BackupConfig();
  }
}

void ReadRegisterScan::Run(){

  for(auto fe : GetFEs()){
    std::cout<<"ReadRegisterScan::Run: Comparing all the register values on the chip with the ones written in config"<<std::endl;
    //std::map<std::string,uint32_t> Registers = fe->GetConfig()->GetRegisters();
    //for(const auto &item : Registers ){
    for(uint32_t Register=0;Register<fe->GetConfig()->Size();Register++){

      uint32_t OldRegValue = fe->GetConfig()->GetRegisterValue(Register);
      fe->ReadRegister(Register); 
      Send(fe);
      //This wait is arbitrary and we can modify this. (Not sure what the minimum is)
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      uint32_t NewRegValue = fe->GetConfig()->GetRegisterValue(Register);
      //if (OldRegValue!=NewRegValue)
  	std::cout<<"   Stored Value for Register 0x"<<hex<<Register<<dec<<": 0x"<<hex<<NewRegValue<<dec<<"=/=0x"<<hex<<OldRegValue<<dec<<" (Old Value)"<<std::endl;
    }

    fe->InitAdc();
    std::cout<<"Reading Temperature and Radiation sensors"<<std::endl;
    //Will now read Temp/Radiation sensors
    for(uint8_t RadSensor=0;RadSensor<2;RadSensor++){ // O for Temp, 1 for Rad Sensor
      for(uint8_t Pos=0; Pos<4;Pos++){ //There are 4 sensors for each


	

	fe->ReadSensor(Pos,bool(RadSensor));
	Send(fe);
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	fe->ReadRegister(136); 
	Send(fe);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	if(RadSensor==1)
	  std::cout<<"  ADC Values for Radiation Sensor "<<int(Pos)<<": 0x"<<hex<<fe->GetRadiationSensor(Pos)->GetADC()<<dec<<std::endl; 
	else
	  std::cout<<"  ADC Values for Temperature Sensor "<<int(Pos)<<": "<<fe->GetTemperatureSensor(Pos)->GetTemperature()<<" Kelvin, Value: "<<fe->GetTemperatureSensor(Pos)->GetADC()<<std::endl; 
      }
    }
    




  }

}

void ReadRegisterScan::Analysis(){

  for(auto fe : GetFEs()){
    fe->RestoreBackupConfig();
  }
 


}
