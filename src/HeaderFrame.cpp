#include "RD53Emulator/HeaderFrame.h"
#include <sstream>
#include <iomanip>
#include <bitset>
#include <iostream>

using namespace std;
using namespace RD53A;

HeaderFrame::HeaderFrame(){
  m_TID = 0;
  m_TTag = 0;
  m_BCID = 0;
}

HeaderFrame::HeaderFrame(uint32_t tid, uint32_t ttag, uint32_t bcid){
  SetHeader(tid,ttag,bcid);
}

HeaderFrame::~HeaderFrame(){}

uint32_t HeaderFrame::UnPack(uint8_t * bytes, uint32_t maxlen){

  //A header starts with 0b0000001
  //The last bit of the first byte is masked out
  //if it doesn't is not a header

  bool ok=(bytes[0]>>1==0x1);

  /*  cout << "HeaderFrame: " << hex 
       << " byte: " << setw(2) << setfill('0') << (uint32_t)bytes[0] 
       << " mask: " << setw(2) << setfill('0') << (uint32_t)(bytes[0]>>1) 
       << " ok:  " << ok 
       << dec << endl;
  */
  if(!ok){return 0;}
  
  m_TID  = (bytes[0]&0x01)<<4;
  m_TID |= (bytes[1]&0xF0)>>4;
  m_TTag = (bytes[1]&0x0F)<<1;
  m_TTag|= (bytes[2]&0x80)>>7;
  m_BCID = (bytes[2]&0x7F)<<8;
  m_BCID|= (bytes[3]&0xFF)<<0;

  return 4;
}

uint32_t HeaderFrame::Pack(uint8_t * bytes){

  bytes[0]  =  0x2;
  bytes[0] |= (m_TID >>4)&0x01;
  bytes[1]  = (m_TID <<4)&0xF0;
  bytes[1] |= (m_TTag>>1)&0x0F;
  bytes[2]  = (m_TTag<<7)&0x80;
  bytes[2] |= (m_BCID>>8)&0x7F;
  bytes[3] |= (m_BCID>>0)&0xFF;

  return 4;
}

string HeaderFrame::ToString(){
  ostringstream os;
  os << "HeaderFrame "
     << " TID: "  << m_TID  << " (0x" << hex << m_TID  << dec << ")"
     << " TTag: " << m_TTag << " (0x" << hex << m_TTag << dec << ")"
     << " BCID: " << m_BCID << " (0x" << hex << m_BCID << dec << ")";
  return os.str();
}

uint32_t HeaderFrame::GetType(){
  return Frame::HEADER;
}

void HeaderFrame::SetHeader(uint32_t triggerID, uint32_t triggerTag, uint32_t BCID){
  m_TID = triggerID&0x1F;
  m_TTag = triggerTag&0x1F;
  m_BCID = BCID&0x7FFF;
}

void HeaderFrame::SetTID(uint32_t triggerID){
  m_TID = triggerID&0x1F;
}

void HeaderFrame::SetTTag(uint32_t triggerTag){
  m_TTag = triggerTag&0x1F;
}

void HeaderFrame::SetBCID(uint32_t BCID){
  m_BCID = BCID&0x7FFF;
}

uint32_t HeaderFrame::GetTID() {
  return m_TID;
}

uint32_t HeaderFrame::GetTTag() {
  return m_TTag;
}

uint32_t HeaderFrame::GetBCID() {
  return m_BCID;
}
