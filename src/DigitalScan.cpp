#include "RD53Emulator/DigitalScan.h"

#include <iostream>
#include <chrono>
#include <unistd.h>

#include "TCanvas.h"

using namespace std;
using namespace RD53A;

DigitalScan::DigitalScan(){
  m_ntrigs = 100;
  m_TriggerLatency = 58;
}

DigitalScan::~DigitalScan(){}

void DigitalScan::PreRun(){

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_ntrigs << endl;
  
  if(m_verbose){
    cout << "DigitalScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "DigitalScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }
  
  for(auto fe : GetFEs()){ 
 
    fe->BackupConfig();

    cout << "DigitalScan: Configure digital injection" << endl;
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,1);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0x4000); 
    fe->WriteGlobal();
    Send(fe);

    m_occ[fe->GetName()]   =new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);
    m_enable[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()]   =new TH1I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";TOT").c_str(), 16, 0, 16);
    m_tid[fe->GetName()]   =new TH1I(("tid_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger ID").c_str(), 16, 0, 16);
    m_ttag[fe->GetName()]  =new TH1I(("ttag_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger Tag").c_str(), 16, 0, 16);
    m_bcid[fe->GetName()]  =new TH1I(("bcid_"+fe->GetName()).c_str(),(fe->GetName()+";BCID").c_str(),0xFFF+1,-0.5,0xFFF+0.5);
    m_hits[fe->GetName()]  =new HitTree(("hits_"+fe->GetName()).c_str(),fe->GetName());
    
    cout << "DigitalScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
  }

  //Create the mask
  cout << "DigitalScan: Create the mask" << endl;
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(0,0,400,192);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency(100);
  GetMasker()->Build();
}

void DigitalScan::Run(){

  //Prepare the trigger
  cout << "DigitalScan: Prepare the TRIGGER" << endl;

  //  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1});
  PrepareTrigger(m_TriggerLatency, 2, true, true, true, true);

  //loop over mask steps
  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    cout << "DigitalScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
    GetMasker()->GetStep(st);

    //Do the mask step
    ConfigMask();
    uint32_t expected=GetMasker()->GetPixels().size();
    
    //Trigger and read-out
    for(uint32_t trig=0;trig<m_ntrigs; trig++){
      //std::this_thread::sleep_for(std::chrono::microseconds(100));
      Trigger();
    }

    //histogram hit
    bool LastLoop=false;
    uint32_t nhits=0;
    uint32_t nRhits=0;
    uint32_t nGoodHists=0;

    auto start = chrono::steady_clock::now();
    
    while(true){
      
      for(auto fe : GetFEs()){
	if(!fe->HasHits()) continue;
	LastLoop=false;
	start = chrono::steady_clock::now();
	Hit* hit = fe->GetHit();
	if(hit!=0){
	  hit->SetEvNum(st);
	  //std::cout << hit->GetTOT() << std::endl; 
	  if(hit->GetTOT()!=0x0) {
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    nRhits++;
	    
	    if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
	      cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	      // fe->NextHit();
	      // continue;
	    } else { //VD maybe add a check on TOT
	      nGoodHists++;
	    }
	    //cout << " Good hit: " << fe->GetName() << ": " << hit->ToString() << endl;
	    m_tot[fe->GetName()]->Fill(hit->GetTOT());
	    m_tid[fe->GetName()]->Fill(hit->GetTID());
	    m_ttag[fe->GetName()]->Fill(hit->GetTTag());
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	    //m_bcidmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetBCID());
	    //m_ttagmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetTTag());
	  } 
	  nhits++;
	  //tree->Set(hit);
	  //tree->Fill();
	}
	fe->NextHit();
	//
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>300 or nGoodHists>=m_ntrigs*expected*GetFEs().size() ){LastLoop=true;}
    }
    cout << " Digital: NRealHits in step: " << nRhits 
	 <<" / "<< m_ntrigs*GetMasker()->GetPixels().size()*GetFEs().size() <<" N Orig Hits "<<nhits << endl;

    //Undo the mask step
    UnconfigMask();
  
  }
}

void DigitalScan::Analysis(){
  TCanvas* can=new TCanvas("plot","plot",800,600);
  
  for(auto fe : GetFEs()){
    cout << "DigitalScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();
  
    m_occ[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    if(m_storehits){m_hits[fe->GetName()]->Write();}

    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );
    
    delete m_occ[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
    delete m_hits[fe->GetName()];
  }
  delete can;
}
