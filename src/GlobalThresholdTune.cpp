#include "RD53Emulator/GlobalThresholdTune.h"
#include "RD53Emulator/Tools.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53A;

GlobalThresholdTune::GlobalThresholdTune(){
  m_ntrigs = 100;
  m_TriggerLatency = 48;
  m_vcalMed  = 1000;
  m_vcalHigh = 1000;
  m_vcalDiff = 0;
  m_colMin   = 0;
  m_colMax   = 400;
  m_DACMax   = 400;
  m_DACMin   = 100;
  m_DACStep  = 1 ;
}

GlobalThresholdTune::~GlobalThresholdTune(){}

void GlobalThresholdTune::PreRun(){

  if((m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){
    m_whichfe=Pixel::Sync; 
    m_DACMin=100; 
    m_DACMax=600; 
    m_DACStep= 20; 
    m_colMin=Matrix::SYN_COL0; m_colMax=Matrix::SYN_COLN;
  } else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){
    m_whichfe=Pixel::Lin; 
    m_DACMin=100; 
    m_DACMax=400;    
    m_DACStep= 1; 
    m_colMin=Matrix::LIN_COL0;   m_colMax=Matrix::LIN_COLN;
    cout << " HELLO, I am in Lin " << endl;
  } else if(!(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){
    m_whichfe=Pixel::Diff; 
    m_DACMin=100; 
    m_DACMax=600; 
    m_DACStep= 20; 
    m_colMin=Matrix::DIF_COL0; m_colMax=Matrix::DIF_COLN;
    cout << " HELLO, I am in Diff " << endl;
  }
  
  m_vcalDiff = Tools::injToVcal(m_threshold);
  m_vcalHigh = m_vcalMed + m_vcalDiff;
  cout << " PRECONFIG: " << m_threshold << " correspond to VCAL diff: " << m_vcalDiff << endl;

  m_nSteps = 1 + (m_DACMax - m_DACMin) / m_DACStep;
  
  m_logFile << "ntrigs: " << m_ntrigs << endl;
  m_logFile << "latency: " << m_TriggerLatency << endl;
  m_logFile << "vcalMed: " << m_vcalMed << endl;
  m_logFile << "vcalHigh: " << m_vcalHigh << endl;
  m_logFile << "DACmin: " << m_DACMin << endl;
  m_logFile << "DACmax: " << m_DACMax << endl;
  m_logFile << "DACstep: " << m_DACStep << endl;
  m_logFile << "nsteps: " << m_nSteps << endl;


  for(auto fe : GetFEs()){

    fe->BackupConfig();

    //Configure the options for an threshold scan                                                                                     
    cout << "GlobalThresholdTune: Configure analog injection for " << fe->GetName() << endl;
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalHigh);
    fe->GetConfig()->SetField(Configuration::VCAL_MED ,m_vcalMed);
    
    if(m_whichfe == Pixel::Sync){
      cout << "GlobalThresholdTune: Configure Sync for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
    }
    else if(m_whichfe == Pixel::Lin){
      cout << "GlobalThresholdTune: Configure Lin for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
    }
    else if(m_whichfe == Pixel::Diff){
      cout << "GlobalThresholdTune: Configure Diff for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Lin,500);
    }

    fe->WriteGlobal();
    Send(fe);
    
    fe->DisableAll();
    ConfigurePixels(fe);
    
    m_occ[fe->GetName()] = new TH2I*[m_nSteps];
    for(uint32_t Step=0; Step < m_nSteps; Step++){
      uint32_t GlobalDAC = m_DACMax - Step * m_DACStep;
      m_occ[fe->GetName()][Step] = new TH2I(("occ_"+to_string(Step)+"_"+fe->GetName()).c_str(),
					    ("Occupancy map, global DAC value: "+to_string(GlobalDAC)+";Column;Row;Number of hits").c_str(), 
					    Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }

  }

  
  
  //Setting up the masking according to the Scan Type
  cout<<"Setting up mask for: " << m_colMin << " - " << m_colMax << endl;
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax+1,Matrix::NUM_ROWS);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency((m_colMax+1-m_colMin)/4);
  GetMasker()->Build();

}

void GlobalThresholdTune::Run(){

  //Prepare the trigger
  cout << "GlobalThresholdTune: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); 

  bool breakStep=false;

  for (m_iStep = 0; m_iStep < m_nSteps; m_iStep++){
    if (breakStep) break;

    //Setting the global threshold DAC value
    uint32_t GlobalDAC = m_DACMax - m_iStep * m_DACStep;
    cout << endl << "GlobalThresholdTune: global threshold DAC = " << GlobalDAC << endl;
    for(auto fe : GetFEs()) {
      fe->SetGlobalThreshold(m_whichfe,GlobalDAC);
      fe->WriteGlobal();
      Send(fe);
    }

    unsigned NPixels=0;
    for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
      if (st%8!=0) continue;
      //count++;
      GetMasker()->GetStep(st);
      ConfigMask();
      NPixels+=GetMasker()->GetPixels().size();
      cout << "                         Mask Step: " << st << " with nPixel: " << NPixels << endl; 

      //Trigger and read-out
      for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();

      
      auto start = chrono::steady_clock::now();
      
      //histogram hit
      uint32_t nhits=0;
      bool LastLoop=false;
      uint32_t tmpTOT=0;
      while(true){
	
	for(auto fe : GetFEs()){
	  if(!fe->HasHits()) continue;
	  LastLoop=false;
	  Hit* hit = fe->GetHit();
	  if(hit!=0) {
	    tmpTOT=hit->GetTOT();
	    
	    if( tmpTOT == 0xF) {
	      fe->NextHit();
	      continue;
	    }
	    m_occ[fe->GetName()][m_iStep]->Fill(hit->GetCol(),hit->GetRow());
	    nhits++;
	  }
	  fe->NextHit();
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if(LastLoop) break;
	if(ms>100){LastLoop=true;}
      }
      UnconfigMask();
    }
  

    for(auto fe: GetFEs()){
      double meanTh = m_occ[fe->GetName()][m_iStep]->Integral()/NPixels;
      std::cout << __PRETTY_FUNCTION__  << "Integral / NPixels: "<< m_occ[fe->GetName()][m_iStep]->Integral()<< "/"<<NPixels << " => mean Occ: " << meanTh/m_ntrigs*100 << " % at m_iStep: " << m_iStep << endl;
    
      /*
	The DAC value for which the mean occupancy is 50% of m_ntrigs corresponds
	to the threshold target. Therefore we stop the scan when 
	m_meanTh>0.5*m_ntrigs and then we find the DAC value for which 
	m_meanTh=0.5*m_ntrigs by linear interpolation between the last DAC value
	and the previous one.
      */ 
      if(meanTh > 0.5 * m_ntrigs) {
	std::cout<<"Breaking Mean Thr: "<< meanTh<<" Expect Triggers "<<0.5 * m_ntrigs<<std::endl;
	breakStep=true;
	//we should skip this front-end from the next loop
      }
    }
  }
}

void GlobalThresholdTune::Analysis(){
  

  for(auto fe : GetFEs()){
  
    //Doing Some input checks. 
    if(m_nSteps==m_iStep){ //If m_nSteps is m_iStep it means, for loop finnished naturally. This means the for loop push m_iSteps to outof range and this needs to be fixed.
      m_iStep+=-1; 
      std::cout<<__PRETTY_FUNCTION__<<" WARNING!!!! the DAC Range of is not enough, please decrease DAC Min"<<std::endl;

    } 
    else if(0==m_iStep) { //If m_iSteps is 0 it means, for loop finnished at first loop. This will cause issue with the remaining function. So will add one to prevent issues. 
      m_iStep+=1;
      std::cout<<__PRETTY_FUNCTION__<<" WARNING!!!! the DAC Range of is not enough, please increase DAC Max"<<std::endl;
    }


    std::cout<<"Writting Occupancy plots"<<std::endl;
    for(uint32_t Step=0; Step < m_nSteps; Step++){ 
      m_occ[fe->GetName()][Step]->Write();
    }
    
    
    //linear interpolation
    float meanOccTarg = 0.5 * m_ntrigs;
    uint32_t DAC1 = m_DACMax - (m_iStep - 1) * m_DACStep; //second to last value 
    uint32_t DAC2 = m_DACMax - m_iStep * m_DACStep; //last value
    std::cout<<"DAC1: "<<DAC1<<" DAC2: "<<DAC2<<" iStep: "<<m_iStep<<"/"<<m_nSteps <<" m_DACStep "<<m_DACStep<<std::endl;
    
    float meanOcc1 = m_occ[fe->GetName()][m_iStep-1]->Integral()/(Matrix::NUM_ROWS*(m_colMax - m_colMin));
    float meanOcc2 = m_occ[fe->GetName()][m_iStep]->Integral()/(Matrix::NUM_ROWS*(m_colMax - m_colMin));
    std::cout<< "Mean Occ1 : "<<meanOcc1<<" Mean Occ2 :"<<meanOcc2<<" Mean Occ Target "<<meanOccTarg<<std::endl;
    uint32_t finalDAC = round(DAC1 + (meanOccTarg - meanOcc1) * signed((DAC2 - DAC1))/(meanOcc2 - meanOcc1));
    //min/max??
    std::cout << __PRETTY_FUNCTION__ << "m_iStep: " << m_iStep << " Occ target: " << meanOccTarg << " DAC1: " << DAC1 << " DAC2: " << DAC2 << " meanOcc1: " << meanOcc1 << " meanOcc2: " << meanOcc2 << " finalDAC: " << finalDAC <<endl;
    
    fe->RestoreBackupConfig();
    fe->SetGlobalThreshold(m_whichfe,finalDAC);
    fe->WriteGlobal();
    Send(fe);
    
    for(uint32_t Step=0; Step < m_nSteps; Step++){
      delete m_occ[fe->GetName()][Step];
    }
    delete[] m_occ[fe->GetName()];
  }
}
