#include "RD53Emulator/PreampTune.h" 
#include "RD53Emulator/Tools.h" 

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53A;

PreampTune::PreampTune(){
  m_mask_step = 4;
  m_cc_max = 4;
  m_ntrigs = 10;
  m_TriggerLatency = 48;
  m_DACMax = 100;
  m_DACMin = 0;
  m_DACStep = 5;
  m_nSteps = 1 + (m_DACMax - m_DACMin) / m_DACStep;
  m_vcalMed = 500;
  m_vcalDiff = 0;
  m_vcalHigh = m_vcalMed + m_vcalDiff;
  m_ToTtarget = 8;
  m_colMin = 0;
  m_colMax = 400;
  m_PreviousToTDiff = 10000.;
  m_ToTDiff = 0.;
  m_PreviousMeanToT = 0.;
  m_meanToTValue = 0.;
  m_endLoop = false;
  m_iStep = 0;

  if(m_verbose){
    std::cout << __PRETTY_FUNCTION__ << ": m_mask_step = " << m_mask_step << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_cc_max = " << m_cc_max << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_ntrigs = " << m_ntrigs << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_DACMax = " << m_DACMax << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_DACStep = " << m_DACStep << std::endl;
  }
} 

PreampTune::~PreampTune(){}

void PreampTune::PreRun(){
  if(m_scan.find("PreampSyn")!=string::npos) m_DACMax = 60;
  m_vcalDiff = Tools::injToVcal(m_charge);
  m_vcalHigh = m_vcalMed + m_vcalDiff;
  m_ToTtarget = m_ToT;
  
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Sync));
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Lin));
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Diff));
    if(m_scan.find("PreampSyn")!=string::npos){
      m_colMin = 0; m_colMax = 128; //DAC, ~(700-2000) e-
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0x4000);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
    }
    else if(m_scan.find("PreampLin")!=string::npos){
      m_colMin = 128; m_colMax = 264; //DAC, ~(700-2000) e-
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
    }
    else if(m_scan.find("PreampDiff")!=string::npos){
      m_colMin = 264; m_colMax = 400; //DAC, ~(700-2000) e-
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Lin,500);
    }
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalHigh);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->WriteGlobal();
    Send(fe);
    m_occ[fe->GetName()]=new TH2I*[m_nSteps];
    m_tot[fe->GetName()]=new TH2I*[m_nSteps];
    m_meantot[fe->GetName()]=new TH2F*[m_nSteps];
    for(uint32_t Step=0; Step<m_nSteps; Step++){
      uint32_t DACValue = m_DACMax - m_DACStep * Step;
      m_occ[fe->GetName()][Step] = new TH2I(("occ_"+to_string(Step)+"_"+fe->GetName()).c_str(),("Occupancy map, DAC="+to_string(DACValue)+";Column;Row;Number of hits").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
      m_tot[fe->GetName()][Step] = new TH2I(("tot_"+to_string(Step)+"_"+fe->GetName()).c_str(),("ToT sum map, DAC="+to_string(DACValue)+";Column;Row;ToT #left[bc#right]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
      m_meantot[fe->GetName()][Step] = new TH2F(("meantot_"+to_string(Step)+"_"+fe->GetName()).c_str(),("Mean ToT map, DAC="+to_string(DACValue)+";Column;Row;Mean ToT #left[bc#right]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }

    m_logFile << "\"m_mask_step\": " << m_mask_step << endl;
    m_logFile << "\"m_cc_max\": " << m_cc_max << endl;
    m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
    m_logFile << "\"m_DACMax\": " << m_DACMax << endl;
    m_logFile << "\"m_DACMin\": " << m_DACMin << endl;
    m_logFile << "\"m_DACStep\": " << m_DACStep << endl;
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": added front-end with chip ID " << fe->GetChipID() << std::endl;
  }

  std::cout<<"Setting up mask for: "<<m_colMin<<" - "<<m_colMax<<std::endl;
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax,192);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency((m_colMax-m_colMin)/4);
  GetMasker()->Build();



}

void PreampTune::Run(){

  //Prepare the trigger
  cout << __PRETTY_FUNCTION__ << "Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // The Analog scans requirs many mo

  for(m_iStep = 0; m_iStep<m_nSteps; m_iStep++){
    //Setting the DAC value
    uint32_t DACValue = m_DACMax - m_iStep * m_DACStep;
    std::cout << __PRETTY_FUNCTION__ << ": preamplifier DAC value = " << DACValue << std::endl;
    for(auto fe : GetFEs()) {
      if(m_scan.find("PreampSyn")!=string::npos){
	fe->GetConfig()->SetField(Configuration::IBIAS_KRUM_SYNC,DACValue); m_whichfe = "syn"; m_colMin=0; m_colMax=128;}
      else if(m_scan.find("PreampLin")!=string::npos){
        fe->GetConfig()->SetField(Configuration::KRUM_CURR_LIN,DACValue); m_whichfe = "lin"; m_colMin=128; m_colMax=264;}
      else if(m_scan.find("PreampDiff")!=string::npos){
        fe->GetConfig()->SetField(Configuration::VFF_DIFF,DACValue); m_whichfe = "diff"; m_colMin=264; m_colMax=400;}
      fe->WriteGlobal();
      Send(fe);
    }

    //change the mask
    for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
      
      //cout << "AnalogScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
      GetMasker()->GetStep(st);
      ConfigMask();


      uint32_t nhits2=0;
        
      //Trigger and read-out
      for(uint32_t trig=0;trig<m_ntrigs; trig++){
          
	Trigger();
	auto start = chrono::steady_clock::now();
          
	//histogram hit
	uint32_t nhits=0;
	while(true){
            
	  for(auto fe : GetFEs()){
	    if(!fe->HasHits()) continue;
	    Hit* hit = fe->GetHit();
	    if(hit!=0 && hit->GetTOT()!=0x0 ){
	      m_occ[fe->GetName()][m_iStep]->Fill(hit->GetCol(),hit->GetRow());
	      m_tot[fe->GetName()][m_iStep]->Fill(hit->GetCol(),hit->GetRow(),hit->GetTOT());
	      nhits++;
	    }
	    fe->NextHit();
	  }
	  auto end = chrono::steady_clock::now();
	  uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	  if(ms>150){break;}
	}
	nhits2+=nhits;

      }
      cout << __PRETTY_FUNCTION__ << "NHits in step: " << nhits2 << endl;      
      UnconfigMask();
    }
    

    //mean ToT map
    double sumtot = 0.;
    uint32_t totalhit = 0;
 
    for(auto fe : GetFEs()){
      m_occ[fe->GetName()][m_iStep]->Write();
      for(uint32_t row=0; row<Matrix::NUM_ROWS; row++){
	for(uint32_t col=m_colMin; col<m_colMax; col++){
	  sumtot = m_tot[fe->GetName()][m_iStep]->GetBinContent(col+1,row+1);
	  totalhit = m_occ[fe->GetName()][m_iStep]->GetBinContent(col+1,row+1);
	  if (totalhit == 0) m_meantot[fe->GetName()][m_iStep]->Fill(col,row,0.);
	  else {
	    m_meantot[fe->GetName()][m_iStep]->Fill(col,row,sumtot/totalhit);
	  }
	}
      }
      m_meanToTValue = m_meantot[fe->GetName()][m_iStep]->Integral()/(Matrix::NUM_ROWS*(m_colMax-m_colMin));
      m_meantot[fe->GetName()][m_iStep]->Write();
    }
    m_ToTDiff = (abs(m_meanToTValue - m_ToTtarget));
    std::cout << __PRETTY_FUNCTION__ << "m_iStep: " << m_iStep << " DAC: " << DACValue << " mean ToT: " << m_meanToTValue << endl;
    if (m_iStep == m_nSteps-1) m_endLoop = true;
    /* We break the loop when the difference between the measured ToT and the
       target one is higher than the one at the previous step */
    if (m_ToTDiff > m_PreviousToTDiff) break; 
    else if(m_ToTDiff <= m_PreviousToTDiff and !m_endLoop){m_PreviousToTDiff = m_ToTDiff; m_PreviousMeanToT = m_meanToTValue;}
  }
}

void PreampTune::Analysis(){
  /* We find the DAC value that corresponds to the ToT target by 
     interpolation between the last two DAC values */
  if(m_endLoop) m_iStep = m_nSteps - 1; 
  uint32_t DAC1 = m_DACMax - (m_iStep - 1) * m_DACStep; //second to last value
  uint32_t DAC2 = m_DACMax - m_iStep * m_DACStep; //last value
  float meanToT1 = m_PreviousMeanToT;
  float meanToT2 = m_meanToTValue;
  int DACTarget = round(DAC1 + (m_ToTtarget-meanToT1)*signed((DAC2-DAC1))/(meanToT2-meanToT1));
  std::cout << __PRETTY_FUNCTION__ << ": DAC1: " << DAC1 << " DAC2: " << DAC2 << " meanToT1: " << meanToT1 << " meanToT2: " << meanToT2 << " DACTarget: " << DACTarget << endl;
  if(DACTarget < (int)m_DACMin) DACTarget = m_DACMin;
  if(DACTarget > (int)m_DACMax) DACTarget = m_DACMax;
  std::cout << __PRETTY_FUNCTION__  << ": Final DAC: " << DACTarget << endl;
  for(auto fe : GetFEs()){                                                                                                                
    if(m_scan.find("PreampSyn")!=string::npos){fe->GetConfig()->SetField(Configuration::IBIAS_KRUM_SYNC,DACTarget);}
    else if(m_scan.find("PreampLin")!=string::npos){fe->GetConfig()->SetField(Configuration::KRUM_CURR_LIN,DACTarget);}
    else if(m_scan.find("PreampDiff")!=string::npos){fe->GetConfig()->SetField(Configuration::VFF_DIFF,DACTarget);}
    fe->SetGlobalThreshold(Pixel::Sync,m_mem_thres[fe->GetName()][0]);
    fe->SetGlobalThreshold(Pixel::Lin,m_mem_thres[fe->GetName()][1]);
    fe->SetGlobalThreshold(Pixel::Diff,m_mem_thres[fe->GetName()][2]);
    fe->WriteGlobal();
    Send(fe);
    for(uint32_t step=0; step<m_iStep; step++){
      delete m_occ[fe->GetName()][step];
    }
    delete[] m_occ[fe->GetName()];
  }
  m_logFile << "\"m_iStep (number of steps taken)\": " << m_iStep << endl;
  m_logFile << "\"DAC1\": " << DAC1 << endl;
  m_logFile << "\"DAC2\": " << DAC2 << endl;
  m_logFile << "\"meanToT1\": " << meanToT1 << endl;
  m_logFile << "\"meanToT2\": " << meanToT2 << endl;
}
