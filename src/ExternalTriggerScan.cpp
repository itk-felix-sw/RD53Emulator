#include "RD53Emulator/ExternalTriggerScan.h"
#include "RD53Emulator/Tools.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TSysEvtHandler.h"

#include <iostream>
#include <chrono>
#include <signal.h>

using namespace std;
using namespace RD53A;

bool ExternalTriggerScan::m_scanStop = false;

/*
void handler(int) {
  cout << " LEYRE DIXIT: bye ... " << endl;
}

class THandler: public TSignalHandler {
public:
  THandler(ESignals sig): TSignalHangler(sig, false) {}
  virtual Bool_t Notify() {
    handler(0);
    return false;
  }
}
*/

TApplication* theAppETS;//("myApp",0,0);

ExternalTriggerScan::ExternalTriggerScan(){
  m_TriggerLatency = 4;
  m_TriggerFrequency = 40e3;
  m_colMin = 0;
  m_colMax = 400;
}

ExternalTriggerScan::~ExternalTriggerScan(){}

void ExternalTriggerScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_scanStop = true;
}

void ExternalTriggerScan::PreRun(){

  for(auto fe : GetFEs()){

    cout << "ExternalTriggerScan: Configure" << endl;
    cout << "TRIGGER LATENCY IS : " << m_latency << endl;
    
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_latency);

    if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){
      m_colMin = Matrix::SYN_COL0; 
      m_colMax = Matrix::NUM_COLS; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
    }else if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){ //synlindiff
      m_colMin = Matrix::SYN_COL0; 
      m_colMax = Matrix::LIN_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->SetGlobalThreshold(Pixel::Diff,Tools::chargeToThr(100000,Pixel::Diff));
    }else if((m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){//syn
      m_colMin = Matrix::SYN_COL0; 
      m_colMax = Matrix::SYN_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1 ,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2 ,0);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
    }else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){//lindiff
      m_colMin = Matrix::LIN_COL0;
      m_colMax = Matrix::NUM_COLS;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
    }else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){ //lin
      m_colMin = Matrix::LIN_COL0; 
      m_colMax = Matrix::LIN_COLN+1;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
    }else if(!(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){ //Diff
      m_colMin = Matrix::DIF_COL0; 
      m_colMax = Matrix::DIF_COLN+1;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);//Tools::chargeToThr(500,Pixel::Sync));
      fe->SetGlobalThreshold(Pixel::Lin, 500);//Tools::chargeToThr(500,Pixel::Lin));
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC ,0);
    }else{
      m_colMin = Matrix::LIN_COL0;
      m_colMax = Matrix::NUM_COLS;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,Tools::chargeToThr(500,Pixel::Sync));
    }

    fe->WriteGlobal();
    Send(fe);
    //fe->EnableAll();
    //fe->DisableAll();
    if (m_maskopt){
      fe->ApplyMasking();
    } else {
      fe->EnableAll();
    }  

    // Special maskings, currently disabled

    // if ( m_scan_fe & DiffFE ) {
    //   for (uint32_t col=Matrix::DIF_COL0;col<Matrix::NUM_COLS;col++){
    // 	for (uint32_t row=140;row<Matrix::NUM_ROWS;row++){
    // 	  fe->SetPixelEnable(col,row,false);
    // 	}
    //   } 
    // }

    // if ( m_scan_fe & SyncFE ) {
    //   for (uint32_t col=0;col<Matrix::SYN_COLN+1;col++){
    // 	for (uint32_t row=100;row<Matrix::NUM_ROWS;row++){
    // 	  fe->SetPixelEnable(col,row,false);
    // 	}
    //   } 
    // }


    //for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
    //  for(uint32_t col = m_colMin       ; col < m_colMax; col++) fe->SetPixelEnable(col,row,true);
    //}
    ConfigurePixels(fe);
    
    //Create histograms
    m_occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_occHigh[fe->GetName()]=new TH2I(("occH_"+fe->GetName()).c_str(),";Column;Row;Number of Hi hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);

    m_enable[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),"Mask map;Column;Row;Masked", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()] =new TH1I(("tot_"+fe->GetName()).c_str(),"ToT"  , 16, -0.5, 15.5);
    m_bcid[fe->GetName()]=new TH1I(("bcid_"+fe->GetName()).c_str(),"BCID", 32, -0.5, 31.5);

    m_triggers = new TH1I("triggers", "Number of triggers", 1, 0.5, 1.5);
    m_deltaBCID[fe->GetName()]=new TH2I(("deltaBCID_"+fe->GetName()).c_str(),"DeltaBCID", 32, -0.5, 31.5, 16, -0.5, 15.5);
  }

  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax,192);
  GetMasker()->SetShape(m_colMax-m_colMin,192);
  GetMasker()->Build();
  GetMasker()->GetStep(0);

  bool enable=true;
  //ConfigMask();
  for(auto fe : GetFEs()){
    for(auto cc : GetMasker()->GetCoreColumns()){
      fe->EnableCoreColumn(cc,enable);
      fe->EnableCalCoreColumn(cc,enable);
    }
    for(auto dc : GetMasker()->GetDoubleColumns()){
      fe->EnableCalDoubleColumn(dc,enable);
    }
    fe->ProcessCommands();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));

  }

}

void ExternalTriggerScan::Run(){

  gROOT->SetBatch(false);

  theAppETS=new TApplication("myApp",0,0);
  TCanvas* myC=new TCanvas("can","can", 1000*GetFEs().size(),1000);
  myC->Divide(2,2);
    
  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // bmdebug test
 

  Encoder encoder;
  encoder.AddCommand(new Sync());
  encoder.AddCommand(new Sync());
  
  //old code (works for lin/diff)
  encoder.AddCommand(new Pulse());   
  for(uint32_t i=0;i<64;i++)  encoder.AddCommand(new Noop());
  encoder.AddCommand(new BCR());
  for(uint32_t i=0;i<1;i++){
    encoder.AddCommand(new RD53A::Trigger(1,1,1,1,0xA));
    encoder.AddCommand(new RD53A::Trigger(1,1,1,1,0x5));
  }
  for(uint32_t i=0;i<32;i++)  encoder.AddCommand(new Noop()); //OLD
 

  //newcode
  /*
  encoder.AddCommand(new Pulse(8,8));
  for(uint32_t i=0;i<32;i++)  encoder.AddCommand(new Noop());
  //if (m_scan_fe & SyncFE) 
  encoder.AddCommand(new ECR());
  encoder.AddCommand(new BCR());
  for(uint32_t i=0;i<11;i++)        encoder.AddCommand(new Noop());
  for(uint32_t i=0;i<3;i++)         encoder.AddCommand(new Noop()); //for Cal
  for(uint32_t i=0;i<(48/4)-3;i++)  encoder.AddCommand(new Noop()); //latency
  for(uint32_t i=0;i<1;i++){
    encoder.AddCommand(new RD53A::Trigger(1,1,1,1,0xA));
    encoder.AddCommand(new RD53A::Trigger(1,1,1,1,0x5));
    //encoder.AddCommand(new RD53A::Trigger(0,0,1,0,0x4));
    //encoder.AddCommand(new RD53A::Trigger(0,0,1,1,0x5));
  }
  for(uint32_t i=0;i<6;i++)         encoder.AddCommand(new Noop()); //for Cal
  */
  
  //PrepareTrigger(&encoder);

  //double period=1e6/m_TriggerFrequency;

  //int nHitsV=0;
  // //cleanup memory before the scan
  // do {
  //   nHitsV = 0;
  //   for(auto fe : GetFEs()){
  //     if(!fe->HasHits()) continue;
  //     Hit* hit = fe->GetHit();
  //     if(hit!=0 && hit->GetTOT()!=0x0) nHitsV++;
  //     fe->NextHit();
  //   }
  // }while (nHitsV > 0);

  
  auto scan_start = chrono::steady_clock::now();
  
  m_scanStop = false;
  signal(SIGINT, StopScan);
  signal(SIGTERM, StopScan);
  signal(SIGILL, StopScan);
  cout << "Please press Ctrl+C to stop the scan" << endl;

  //THandler theHandler1(kSigInterrupt);
  //THandler theHandler2(kSigTermination);
  //theHandler1.Add();
  //theHandler2.Add();
  /*
  TApplication theApp("myApp",0,0);
  TCanvas* myC=new TCanvas("can","can", 2500,600);
  myC->Divide(GetFEs().size(),1);
  */

  uint32_t nhits_total = 0;
  uint32_t ev=0;
  while(!m_scanStop){

    //    std::this_thread::sleep_for(std::chrono::microseconds((1000)));

    // experimental, try this
    // if(ev % 10000 == 0){
    //    for(auto fe : GetFEs()){
    // 	  fe->Reset(5);
    //      fe->Reset(5);
    //      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
    //      Send(fe);  
    //    }
    // }

    //Maybe config mask?

    //auto start_read = chrono::steady_clock::now();

    uint32_t nhits = 0;
    do {
      nhits = 0;
      for(auto fe : GetFEs()){
        if(!fe->HasHits()) continue;
        Hit* hit = fe->GetHit();
	//cout << "--> " << hit->ToString() << endl;
        if(hit!=0 && hit->GetTOT()!=0x0){
	  //	  cout << "We are having a hit with TOT= " << hit->GetTOT() << endl;
	  //	  cout << hit->ToString() << endl;
	  //cout << "Delta = " << hit->GetBCID() << ", TOT = " << hit->GetTOT() << endl;
	  
	  m_tot[fe->GetName()]->Fill(hit->GetTOT());
	  m_bcid[fe->GetName()]->Fill(hit->GetBCID());

	  if (hit->GetTOT()>10) m_occHigh[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
          m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
          nhits++;
	  nhits_total++;
	  //if (hit->GetCol()>140) cout << "--> " << hit->ToString() << endl;
        }
        fe->NextHit();
      }

      // auto end_read = chrono::steady_clock::now();
      // uint32_t ms = chrono::duration_cast<chrono::milliseconds>(end_read - start_read).count();
      // if(ms>30) break;

    }while (nhits > 0);
    
    
    if( ev % 100000 == 0 ){ //100000
      //auto current_time = chrono::steady_clock::now();
      //      cout << "Run time:: "<< chrono::duration_cast<chrono::minutes>(current_time - scan_start).count() << "min ; NHits in trigger # " << ev  << ": " << nhits_total << "      "; // << endl;
      nhits_total = 0;
      int c=0;
      for(auto fe : GetFEs()){
      	c+=1;
      	myC->cd(1);	
	m_occ[fe->GetName()]->SetMaximum(50);
      	m_occ[fe->GetName()]->Draw("COLZ");
	myC->cd(2);	
	m_occHigh[fe->GetName()]->SetMaximum(50);
      	m_occHigh[fe->GetName()]->Draw("COLZ");
	myC->cd(3);	
      	m_tot[fe->GetName()]->Draw("HIST");
	myC->cd(4);	
      	m_bcid[fe->GetName()]->Draw("HIST");
	//cout << " FE_" << c << ": " << m_occ[fe->GetName()]->Integral();
      }
      //cout << endl;
      myC->Update();
      this_thread::sleep_for(chrono::microseconds( 100000 ));
      //exit(-1);
    }
    //this_thread::sleep_for(chrono::microseconds( (int)period ));
    ev++;
  }
  
  //UnconfigMask();
  
  cout << "Total scan time: " << chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - scan_start).count() << " s" << endl;
}

void ExternalTriggerScan::Analysis(){
  gROOT->SetBatch(true);
  TCanvas* can=new TCanvas("plot","plot",800,600);

  for(auto fe : GetFEs()){

    //get total # of hits
    float thres = m_triggers->GetBinContent(1)*8e-6; //currently threshold = 1e-6 per BC
    //create a mask for noisy pixels, mask everything > ntot/npix
    if (m_maskopt == 0){
       for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
         for(uint32_t col = m_colMin; col < m_colMax; col++){
           if (m_occ[fe->GetName()]->GetBinContent(col+1,row+1) < thres){
	     fe->SetPixelEnable(col,row,true);
	     m_enable[fe->GetName()]->SetBinContent(col+1,row+1,1);
	   }else{
	     fe->SetPixelEnable(col,row,false);
             m_mask[fe->GetName()]->SetBinContent(col+1,row+1,1);
	     if (m_triggers->GetBinContent(1)>1e6) cout << " FE: " << fe->GetName() << " --> Masking pixel: " << col << " , " << row << endl;
	   }
         }
       }
       //m_enable=m_mask;
    }

    m_mask[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_occ[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_triggers->Write();


    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );

    m_enable[fe->GetName()]->SetMaximum(2);
    m_enable[fe->GetName()]->SetMinimum(0);
    m_enable[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__ENABLE.pdf").c_str() );

    m_mask[fe->GetName()]->SetMaximum(2);
    m_mask[fe->GetName()]->SetMinimum(0);
    m_mask[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__MASKED.pdf").c_str() );

    m_tot[fe->GetName()]->Draw("hist");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOT.pdf").c_str() );

    delete m_occ[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_mask[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_deltaBCID[fe->GetName()];
  }

  delete can;

}
