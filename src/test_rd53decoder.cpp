#include "RD53Emulator/Decoder.h"
#include <iostream>
#include <ctime>

using namespace std;
using namespace RD53A;

int main() {

  cout << "Test RD53 Decoder" << endl;
  
  //create decoder
  Decoder * dec=new Decoder();
  
  //add frames
  cout << "Adding frames to the encoder" << endl;
  dec->AddFrame(new HeaderFrame(0,0,1234));
  dec->AddFrame(new HitFrame(22,10,1,5,5,5,5));
  dec->AddFrame(new HeaderFrame(0,1,1235));
  dec->AddFrame(new HitFrame(40,12,1,7,7,7,7));
  dec->AddFrame(new HitFrame(40,13,1,7,7,7,7));
  dec->AddFrame(new HitFrame(40,14,1,7,7,7,7));
  dec->AddFrame(new HeaderFrame(0,2,1236));
  dec->AddFrame(new HeaderFrame(0,3,1237));
  dec->AddFrame(new InvalidFrame());

  //check the contents
  cout << "Initial frames: " << endl;
  for(uint32_t i=0;i<dec->GetFrames().size();i++){
    cout << "-- " << dec->GetFrames()[i]->ToString() << endl; 
  }
  cout << "Encode" << endl;
  dec->Encode();
  cout << "Byte string: " << dec->GetByteString() << endl;
  
  cout << "Get the bytes (from decoder)" << endl;
  uint8_t *bytes = dec->GetBytes();
  uint32_t len = dec->GetLength();
  cout << " Length: " << len << endl;

  cout << "Set the bytes (from FELIX)" << endl;
  dec->SetBytes(bytes,len);

  cout << "Decode" << endl;
  dec->Decode();
  cout << "Byte string: " << dec->GetByteString() << endl;
  cout << "Decoded frames: " << endl;
  for(auto frame : dec->GetFrames()){
    cout << "-- " << frame->ToString() << endl; 
  }

  cout << "Byte string: " << dec->GetByteString() << endl;
  cout << "Encode again" << endl;
  dec->Encode();
  cout << "Byte string: " << dec->GetByteString() << endl;
  cout << "Decoded frames: " << endl;
  for(auto frame : dec->GetFrames()){
    cout << "-- " << frame->ToString() << endl; 
  }

  cout << "Decode again" << endl;
  dec->Decode();
  cout << "Byte string: " << dec->GetByteString() << endl;
  cout << "Decoded frames: " << endl;
  for(auto frame : dec->GetFrames()){
    cout << "-- " << frame->ToString() << endl; 
  }
  
  
  
  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency;
  
  cout << "Econding performance: nframes:" << dec->GetFrames().size() << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    dec->Encode(); 
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();	
  for(uint32_t i=0; i<n_test; i++){
    dec->Decode();    
  }

  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;
  
  cout << "Cleaning the house" << endl;
  delete dec;

  cout << "Have a nice day" << endl;
  
  return 0;
}

