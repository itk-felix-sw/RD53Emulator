#include "RD53Emulator/FrontEnd.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <thread>
#include <math.h> 

using namespace std;
using namespace RD53A;

FrontEnd::FrontEnd(){
  m_decoder = new Decoder();
  m_encoder = new Encoder();
  m_config  = new Configuration();
  m_matrix  = new Matrix();
  m_verbose = 1;
  m_chipid = 16;
  m_name = "RD53A";
  m_active=true;
  m_dcs_node = "";
  for(uint32_t i=0;i<4;i++){
    m_ntcs.push_back(new TemperatureSensor());
    m_bjts.push_back(new RadiationSensor());
  }
  m_hit = new Hit();
  m_refBCID = 0;
}

FrontEnd::~FrontEnd(){
  delete m_decoder;
  delete m_encoder;
  delete m_config;
  delete m_matrix;
  delete m_hit;
  for(uint32_t i=0;i<4;i++){
    delete m_ntcs[i];
    delete m_bjts[i];
  }
}

void FrontEnd::SetVerbose(bool enable){
  m_verbose = enable;
}

void FrontEnd::SetChipID(uint32_t chipid){
  m_chipid=chipid;
}

uint32_t FrontEnd::GetChipID(){
  return m_chipid;
}

void FrontEnd::SetName(string name){
  m_name=name;
}

string FrontEnd::GetName(){
  return m_name;
}

void FrontEnd::SetDcsNode(string dcs_node){
  m_dcs_node = dcs_node;
}

string FrontEnd::GetDcsNode(){
  return m_dcs_node;
}

Configuration * FrontEnd::GetConfig(){
  return m_config;
}

void FrontEnd::BackupConfig(){
  m_config->Backup();
  m_matrix->Backup();
}

void FrontEnd::RestoreBackupConfig(){ 
  m_config->Restore();
  m_matrix->Restore();
}

void FrontEnd::Clear(){
  m_encoder->Clear();
}

void FrontEnd::Reset(uint32_t res){
  if(res == 0) {
    //Write the global pulse route to reset a bunch of things
    uint32_t val = 0;
    val |= Configuration::GP_RESET_CHN;
    val |= Configuration::GP_RESET_CMD;
    val |= Configuration::GP_RESET_CONFIG;
    val |= Configuration::GP_RESET_MON;
    val |= Configuration::GP_RESET_AURORA;
    val |= Configuration::GP_RESET_SERILIZER;
    val |= Configuration::GP_RESET_ADC;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GLOBAL_PULSE_RT, val));
    //m_config->GetField(Configuration::GLOBAL_PULSE_RT)->SetValue(val);
  }
  else if (res == 1) {
    //Send the global pulse to reset what was configured before
    m_encoder->AddCommand(new Pulse(m_chipid, 8));
  }
  else if (res == 2) {
    //Send a sequence of 32 sync commands
    for(uint32_t i = 0; i < 32; i++){
      m_encoder->AddCommand(new Noop());
      m_encoder->AddCommand(new Sync());
    }
  }
  else if (res == 3) {
    //activate monitor and prime sync FE AZ
    uint32_t val = 0;
    val |= Configuration::GP_START_MON;
    val |= Configuration::GP_START_ZERO_LEVEL;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GLOBAL_PULSE_RT, val));
    //m_config->GetField(Configuration::GLOBAL_PULSE_RT)->SetValue(val);
  }
  else if (res == 4) {
    //Send the global pulse to reset what was configured before
    m_encoder->AddCommand(new Pulse(m_chipid, 8));
  }
  else if (res == 5) {
    m_encoder->AddCommand(new ECR());
    m_encoder->AddCommand(new Noop());
  }
  else if (res == 6) {
    m_encoder->AddCommand(new BCR());
    m_encoder->AddCommand(new Noop());
  }
}

bool FrontEnd::IsActive(){
  return m_active;
}

void FrontEnd::SetActive(bool active){
  m_active=active;
}

void FrontEnd::EnableAll(){
  for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      m_matrix->GetPixel(col,row)->SetEnable(true);
      m_matrix->GetPixel(col,row)->SetInject(true);
    }
  }
}


void FrontEnd::DisableAll(){
  for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      m_matrix->GetPixel(col,row)->SetEnable(false);
      m_matrix->GetPixel(col,row)->SetInject(false);
    }
  }
}



uint32_t FrontEnd::GetGlobalThreshold(uint32_t col, uint32_t /*row*/){
  if(col>=Matrix::SYN_COL0 and col <= Matrix::SYN_COLN){
    return m_config->GetField(Configuration::VTH_SYNC)->GetValue();
  }else if(col>=Matrix::LIN_COL0 and col <= Matrix::LIN_COLN){
    return m_config->GetField(Configuration::VTH_LIN)->GetValue();
  }else if(col>=Matrix::DIF_COL0 and col <= Matrix::DIF_COLN){
    return m_config->GetField(Configuration::VTH1_DIFF)->GetValue();
  }
  return 0;
}

uint32_t FrontEnd::GetGlobalThreshold(uint32_t type){
  switch (type){
  case Pixel::Lin: return m_config->GetField(Configuration::VTH_LIN)->GetValue();
  case Pixel::Diff: return m_config->GetField(Configuration::VTH1_DIFF)->GetValue();
  case Pixel::Sync: return m_config->GetField(Configuration::VTH_SYNC)->GetValue();
  }
  return 0;
}

void FrontEnd::SetGlobalThreshold(uint32_t type, uint32_t threshold){
  switch (type){
  case Pixel::Lin: return m_config->GetField(Configuration::VTH_LIN)->SetValue(threshold);
  case Pixel::Diff: return m_config->GetField(Configuration::VTH1_DIFF)->SetValue(threshold);
  case Pixel::Sync: return m_config->GetField(Configuration::VTH_SYNC)->SetValue(threshold);
  }
}

bool FrontEnd::GetPixelEnable(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetEnable();
}

void FrontEnd::SetPixelEnable(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetEnable(enable);
  
}

void FrontEnd::ApplyMasking(){
 for (uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for (uint32_t row=0;row<Matrix::NUM_ROWS;row++){
       if ( !m_matrix->GetPixel(col,row)->GetMask() ){
	 cout << "Masking: " << col << " , " << row << endl;
	 m_matrix->GetPixel(col, row)->SetEnable(false);
       }
    }
 } 
 return;  
}

bool FrontEnd::GetPixelInject(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetInject();
}

void FrontEnd::SetPixelInject(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetInject(enable);
}

bool FrontEnd::GetPixelHitbus(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetHitbus();
}

void FrontEnd::SetPixelHitbus(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetHitbus(enable);
}

int32_t FrontEnd::GetPixelThreshold(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetThreshold();
}

void FrontEnd::SetPixelThreshold(uint32_t col, uint32_t row, int32_t threshold){
  m_matrix->GetPixel(col,row)->SetThreshold(threshold);
}

bool FrontEnd::GetPixelGain(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetGain();
}

void FrontEnd::SetPixelGain(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetGain(enable);
}

void FrontEnd::SetPixelMask(uint32_t col, uint32_t row, uint32_t mask){
  return m_matrix->GetPixel(col,row)->SetMask(mask);
}

uint32_t FrontEnd::GetPixelMask(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetMask();
}

void FrontEnd::SetGlobalConfig(map<string,uint32_t> config){
  m_config->SetRegisters(config);
}

map<string,uint32_t> FrontEnd::GetGlobalConfig(){
  return m_config->GetRegisters();
}

TemperatureSensor * FrontEnd::GetTemperatureSensor(uint32_t index){
  return m_ntcs[index];
}

RadiationSensor * FrontEnd::GetRadiationSensor(uint32_t index){
  return m_bjts[index];
}

void FrontEnd::ProcessCommands(){
  for(uint32_t addr=Configuration::REGISTER0;addr<Configuration::NUM_REGISTERS;addr++){
    if(!m_config->GetRegister(addr)->IsUpdated()){continue;}
    WriteRegister(addr);
    m_config->GetRegister(addr)->Update(false);
  }
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      if(!m_matrix->IsPairUpdated(dcol,row)){continue;}
      WritePixelPair(dcol,row);
      m_matrix->UpdatePair(dcol,row,false);
    }
  }
}

void FrontEnd::WriteGlobal(){
  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new Noop());
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegisterValue(addr)));
    m_encoder->AddCommand(new Noop());
  }
}

void FrontEnd::WriteRegister(uint32_t addr){
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegisterValue(addr,true)));
  m_encoder->AddCommand(new Noop());
}

void FrontEnd::ReadRegister(uint32_t addr){
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new RdReg(m_chipid,addr));
  m_encoder->AddCommand(new Noop());
}


void FrontEnd::ReadGlobal(){
  for(uint32_t addr=Configuration::REGISTER0;addr<=Configuration::NUM_REGISTERS;addr++){
    m_encoder->AddCommand(new Noop());
    m_encoder->AddCommand(new RdReg(m_chipid,addr));
  }
}

void FrontEnd::WritePixels(){
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      WritePixelPair(dcol,row);
    }
  }
}

void FrontEnd::WritePixelPair(uint32_t double_col, uint32_t row){
  uint32_t value = m_matrix->GetPair(double_col,row);
  if(!m_matrix->IsPairUpdated(double_col,row)) return;
  m_config->SetRegisterValue(Configuration::ADDR_REGION_COL,double_col);
  m_config->SetRegisterValue(Configuration::ADDR_REGION_ROW,row);
  m_config->SetRegisterValue(Configuration::ADDR_PIX_PORTAL,value);
  WriteRegister(Configuration::ADDR_REGION_COL);
  WriteRegister(Configuration::ADDR_REGION_ROW);
  WriteRegister(Configuration::ADDR_PIX_PORTAL);
  m_matrix->UpdatePair(double_col,row,false);
}

void FrontEnd::ReadPixels(){
  for(uint32_t dcol=0;dcol<200;dcol++){
    for(uint32_t row=0;row<192;row++){
      ReadPixelPair(dcol,row);
    }
  }
}

void FrontEnd::ReadPixelPair(uint32_t double_col, uint32_t row){
  m_config->GetField(Configuration::REGION_COL)->SetValue(double_col);
  m_config->GetField(Configuration::REGION_ROW)->SetValue(row);
  m_encoder->AddCommand(new WrReg(m_chipid,1,m_config->GetRegisterValue(1,true)));
  m_encoder->AddCommand(new WrReg(m_chipid,2,m_config->GetRegisterValue(2,true)));
  m_encoder->AddCommand(new RdReg(m_chipid,0));
}

void FrontEnd::EnableCoreColumn(uint32_t ccol, bool enable){
  uint32_t reg=0;
  uint32_t off=0;
  if     (ccol>= 0 and ccol<16){ reg=Configuration::EN_CORE_COL_SYNC;   off= 0; }
  else if(ccol>=16 and ccol<32){ reg=Configuration::EN_CORE_COL_LIN_1;  off=16; }
  else if(ccol==32)            { reg=Configuration::EN_CORE_COL_LIN_2;  off=32; }
  else if(ccol>=33 and ccol<49){ reg=Configuration::EN_CORE_COL_DIFF_1; off=33; }
  else if(ccol==49)            { reg=Configuration::EN_CORE_COL_DIFF_2; off=49; }
  uint32_t value = m_config->GetField(reg)->GetValue();
  if(enable) value |= (1<<(ccol-off));
  else       value &= ~(1<<(ccol-off));
  m_config->GetField(reg)->SetValue(value);
}

void FrontEnd::SetCoreColumn(uint32_t mode, uint32_t index, string frontend){
  uint32_t start=0;
  uint32_t stop=0;
  if(index>mode) return;
  //  uint32_t stp=50/mode; // EJS 2020-11-08: not convinced this is correct
  uint32_t stp=mode; // EJS 2020-11-08: this might be the correct way
  if(frontend.find("syn")!=string::npos){start=0; stop=16;}
  if(frontend.find("lin")!=string::npos){start=16; stop=33;}
  if(frontend.find("diff")!=string::npos){start=33; stop=50;}
  if(frontend.find("all")!=string::npos){start=0; stop=50;}
  for(uint32_t cc=0;cc<50;cc++){
    bool ccenable = ((index+cc)%stp==0?true:false); // EJS 2020-11-08
    if(cc>=start and cc<stop) EnableCoreColumn(cc,ccenable);
    else EnableCoreColumn(cc,false);
  }
}

void FrontEnd::EnableCalDoubleColumn(uint32_t double_col, bool enable){
  uint32_t reg=0;
  uint32_t off=0;
  if     (double_col>=  0 and double_col< 16){ reg=Configuration::CAL_COLPR_SYNC_1;   off=  0; }
  else if(double_col>= 16 and double_col< 32){ reg=Configuration::CAL_COLPR_SYNC_2;   off= 16; }
  else if(double_col>= 32 and double_col< 48){ reg=Configuration::CAL_COLPR_SYNC_3;   off= 32; }
  else if(double_col>= 48 and double_col< 64){ reg=Configuration::CAL_COLPR_SYNC_4;   off= 48; }
  else if(double_col>= 64 and double_col< 80){ reg=Configuration::CAL_COLPR_LIN_1;    off= 64; }
  else if(double_col>= 80 and double_col< 96){ reg=Configuration::CAL_COLPR_LIN_2;    off= 80; }
  else if(double_col>= 96 and double_col<112){ reg=Configuration::CAL_COLPR_LIN_3;    off= 96; }
  else if(double_col>=112 and double_col<128){ reg=Configuration::CAL_COLPR_LIN_4;    off=112; }
  else if(double_col>=128 and double_col<132){ reg=Configuration::CAL_COLPR_LIN_5;    off=128; }
  else if(double_col>=132 and double_col<148){ reg=Configuration::CAL_COLPR_DIFF_1;   off=132; }
  else if(double_col>=148 and double_col<164){ reg=Configuration::CAL_COLPR_DIFF_2;   off=148; }
  else if(double_col>=164 and double_col<180){ reg=Configuration::CAL_COLPR_DIFF_3;   off=164; }
  else if(double_col>=180 and double_col<196){ reg=Configuration::CAL_COLPR_DIFF_4;   off=180; }
  else if(double_col>=196 and double_col<200){ reg=Configuration::CAL_COLPR_DIFF_5;   off=196; }
  uint32_t value = m_config->GetField(reg)->GetValue();
  if(enable) value |= (1<<(double_col-off));
  else       value &= ~(1<<(double_col-off));
  m_config->GetField(reg)->SetValue(value);
}

void FrontEnd::EnableCalCoreColumn(uint32_t core_col, bool enable){
  for(uint32_t dc=core_col*4; dc<(core_col+1)*4; dc++){
    EnableCalDoubleColumn(dc,enable);
  }
}

void FrontEnd::InitAdc(){
  m_config->GetField(Configuration::ADC_TRIM)->SetValue(0);
  m_config->GetField(Configuration::ADC_REF_TRIM)->SetValue(0);
  m_config->GetField(Configuration::GLOBAL_PULSE_RT)->SetValue(8);
  WriteGlobal();
  m_encoder->AddCommand(new Pulse(m_chipid));
}

void FrontEnd::ReadSensor(uint32_t pos, bool read_radiation_sensor){
  if(pos==0){
    m_config->GetField(Configuration::SENSOR_BIAS_0)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_CURRENT_0)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_ENABLE_0)->SetValue(1);
    if(read_radiation_sensor == true)
       m_bjts[pos]->SetPower(true);
    else
      m_ntcs[pos]->SetPower(true);
  }else if(pos==1){
    m_config->GetField(Configuration::SENSOR_BIAS_1)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_CURRENT_1)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_ENABLE_1)->SetValue(1);
    if(read_radiation_sensor == true)
       m_bjts[pos]->SetPower(true);
    else
      m_ntcs[pos]->SetPower(true);
  }else if(pos==2){
    m_config->GetField(Configuration::SENSOR_BIAS_2)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_CURRENT_2)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_ENABLE_2)->SetValue(1);
    if(read_radiation_sensor == true)
       m_bjts[pos]->SetPower(true);
    else
      m_ntcs[pos]->SetPower(true);
  }else if(pos==3){
    m_config->GetField(Configuration::SENSOR_BIAS_3)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_CURRENT_3)->SetValue(1);
    m_config->GetField(Configuration::SENSOR_ENABLE_3)->SetValue(1);
    if(read_radiation_sensor == true)
       m_bjts[pos]->SetPower(true);
    else
      m_ntcs[pos]->SetPower(true);
  }
 
  if(pos==0){
    if(read_radiation_sensor == true)
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(4);
    else
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(3);
  }else if(pos==1){
    if(read_radiation_sensor == true)
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(6);
    else
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(5);
  }else if(pos==2){
    if(read_radiation_sensor == true)
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(14);
    else
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(15);
  }else if(pos==3){
    if(read_radiation_sensor == true)
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(8);
    else
      m_config->GetField(Configuration::MONITOR_VMON_MUX)->SetValue(7);
  }
  m_config->GetField(Configuration::MONITOR_EN)->SetValue(1); //2^12 // Start ADC Conversion
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  m_config->GetField(Configuration::GLOBAL_PULSE_RT)->SetValue(64); //2^4 Reset ADC
  WriteGlobal();
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
 
  m_encoder->AddCommand(new Pulse(m_chipid));
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
 
  m_config->GetField(Configuration::GLOBAL_PULSE_RT)->SetValue(8); //2^3 Clear Monitoring data
  WriteGlobal();
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Pulse(m_chipid));
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());

  m_config->GetField(Configuration::GLOBAL_PULSE_RT)->SetValue(4096); //2^12 // Start ADC Conversion
  WriteGlobal();
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Pulse(m_chipid));
  for (int ind=0; ind<20;ind++)
    m_encoder->AddCommand(new Noop());

  // m_encoder->AddCommand(new RdReg(m_chipid,136));
  // m_encoder->AddCommand(new Noop());
}

void FrontEnd::Trigger(uint32_t delay, uint32_t trigMult, bool bc1, bool bc2, bool bc3, bool bc4){

  /*

  //This is to try to make the AZ work in the sync FE
  //We leave it alone by the time being
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());

  for(uint32_t i=0;i<12;i++){
    m_encoder->AddCommand(new Noop());
  }

  m_encoder->AddCommand(new Pulse());
  
  m_encoder->AddCommand(new Noop());
  m_encoder->AddCommand(new Noop());
  */

  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());

  for(uint32_t i=0;i<32;i++){
    m_encoder->AddCommand(new Noop());
  }

  //The first varaible is the chip id, it's set to 15 to mark each chip but can be modified to target individual chips in the future. 
  m_encoder->AddCommand(new Cal(15,0,0,1,0,0)); //2022-02-10 was  m_encoder->AddCommand(new Cal(15,1,0,4 ,0,0,0));
  //m_encoder->AddCommand(new Cal(8,0,0,1,0,0));
  //We space the Cal from the Trigger with Noops
  //Since each Noop spans 4 BCs we add a whole number of Noops
  for(uint32_t i=0;i<(delay/4-1);i++){
    m_encoder->AddCommand(new Noop());
  }
  for(uint32_t iTrig = 0; iTrig < trigMult; iTrig++){
    m_encoder->AddCommand(new RD53A::Trigger(bc1, bc2, bc3, bc4, 0));
  }
  for(uint32_t i=0;i<8;i++){
    m_encoder->AddCommand(new Noop());
  }
  m_encoder->AddCommand(new Cal(15,1,0,0,0,0));
  //m_encoder->AddCommand(new Pulse());
}

Hit * FrontEnd::GetHit(){
  Hit * tmp = NULL;
  m_mutex.lock();
  tmp = m_hits.front();
  m_mutex.unlock();
  return tmp;
}

bool FrontEnd::NextHit(){
  if (m_hits.empty()) return false;
  m_mutex.lock();
  Hit * hit = m_hits.front();
  m_hits.pop_front();
  delete hit;
  m_mutex.unlock();
  return (!m_hits.empty());
}

bool FrontEnd::HasHits(){
  return (not m_hits.empty());
}

void FrontEnd::ClearHits(){
  while(NextHit());
}

void FrontEnd::SetNumberOfEvents(uint32_t totevnum){ //AAA
  m_totalHits = totevnum;
}

uint32_t FrontEnd::GetTotalEventsNumber(){ //AAA
  return m_totalHits;
}

void FrontEnd::HandleData(uint8_t *recv_data, uint32_t recv_size, bool register_data){

  if(m_verbose) cout << "FrontEnd::HandleData" <<endl;
  if(m_verbose)  cout<< "FrontEnd::HandleData is being set to handle Normal(0) or RegisterData(1): "<<register_data<<endl;

  //Register data required 64 bit decoding, so the decoder is changed into 64bit mode. 
  if (register_data){
    //std::cout<<"Decoding Register data"<<std::endl;
    m_decoder->SetMode(m_decoder->MODE_64);
  }else
    m_decoder->SetMode(m_decoder->MODE_32);

  m_decoder->SetBytes(recv_data,recv_size,true);
  if(m_verbose){cout << "FrontEnd::HandleData Decode bytestream: " << m_decoder->GetByteString() << endl;}
  m_decoder->Decode(m_verbose);

  for(auto frame: m_decoder->GetFrames()){

    if(m_verbose) cout << "FrontEnd::HandleData: " << frame->ToString() << endl;

    if(frame->GetType()==Frame::HEADER){
      HeaderFrame * hdr=dynamic_cast<HeaderFrame*>(frame);
      int tmpBCID=hdr->GetBCID();
      if ( abs( tmpBCID-m_refBCID )>30 ) {
    	m_refBCID=tmpBCID;
    	//m_hit->SetEvNum(m_hit->GetEvNum()+1); //before ISMET
      }
      m_hit->SetBCID( tmpBCID>=m_refBCID ? tmpBCID-m_refBCID : tmpBCID-m_refBCID+pow(2,12) ); //before ISMET
      
      if ( hdr->GetBCID()==5 ) m_hit->SetEvNum(m_hit->GetEvNum()+1);
      m_hit->SetBCID( hdr->GetBCID() );
      m_hit->SetTTag( hdr->GetTTag() );
    }else if(frame->GetType()==Frame::HIT){
      HitFrame * hif=dynamic_cast<HitFrame*>(frame);
      for(uint32_t idx=0;idx<4;idx++){
  	if(hif->GetTOT(idx)==0x0)continue;
  	Hit * nhit = m_hit->Clone();
  	nhit->Set(hif->GetCol()+idx,hif->GetRow(),hif->GetTOT(idx));
  	m_mutex.lock();
  	m_hits.push_back(nhit);
  	m_mutex.unlock();
      }
    }else if(frame->GetType()==Frame::REGISTER){
      RegisterFrame * reg=dynamic_cast<RegisterFrame*>(frame);
      std::string RegFrameString = reg->ToString();
      if(reg->GetAuroraCode()!=0xB4) cout << "FrontEnd::HandleData: " << reg->ToString() << endl;
      if(reg->GetAuroraCode()!=0xCC){
        for(uint32_t i=0;i<2;i++){
          if(reg->GetAddress(i)==Configuration::PIX_PORTAL){
            uint32_t reg_col=m_config->GetField(Configuration::REGION_COL)->GetValue();
            uint32_t reg_row=m_config->GetField(Configuration::REGION_ROW)->GetValue();
            m_matrix->SetPair(reg_col,reg_row,reg->GetValue(i));
          }
          else if(reg->GetAddress(i)>Configuration::PIX_PORTAL and reg->GetAddress(i)<=0x1FF){
            m_config->SetRegisterValue(reg->GetAddress(i),reg->GetValue(i));
          }
          if(reg->GetAddress(i) == 136){
            for(int j=0; j < 4; j++){
              if(m_ntcs[j]->GetPower() == true && m_ntcs[j]->isUpdated() == false && reg->GetAuto(i) == 0){
                m_ntcs[j]->SetADC(reg->GetValue(i));
                m_ntcs[j]->Update(true);
              }
              else if(m_bjts[j]->GetPower() == true && m_bjts[j]->isUpdated() == false && reg->GetAuto(i) == 0){
                m_bjts[j]->SetADC(reg->GetValue(i));
                m_bjts[j]->Update(true);
  	      }
  	    }
          }
        }
      }
    }else if(frame->GetType()==Frame::DATA){
      DataFrame * dat=dynamic_cast<DataFrame*>(frame);
      if(dat->GetType()==DataFrame::SYN_HDR){
        m_hit->Update(dat->GetTID(1),dat->GetTTag(1),dat->GetBCID(1));
      }else if(dat->GetType()==DataFrame::HDR_HDR){
        m_hit->Update(dat->GetTID(0),dat->GetTTag(0),dat->GetBCID(0));
        m_hit->Update(dat->GetTID(1),dat->GetTTag(1),dat->GetBCID(1));
      }else if(dat->GetType()==DataFrame::HDR_HIT){
        m_hit->Update(dat->GetTID(0),dat->GetTTag(0),dat->GetBCID(0));
        for(uint32_t idx=0;idx<4;idx++){
          if(dat->GetTOT(1,idx)>0){
            Hit * nhit = m_hit->Clone();
            nhit->Set(dat->GetCol(1)+idx,dat->GetRow(1),dat->GetTOT(1,idx));
            m_mutex.lock();
            m_hits.push_back(nhit);
            m_mutex.unlock();
          }
        }
      }else if(dat->GetType()==DataFrame::SYN_HIT){
        for(uint32_t idx=0;idx<4;idx++){
          if(dat->GetTOT(1,idx)>0){
            Hit * nhit = m_hit->Clone();
            nhit->Set(dat->GetCol(1)+idx,dat->GetRow(1),dat->GetTOT(1,idx));
            m_mutex.lock();
            m_hits.push_back(nhit);
            m_mutex.unlock();
          }
        }
      }else if(dat->GetType()==DataFrame::HIT_HDR){
        for(uint32_t idx=0;idx<4;idx++){
          if(dat->GetTOT(0,idx)>0){
            Hit * nhit = m_hit->Clone();
            nhit->Set(dat->GetCol(0)+idx,dat->GetRow(0),dat->GetTOT(0,idx));
            m_mutex.lock();
            m_hits.push_back(nhit);
            m_mutex.unlock();
          }
        }
        m_hit->Update(dat->GetTID(1),dat->GetTTag(1),dat->GetBCID(1));
      }else if(dat->GetType()==DataFrame::HIT_HIT){
        for(uint32_t i=0;i<2;i++){
          for(uint32_t idx=0;idx<4;idx++){
            if(dat->GetTOT(i,idx)>0){
              Hit * nhit = m_hit->Clone();
              nhit->Set(dat->GetCol(i)+idx,dat->GetRow(i),dat->GetTOT(i,idx));
              m_mutex.lock();
              m_hits.push_back(nhit);
              m_mutex.unlock();
            }
          }
        }
      }
    }
  }
}

void FrontEnd::Encode(){
  if(m_verbose){
    for(auto cmd: m_encoder->GetCommands()){
      cout << "FronEnd::Encode: Command: " << cmd->ToString() << endl;
    }
  }
  m_encoder->Encode();
  if(m_verbose){
    cout << "FrontEnd::Encode: Byte Stream: " << m_encoder->GetByteString() << endl;
  }
}

uint32_t FrontEnd::GetLength(){
  return m_encoder->GetLength();
}

uint8_t * FrontEnd::GetBytes(){
  return m_encoder->GetBytes();
}




