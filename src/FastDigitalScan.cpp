#include "RD53Emulator/FastDigitalScan.h"

#include <iostream>
#include <chrono>
#include <unistd.h>

using namespace std;
using namespace RD53A;

FastDigitalScan::FastDigitalScan(){
  m_ntrigs = 100;
  m_TriggerLatency = 58;
}

FastDigitalScan::~FastDigitalScan(){}

void FastDigitalScan::PreRun(){
  //First Backup all FE's as we don't want Digital Scan to change the final config file
  for(auto fe : GetFEs()){ 
    fe->BackupConfig();
  }


  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_ntrigs << endl;
  
  if(m_verbose){
    cout << "FastDigitalScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "FastDigitalScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }
  
  for(auto fe : GetFEs()){ 
 
    cout << "FastDigitalScan: Configure digital injection" << endl;
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,1);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0x4000); 
    fe->WriteGlobal();
    Send(fe);

    cout << "FastDigitalScan: Store the previous thresholds in memory" << endl;
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Sync));
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Lin));
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Diff));

    cout << "FastDigitalScan: Force the threshold to 500" << endl;
    fe->SetGlobalThreshold(Pixel::Sync,500);
    fe->SetGlobalThreshold(Pixel::Lin, 500);
    fe->SetGlobalThreshold(Pixel::Diff,500);
    fe->WriteGlobal();
    Send(fe);

    m_occ[fe->GetName()]   =new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);
    m_enable[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tid[fe->GetName()]   =new TH1I(("tid_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger ID").c_str(), 16, 0, 16);
    m_ttag[fe->GetName()]  =new TH1I(("ttag_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger Tag").c_str(), 16, 0, 16);
    m_bcid[fe->GetName()]  =new TH1I(("bcid_"+fe->GetName()).c_str(),(fe->GetName()+";BCID").c_str(),0xFFF+1,-0.5,0xFFF+0.5);

  }

}

void FastDigitalScan::Run(){

  //Prepare the trigger
  cout << "FastDigitalScan: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency,2,true,true,true,true);

 
  //Create the mask
  cout << "FastDigitalScan: Create the mask" << endl;
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(0,0,400,192);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency(100);
  GetMasker()->Build();

  //loop over mask steps
  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    cout << "FastDigitalScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
    GetMasker()->GetStep(st);

    //Do the mask step
    ConfigMask();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //accumulate hits

    
    //Trigger and read-out

    for(uint32_t trig=0;trig<m_ntrigs; trig++){
      Trigger();
    }
    auto start = chrono::steady_clock::now();

    //histogram hit
    bool LastLoop=false;
    uint32_t nhits=0;
    while(true){
      for(auto fe : GetFEs()){
	if(!fe->HasHits()) continue;
	LastLoop=false;
	start = chrono::steady_clock::now();
	Hit* hit = fe->GetHit();
	if(hit!=0){
	  if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
	    cout << "Hit is not expected in the current mask step: " << hit->ToString() << endl;
	  } 
	  m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	  m_tid[fe->GetName()]->Fill(hit->GetTID());
	  m_ttag[fe->GetName()]->Fill(hit->GetTTag());
	  m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	  nhits++;
	}
	fe->NextHit();
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>200){LastLoop=true;}
    }
    cout << "FastDigitalScan: NHits in step: " << nhits <<" / "<<m_ntrigs*GetMasker()->GetPixels().size()*GetFEs().size()<< endl;
    
    //Undo the mask step
    UnconfigMask();
  
  }
}

void FastDigitalScan::Analysis(){

  for(auto fe : GetFEs()){
    //First Backup all FE's as we don't want Digital Scan to change the final config file
    
  
    cout << "FastDigitalScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();
    // fe->SetGlobalThreshold(Pixel::Sync,m_mem_thres[fe->GetName()][0]);
    // fe->SetGlobalThreshold(Pixel::Lin,m_mem_thres[fe->GetName()][1]);
    // fe->SetGlobalThreshold(Pixel::Diff,m_mem_thres[fe->GetName()][2]);
    // fe->WriteGlobal();
    // Send(fe);
  
    m_occ[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    delete m_occ[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
  }
}
