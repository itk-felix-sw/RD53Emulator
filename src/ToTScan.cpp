#include "RD53Emulator/ToTScan.h" 
#include "RD53Emulator/Tools.h"

#include <iostream>
#include <chrono>

#include "TCanvas.h"

using namespace std;
using namespace RD53A;

ToTScan::ToTScan(){
  m_mask_step = 4;
  m_cc_max = 4;
  m_ntrigs = 100;
  m_TriggerLatency = 48;
  m_vcalMed = 500;
  m_vcalDiff =400;
  m_vcalHigh = m_vcalMed + m_vcalDiff;

  if(m_verbose){
    std::cout << __PRETTY_FUNCTION__ << ": m_mask_step = " << m_mask_step << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_cc_max = " << m_cc_max << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_ntrigs = " << m_ntrigs << std::endl;
  }
} 

ToTScan::~ToTScan(){}

void ToTScan::PreRun(){
  m_vcalDiff = Tools::injToVcal(m_charge);
  std::cout<<"Injecting Charge of "<<m_charge<<" with Med: "<<m_vcalMed<<" and Diff: "<< m_vcalDiff  <<std::endl;

  for(auto fe : GetFEs()){

    fe->BackupConfig();
    
    //Configure the options for an threshold scan
    cout << "TotScan: Configure analog injection for " << fe->GetName() << endl;
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalMed+m_vcalDiff);

    if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){
      cout << "TotScan: Configure Sync+Lin+Diff for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      GetMasker()->SetRange(Matrix::SYN_COL0,0,Matrix::DIF_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::SYN_COL0)/2);
    }
    else if((m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){    //syn
      cout << "TotScan: Configure Sync for " << fe->GetName() << endl;
      GetMasker()->SetRange(Matrix::SYN_COL0,0,Matrix::SYN_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::SYN_COLN+1-Matrix::SYN_COL0)/8);
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);   
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1 ,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2 ,0);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
    }
    else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){    //lin
      cout << "TotScan: Configure Lin for " << fe->GetName() << endl;
      GetMasker()->SetRange(Matrix::LIN_COL0,0,Matrix::LIN_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::LIN_COLN+1-Matrix::LIN_COL0)/4);
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);//Tools::chargeToThr(500,Pixel::Sync));
      fe->SetGlobalThreshold(Pixel::Diff,500);//Tools::chargeToThr(500,Pixel::Diff));
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
    }
    else if(!(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){    //diff
      cout << "TotScan: Configure Diff for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      GetMasker()->SetRange(Matrix::DIF_COL0,0,Matrix::DIF_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::DIF_COL0)/4);
      fe->SetGlobalThreshold(Pixel::Sync,500); //Tools::chargeToThr(500,Pixel::Sync));
      fe->SetGlobalThreshold(Pixel::Lin, 500); //Tools::chargeToThr(500,Pixel::Lin));
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC ,0);
    }
    else{
      cout << "TotScan: Configure Lin+Diff for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,Tools::chargeToThr(500,Pixel::Sync));
      GetMasker()->SetRange(Matrix::LIN_COL0,0,Matrix::DIF_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::LIN_COL0)/2);
    }
       
    fe->WriteGlobal();
    Send(fe);

    cout << "ToTScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
    
    m_totT[fe->GetName()]       = new TH1I(("totT_"+fe->GetName()).c_str(),(fe->GetName()+";ToT [bc]").c_str(), 16, -0.5, 15.5);
    m_occ[fe->GetName()]        = new TH2I(("occ_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()]        = new TH2I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row;ToT [bc]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot2[fe->GetName()]       = new TH2I(("tot2_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row;ToT^2 [bc^2]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    
    m_meantot[fe->GetName()]    = new TH2F(("meantot_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row;Mean ToT [bc]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_sigmatot[fe->GetName()]   = new TH2F(("sigmatot_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row; #sigma(ToT) [bc]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_meantot_1d[fe->GetName()] = new TH1F(("meantot_1d_" +fe->GetName()).c_str(),(fe->GetName()+";Mean ToT [bc]").c_str()    , 16, -0.5, 15.5);
    m_sigmatot_1d[fe->GetName()]= new TH1F(("sigmatot_1d_"+fe->GetName()).c_str(),(fe->GetName()+"; #sigma(ToT) [bc]").c_str(), 40, 0, 1.1);

    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": added front-end with chip ID " << fe->GetChipID() << std::endl;
  }
}

void ToTScan::Run(){

  cout << "ToTScan: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // The Analog scans requirs many more triggers due to timming issues
  
  cout << "TotScan: Prepare the mask" << endl;
  GetMasker()->SetShape(8,2);
  GetMasker()->Build();
  bool cutBCID=( m_scan_fe & SyncFE );
  
  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    cout << "TotScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
    GetMasker()->GetStep(st);
    ConfigMask();

    //uint32_t expected=GetMasker()->GetPixels().size();
    uint32_t nhits2=0;

    for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();
   
    auto start = chrono::steady_clock::now();
    
    //histogram hit
    uint32_t tmpTOT=0;
    bool LastLoop=false;

    while(true){
      
      for(auto fe : GetFEs()){
	if(!fe->HasHits()) continue;
	LastLoop=false;
	start = chrono::steady_clock::now();

	Hit* hit = fe->GetHit();
	if(hit!=0){
	  tmpTOT=hit->GetTOT();
	  
	  if( tmpTOT == 0xF or !(cutBCID? hit->GetBCID()==0:1)) {
	    fe->NextHit();
	    continue;
	  }
 
	  if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
	    cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	    //fe->NextHit();
	    //continue;
	  }

	  m_totT[fe->GetName()]->Fill(tmpTOT);
	  m_occ [fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	  m_tot [fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),tmpTOT);
	  m_tot2[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),pow(tmpTOT,2));
	  nhits2++;
	}
	//nhits2+=nhits;
	fe->NextHit();
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>300){LastLoop=true;}
    }

    cout << "AnalogScan: NRealHits in step: " << nhits2 <<" / "<< m_ntrigs*GetMasker()->GetPixels().size()*GetFEs().size()   <<std::endl;
    UnconfigMask();
  }
}

void ToTScan::Analysis(){
  TCanvas* can=new TCanvas("plot","plot",800,600);
  double sumtot = 0.;
  uint32_t totalhit = 0;
  double sumtot2 = 0.;
  double meantot2 = 0.;
  
  for(auto fe : GetFEs()){
    cout << "AnalogScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();

    m_occ[fe->GetName()]->Write();
    for(uint32_t row=0; row<Matrix::NUM_ROWS; row++){
      for(uint32_t col=0; col<Matrix::NUM_COLS; col++){
	sumtot = m_tot[fe->GetName()]->GetBinContent(col+1,row+1);
	totalhit = m_occ[fe->GetName()]->GetBinContent(col+1,row+1);
	if (totalhit == 0) m_meantot[fe->GetName()]->Fill(col,row,0.);
	else {
	  m_meantot[fe->GetName()]->Fill(col,row,sumtot/totalhit);
	  m_meantot_1d[fe->GetName()]->Fill(sumtot/totalhit);
	  meantot2 = pow(m_meantot[fe->GetName()]->GetBinContent(col+1,row+1),2); 
	  sumtot2 = m_tot2[fe->GetName()]->GetBinContent(col+1,row+1);
	  m_sigmatot[fe->GetName()]->Fill(col,row,pow((sumtot2/totalhit - meantot2),0.5));
	  m_sigmatot_1d[fe->GetName()]->Fill(pow((sumtot2/totalhit - meantot2),0.5));
	}
      }
    }
    m_meantot[fe->GetName()]->Write();
    m_sigmatot[fe->GetName()]->Write();
    m_meantot_1d[fe->GetName()]->Write();
    m_sigmatot_1d[fe->GetName()]->Write();

    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC_2D.pdf").c_str() );

    m_totT[fe->GetName()]->Draw("HIST");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOT_1D.pdf").c_str() );

    m_meantot_1d[fe->GetName()]->Draw("HIST");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOTmean_1D.pdf").c_str() );
    
    m_sigmatot_1d[fe->GetName()]->Draw("HIST");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOTsigma_1D.pdf").c_str() );

    delete m_occ[fe->GetName()];
    delete m_meantot[fe->GetName()];
    delete m_sigmatot[fe->GetName()];
  }
  delete can;
}
