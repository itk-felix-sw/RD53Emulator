#include "RD53Emulator/InvalidFrame.h"
#include <sstream>
#include <iomanip>
#include <bitset>
#include <iostream>

using namespace std;
using namespace RD53A;

InvalidFrame::InvalidFrame(){
  m_bytes[0] = 0;
  m_bytes[1] = 0;
  m_bytes[2] = 0;
  m_bytes[3] = 0;
}

InvalidFrame::~InvalidFrame(){}

uint32_t InvalidFrame::UnPack(uint8_t * bytes, uint32_t maxlen){

  for(uint32_t i=0;i<4;i++){
    m_bytes[i] = bytes[i];
  }

  return 4;
}

uint32_t InvalidFrame::Pack(uint8_t * bytes){

  for(uint32_t i=0;i<4;i++){
    bytes[i]=m_bytes[i];
  }

  return 4;
}

string InvalidFrame::ToString(){
  ostringstream os;
  os << "InvalidFrame "
     << " 0x" << hex
     << setw(2) << setfill('0') << m_bytes[0]
     << setw(2) << setfill('0') << m_bytes[1]
     << setw(2) << setfill('0') << m_bytes[2]
     << setw(2) << setfill('0') << m_bytes[3]
     << dec;
  return os.str();
}

uint32_t InvalidFrame::GetType(){
  return Frame::INVALID;
}
