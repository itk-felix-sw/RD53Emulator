#include "RD53Emulator/AnalogScan.h"
#include "RD53Emulator/Masker.h"
#include "RD53Emulator/Tools.h"
#include "TCanvas.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53A;

AnalogScan::AnalogScan(){
  m_ntrigs = 100;
  m_TriggerLatency = 48;
  m_colMin = 0; 
  m_colMax = 400;
}

AnalogScan::~AnalogScan(){}

void AnalogScan::PreRun(){

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_ntrigs << endl;
  
  if(m_verbose){
    cout << "AnalogScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "AnalogScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }

  for(auto fe : GetFEs()){

    fe->BackupConfig();

    cout << "AnalogScan: Configure analog injection" << endl;
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,3000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,500);    
    //////////fe->GetConfig()->SetField(Configuration::VCAL_HIGH,4000);//for disc bump
    //////////fe->GetConfig()->SetField(Configuration::VCAL_MED,0);//for disc bump


    if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){
      m_colMin = Matrix::SYN_COL0; m_colMax = Matrix::NUM_COLS; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->GetConfig()->SetField(Configuration::FAST_TOT,1);
      //fe->GetConfig()->SetField(Configuration::AUTO_ZERO,1);
      fe->GetConfig()->SetField(Configuration::INJ_FINE_DELAY,10);
      fe->GetConfig()->SetField(Configuration::IBIAS_KRUM_SYNC,40);
    }
    else if((m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){   //SynONLY
      m_colMin = Matrix::SYN_COL0; m_colMax = Matrix::SYN_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1 ,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2 ,0);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
    }
    else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){   //LinDiff
      m_colMin = Matrix::LIN_COL0; m_colMax = Matrix::NUM_COLS; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
    }
    else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){   //LinONLY
      m_colMin = Matrix::LIN_COL0; m_colMax = Matrix::LIN_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
    }
    else if(!(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){   //DiffONLY
      m_colMin = Matrix::DIF_COL0; m_colMax = Matrix::DIF_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500); 
      fe->SetGlobalThreshold(Pixel::Lin, 500); 
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC ,0);
    }
    else{
      cout << " NO VALID FE SELECTED ... ABORTING " << endl;
      exit(-1);
    }
    
    fe->WriteGlobal();
    Send(fe);
       
    cout << "AnalogScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
    Send(fe);

    m_occ[fe->GetName()]    =new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()]    =new TH1I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";TOT").c_str(), 16, 0, 16);
    m_enable[fe->GetName()] =new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tid[fe->GetName()]    =new TH1I(("tid_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger ID").c_str(), 16, 0, 16);
    m_ttag[fe->GetName()]   =new TH1I(("ttag_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger Tag").c_str(), 16, 0, 16);
    m_bcid[fe->GetName()]   =new TH1I(("bcid_"+fe->GetName()).c_str(),(fe->GetName()+";BCID").c_str(),0xFFFF+1,-0.5,0xFFFF+0.5);
    m_ttagmap[fe->GetName()]=new TH2I(("ttagmap_"+fe->GetName()).c_str(),"TTag map;Column;Row;TTag accumulated", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_bcidmap[fe->GetName()]=new TH2I(("bcidmap_"+fe->GetName()).c_str(),"BCID map;Column;Row;BCID accumulated", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_hits[fe->GetName()]   =new HitTree(("hits_"+fe->GetName()).c_str(),fe->GetName());

  }

  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax,192);
  GetMasker()->SetShape(8,2);
  /////////////////////GetMasker()->SetShape(1,1);
  if ( m_scan_fe & SyncFE ) GetMasker()->SetFrequency((m_colMax-m_colMin)/8);
  else                      GetMasker()->SetFrequency((m_colMax-m_colMin)/4);
  GetMasker()->Build();

}

void AnalogScan::Run(){

  //Prepare the trigger
  cout << "AnalogScan: Prepare the TRIGGER" << endl;
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // This is needed for diff in the disconnected bump config.
  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // This is ok for Analog synlindiff
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1}); 
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,0,0,0,0,0,0}); // This is the minimum needed for lin
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1}); // does not work with lin
  //cout << "AnalogScan: Create the mask" << endl;
  
  bool cutBCID=( m_scan_fe & SyncFE );

  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    /////cout << "AnalogScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
    GetMasker()->GetStep(st);
    
    //Do the mask step
    ConfigMask();
    //std::this_thread::sleep_for(std::chrono::milliseconds(50)); //100
    
    //accumulate hits
    uint32_t expected=GetMasker()->GetPixels().size();
    
    //histogram hit
    bool LastLoop=false;
    uint32_t nhits=0;
    uint32_t nRhits=0;
    uint32_t nGoodHists=0;

    //Trigger and read-out
    //SendPulse();
    //std::this_thread::sleep_for(std::chrono::microseconds(200));
    for(uint32_t trig=0;trig<m_ntrigs; trig++){
      //std::this_thread::sleep_for(std::chrono::microseconds(100));
      Trigger();
    }

    auto start = chrono::steady_clock::now();
    
    while(true){
	
      for(auto fe : GetFEs()){
	if(!fe->HasHits()) continue;
	LastLoop=false;
	start = chrono::steady_clock::now();
	Hit* hit = fe->GetHit();
	if(hit!=0){
	  hit->SetEvNum(st);
	  
	  if(hit->GetTOT()>0 and (cutBCID? hit->GetBCID()==0:1) ) { //added by VD to control the noise
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    nRhits++;
	    
	    //cout << " ::: " << hit->ToString() << endl;
	    if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
	      cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	      // fe->NextHit();
	      // continue;
	    } else {
	      nGoodHists++;
	    }
	    m_tot[fe->GetName()] ->Fill(hit->GetTOT());
	    m_tid[fe->GetName()] ->Fill(hit->GetTID());
	    m_ttag[fe->GetName()]->Fill(hit->GetTTag());
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	    m_bcidmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetBCID());
	    m_ttagmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetTTag());
	  } else {
	  }
	  nhits++;
	  //tree->Set(hit);
	  //tree->Fill();
	}
	fe->NextHit();
	//
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>100 or nGoodHists>=m_ntrigs*expected*GetFEs().size() ){LastLoop=true;}
      //if(ms>300) {LastLoop=true;}
    }
    cout << "AnalogScan:: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() 
	 << " ==== NRealHits in step: " << nRhits << " / " << nGoodHists << " / " << m_ntrigs*GetMasker()->GetPixels().size()*GetFEs().size()   
	 <<" :: ALL Hits: " << nhits << endl;
    
    //Undo the mask step
    UnconfigMask();
  }
  
}
 
void AnalogScan::Analysis(){
  TCanvas* can=new TCanvas("plot","plot",800,600);
  for(auto fe : GetFEs()){
    cout << "AnalogScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();

    
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
      for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	//	m_tot[fe->GetName()]->SetBinContent(col+1,row+1,m_tot[fe->GetName()]->GetBinContent(col+1,row+1)/m_ntrigs);
        if (m_occ[fe->GetName()]->GetBinContent(col+1,row+1) == m_ntrigs){
          m_enable[fe->GetName()]->SetBinContent(col+1,row+1,1);
	  fe->SetPixelEnable(col,row,true);
	}
        else fe->SetPixelEnable(col,row,false);
      }
    }

    m_occ[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    m_ttagmap[fe->GetName()]->Write();
    m_bcidmap[fe->GetName()]->Write();
    if(m_storehits){m_hits[fe->GetName()]->Write();}

    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );

    delete m_occ[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
    delete m_ttagmap[fe->GetName()];
    delete m_bcidmap[fe->GetName()];
    delete m_hits[fe->GetName()];
    
  }
  delete can;
}
