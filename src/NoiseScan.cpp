#include "RD53Emulator/NoiseScan.h"

#include <iostream>
#include <chrono>
#include <signal.h>

using namespace std;
using namespace RD53A;

bool stopScan = false;

void Stop(int){
  std::cout << std::endl << "You pressed ctrl+c -> quitting" << std::endl;
  stopScan = true;
}

NoiseScan::NoiseScan(){
  m_TriggerLatency = 80;
  m_duration = 30; //s
  m_frequency = 0.2; //Hz
  m_maxRate = 1.0e-6; //0.000001;
  m_ntrigs = 0;
  m_TriggerMultiplier = 16;
  m_bc = {1, 1, 1, 1};
  m_bcTriggered = 0;
  m_realDuration = m_duration;
  if(m_verbose){
    std::cout << ": m_duration = " << m_duration << std::endl;
    std::cout << ": m_frequency = " << m_frequency << std::endl;
  }
}

NoiseScan::~NoiseScan(){}

void NoiseScan::PreRun(){
  m_logFile << "\"m_frequency\": " << m_frequency << " Hz" << endl;
  m_logFile << "\"m_maxRate\": " << m_maxRate << " hit/bc" << endl;
  m_logFile << "\"m_TriggerMultiplier\": " << m_TriggerMultiplier << endl;
  m_logFile << "\"m_bc\": {" << m_bc[0] << ", " << m_bc[1] << ", " << m_bc[2] << ", " << m_bc[3] << "}" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0x4000);
    fe->WriteGlobal();
    //fe->SetMask(0,0);
    Send(fe);
    m_occ[fe->GetName()]   =new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_occInBC[fe->GetName()]=new TH2F(("occInBC_"+fe->GetName()).c_str(),";Column;Row;#frac{Number of hits}{bc}", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_enableMap[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    
    if(m_verbose) std::cout << ": added front-end with chip ID " << fe->GetChipID() << std::endl;
  }
}

void NoiseScan::Run(){

  //Prepare the trigger
  cout << "Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency, m_TriggerMultiplier, m_bc[0], m_bc[1], m_bc[2], m_bc[3]);
  uint32_t period = round(1 / m_frequency);
  uint32_t previousDiff = 1;

  auto start_scan = chrono::steady_clock::now();

  signal(SIGINT, Stop);
  while(!stopScan){
    auto elapsed_time = chrono::steady_clock::now();
    uint32_t diff = chrono::duration_cast<chrono::seconds>(elapsed_time - start_scan).count();

    if(diff % period == 0 and diff != previousDiff){
      previousDiff = diff;
      Trigger();
      m_ntrigs++;
      auto start_read = chrono::steady_clock::now();

      //histogram hit
      uint32_t nhits = 0;
      while(true){
	for(auto fe : GetFEs()){
	  if(!fe->HasHits()) continue;
	  Hit* hit = fe->GetHit();
	  if(hit!=0){
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    nhits++;
	  }
	  fe->NextHit();
	}
	auto end_read = chrono::steady_clock::now();
	uint32_t ms = chrono::duration_cast<chrono::milliseconds>(end_read - start_read).count();
	if(ms > 100) break;
      }
      cout << "NHits in trigger # " << m_ntrigs  << ": " << nhits << endl;
      for(auto fe : GetFEs()){
        Send(fe);
      }
    }
    auto end_scan = chrono::steady_clock::now();
    m_realDuration = chrono::duration_cast<chrono::seconds>(end_scan - start_scan).count();
    if(m_realDuration > m_duration) break;
  }
  std::cout << "m_realDuration: " << m_realDuration << " s" << endl;
}

void NoiseScan::Analysis(){

  for(auto fe : GetFEs()){
    if(stopScan) m_duration = m_realDuration;
    m_occ[fe->GetName()]->SetTitle(("Occupancy map (in "+to_string(m_duration)+" s)").c_str());
    m_occInBC[fe->GetName()]->SetTitle(("Occupancy/bc map (in "+to_string(m_duration)+" s)").c_str());
    m_bcTriggered = m_ntrigs * m_TriggerMultiplier * (m_bc[0] + m_bc[1] + m_bc[2] + m_bc[3]);
    m_logFile << "\"m_duration\": " << m_duration << " s" << endl;
    m_logFile << "\"m_bcTriggered\": " << m_bcTriggered << endl;
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
      for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	uint32_t occupancy = m_occ[fe->GetName()]->GetBinContent(col+1,row+1);
	float occupancyInBC = ((float)occupancy)/((float)m_bcTriggered);
	m_occInBC[fe->GetName()]->SetBinContent(col+1,row+1,occupancyInBC);
        if (occupancyInBC >= m_maxRate){
          m_enableMap[fe->GetName()]->SetBinContent(col+1,row+1,1);
	  fe->SetPixelEnable(col,row,false);
        }
      }
    }
    m_occ[fe->GetName()]->Write();
    m_enableMap[fe->GetName()]->Write();
    m_occInBC[fe->GetName()]->Write();
    delete m_occ[fe->GetName()];
    delete m_enableMap[fe->GetName()];
    delete m_occInBC[fe->GetName()];
  }
}
