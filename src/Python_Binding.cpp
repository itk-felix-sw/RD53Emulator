#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
//Main classes
#include <RD53Emulator/Handler.h>
#include <RD53Emulator/FrontEnd.h>
#include <RD53Emulator/Configuration.h>
#include <RD53Emulator/Command.h>
#include <RD53Emulator/TemperatureSensor.h>
#include <RD53Emulator/RadiationSensor.h>
#include <RD53Emulator/Hit.h>
#include <RD53Emulator/Field.h>
#include <RD53Emulator/Register.h>
//Commands
#include <RD53Emulator/BCR.h>
#include <RD53Emulator/Cal.h>
#include <RD53Emulator/ECR.h>
#include <RD53Emulator/Noop.h>
#include <RD53Emulator/Pulse.h>
#include <RD53Emulator/RdReg.h>
#include <RD53Emulator/Sync.h>
#include <RD53Emulator/WrReg.h>
#include <RD53Emulator/Encoder.h>
#include <RD53Emulator/Decoder.h>
//Scans
#include <RD53Emulator/DigitalScan.h>
#include <RD53Emulator/AnalogScan.h>
#include <RD53Emulator/ThresholdScan.h>
#include <RD53Emulator/TestScan.h>
#include <RD53Emulator/SensorScan.h>
#include <RD53Emulator/ConfigScan.h>
#include <RD53Emulator/ToTScan.h>
#include <RD53Emulator/PreampTune.h>
#include <RD53Emulator/MaskCheck.h>
#include <RD53Emulator/GlobalThresholdTune.h>
#include <RD53Emulator/PixelThresholdTune.h>
#include <RD53Emulator/NoiseScan.h>
#include <RD53Emulator/SourceScan.h>
#include <RD53Emulator/ExternalTriggerScan.h>

namespace py = pybind11;
using namespace RD53A;

PYBIND11_MODULE(libRD53A, m) {
	m.doc() = "Python binding of RD53A";

    py::class_<Handler>(m, "Handler")
        .def(py::init())
        .def("SetVerbose", &Handler::SetVerbose)
        .def("SetContext", &Handler::SetContext)
        .def("SetInterface", &Handler::SetInterface)
        .def("SetMapping", &Handler::SetMapping)
        .def("AddMapping", &Handler::AddMapping)
        .def("AddFE", py::overload_cast<std::string, FrontEnd*>(&Handler::AddFE)) //Requires FrontEnd class
        .def("AddFE", py::overload_cast<std::string, std::string>(&Handler::AddFE))
        .def("AddFEbyString", &Handler::AddFEbyString)
        .def("SaveFE", &Handler::SaveFE)
        .def("Config", &Handler::Config)
        .def("Connect", &Handler::Connect)
        .def("Disconnect", &Handler::Disconnect)
        .def("InitRun", &Handler::InitRun)
        .def("PreRun", &Handler::PreRun)
        .def("Run", &Handler::Run)
        .def("Analysis", &Handler::Analysis)
        .def("SaveConfig", &Handler::SaveConfig)
        .def("Save", &Handler::Save)
        .def("Trigger", &Handler::Trigger)
        .def("Send", &Handler::Send) //Requires FrontEnd class
        .def("GetFE", &Handler::GetFE) //Requires FrontEnd class
        .def("GetFEs", &Handler::GetFEs) //Requires FrontEnd class
        .def("SetOutPath", &Handler::SetOutPath)
        .def("GetOutPath", &Handler::GetOutPath)
        .def("GetFullOutPath", &Handler::GetFullOutPath)
        .def("SetRetune", &Handler::SetRetune)
        .def("GetRetune", &Handler::GetRetune)
        .def("SetEnableOutput", &Handler::SetEnableOutput)
        .def("GetEnableOutput", &Handler::GetEnableOutput)
        .def("SetCharge", &Handler::SetCharge)
        .def("GetCharge", &Handler::GetCharge)
        .def("SetToT", &Handler::SetToT)
        .def("GetToT", &Handler::GetToT)
        .def("SetScan", &Handler::SetScan)
        .def("SetThreshold", &Handler::SetThreshold)
        .def("GetThreshold", &Handler::GetThreshold)
        .def("SetCommandLine", &Handler::SetCommandLine)
        .def("GetConfig", &Handler::GetConfig)
        .def("GetResults", &Handler::GetResults);

    py::class_<FrontEnd>(m, "FrontEnd")
        .def(py::init())
        .def("SetVerbose", &FrontEnd::SetVerbose)
        .def("GetChipID", &FrontEnd::GetChipID)
        .def("SetChipID", &FrontEnd::SetChipID)
        .def("SetName", &FrontEnd::SetName)
        .def("GetName", &FrontEnd::GetName)
        .def("GetConfig", &FrontEnd::GetConfig) //Requires Config class
        .def("Clear", &FrontEnd::Clear)
        .def("Reset", &FrontEnd::Reset)
        .def("IsActive", &FrontEnd::IsActive)
        .def("SetActive", &FrontEnd::SetActive)
        .def("SetGlobalThreshold", &FrontEnd::SetGlobalThreshold)
        .def("GetGlobalThreshold", py::overload_cast<uint32_t>(&FrontEnd::GetGlobalThreshold))
        .def("GetGlobalThreshold", py::overload_cast<uint32_t, uint32_t>(&FrontEnd::GetGlobalThreshold))
        .def("SetPixelEnable", &FrontEnd::SetPixelEnable)
        .def("GetPixelEnable", &FrontEnd::GetPixelEnable)
        .def("SetPixelInject", &FrontEnd::SetPixelInject)
        .def("GetPixelInject", &FrontEnd::GetPixelInject)
        .def("SetPixelHitbus", &FrontEnd::SetPixelHitbus)
        .def("GetPixelHitbus", &FrontEnd::GetPixelHitbus)
        .def("SetPixelThreshold", &FrontEnd::SetPixelThreshold)
        .def("GetPixelThreshold", &FrontEnd::GetPixelThreshold)
        .def("SetPixelGain", &FrontEnd::SetPixelGain)
        .def("GetPixelGain", &FrontEnd::GetPixelGain)
        .def("SetGlobalConfig", &FrontEnd::SetGlobalConfig)
        .def("GetGlobalConfig", &FrontEnd::GetGlobalConfig)
        .def("WriteGlobal", &FrontEnd::WriteGlobal)
        .def("ReadGlobal", &FrontEnd::ReadGlobal)
        .def("WritePixels", &FrontEnd::WritePixels)
        .def("WritePixelPair", &FrontEnd::WritePixelPair)
        .def("ReadPixels", &FrontEnd::ReadPixels)
        .def("ReadPixelPair", &FrontEnd::ReadPixelPair)
        .def("SetPixelMask", &FrontEnd::SetPixelMask)
        .def("GetPixelMask", &FrontEnd::GetPixelMask)
        .def("SetCoreColumn", &FrontEnd::SetCoreColumn)
        .def("EnableCoreColumn", &FrontEnd::EnableCoreColumn)
        .def("InitAdc", &FrontEnd::InitAdc)
        .def("ReadSensor", &FrontEnd::ReadSensor)
        .def("GetTemperatureSensor", &FrontEnd::GetTemperatureSensor) //Requires TemperaturSensor class
        .def("GetRadiationSensor", &FrontEnd::GetRadiationSensor) //Requires RadiationSensor class
        .def("Trigger", &FrontEnd::Trigger)
        .def("GetHit", &FrontEnd::GetHit) //Requires Hit class
        .def("NextHit", &FrontEnd::NextHit)
        .def("HasHits", &FrontEnd::HasHits)
        .def("HandleData", &FrontEnd::HandleData)
        .def("ProcessCommands", &FrontEnd::ProcessCommands)
        .def("GetBytes", &FrontEnd::GetBytes)
        .def("GetLength", &FrontEnd::GetLength);

    py::class_<Configuration>(m, "Configuration")
        .def(py::init())
        .def("SetVerbose", &Configuration::SetVerbose)
        .def("Size", &Configuration::Size)
        .def("GetRegister", &Configuration::GetRegister)
        .def("SetRegisterValue", &Configuration::SetRegisterValue)
        .def("SetField", py::overload_cast<std::string, uint16_t>(&Configuration::SetField))
        .def("SetField", py::overload_cast<uint32_t, uint16_t>(&Configuration::SetField))
        .def("GetField", py::overload_cast<std::string>(&Configuration::GetField))
        .def("GetField", py::overload_cast<uint32_t>(&Configuration::GetField))
        .def("GetUpdatedRegisters", &Configuration::GetUpdatedRegisters)
        .def("GetRegisters", &Configuration::GetRegisters)
        .def("SetRegisters", &Configuration::SetRegisters);

    py::class_<TemperatureSensor>(m, "TemperatureSensor")
        .def(py::init())
        .def("SetADC", &TemperatureSensor::SetADC)
        .def("SetValue", &TemperatureSensor::SetValue)
        .def("GetADC", &TemperatureSensor::GetADC)
        .def("GetValue", &TemperatureSensor::GetValue)
        .def("SetPower", &TemperatureSensor::SetPower)
        .def("GetPower", &TemperatureSensor::GetPower)
        .def("Update", &TemperatureSensor::Update)
        .def("isUpdated", &TemperatureSensor::isUpdated)
        .def("SetCalibration", &TemperatureSensor::SetCalibration)
        .def("GetCalibration", &TemperatureSensor::GetCalibration)
        .def("GetTemperature", &TemperatureSensor::GetTemperature);


    py::class_<RadiationSensor>(m, "RadiationSensor")
        .def(py::init())
        .def("SetADC", &RadiationSensor::SetADC)
        .def("SetValue", &RadiationSensor::SetValue)
        .def("GetADC", &RadiationSensor::GetADC)
        .def("GetValue", &RadiationSensor::GetValue)
        .def("SetPower", &RadiationSensor::SetPower)
        .def("GetPower", &RadiationSensor::GetPower)
        .def("Update", &RadiationSensor::Update)
        .def("isUpdated", &RadiationSensor::isUpdated);


    py::class_<Hit>(m, "Hit")
        .def(py::init())
        .def(py::init<uint32_t, uint32_t, uint32_t>())
        .def("Clone", &Hit::Clone)
        .def("Update", &Hit::Update)
        .def("Set", &Hit::Set)
        .def("GetTID", &Hit::GetTID)
        .def("GetTTag", &Hit::GetTTag)
        .def("GetBCID", &Hit::GetBCID)
        .def("GetCol", &Hit::GetCol)
        .def("GetRow", &Hit::GetRow)
        .def("GetTOT", &Hit::GetTOT)
        .def("GetEvNum", &Hit::GetEvNum)
        .def("SetTID", &Hit::SetTID)
        .def("SetTTag", &Hit::SetTTag)
        .def("SetBCID", &Hit::SetBCID)
        .def("SetCol", &Hit::SetCol)
        .def("SetRow", &Hit::SetRow)
        .def("SetTOT", &Hit::SetTOT)
        .def("SetEvNum", &Hit::SetEvNum)
        .def("ToString", &Hit::ToString);


    py::class_<Field>(m, "Field")
        .def(py::init<Register*, uint32_t, uint32_t, uint32_t, bool>())//Requires Register class
        .def("SetValue", &Field::SetValue)
        .def("GetValue", &Field::GetValue);
        

    py::class_<Register>(m, "Register")
        .def(py::init())
        .def("SetValue", &Register::SetValue)
        .def("GetValue", &Register::GetValue)
        .def("Update", &Register::Update)
        .def("IsUpdated", &Register::IsUpdated);

    py::class_<Command>(m, "Command");

    py::class_<BCR, Command>(m, "BCR")
        .def(py::init<>())
        .def(py::init<BCR*>())
        .def("Clone", &BCR::Clone)
        .def("ToString", &BCR::ToString)
        .def("UnPack", &BCR::UnPack)
        .def("Pack", &BCR::Pack)
        .def("GetType", &BCR::GetType);

    py::class_<Cal, Command>(m, "Cal")
        .def(py::init<>())
        .def(py::init<uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t>())
        .def(py::init<Cal*>())
        .def("Clone", &Cal::Clone)
        .def("ToString", &Cal::ToString)
        .def("UnPack", &Cal::UnPack)
        .def("Pack", &Cal::Pack)
        .def("GetType", &Cal::GetType)
        .def("SetChipId", &Cal::SetChipId)
        .def("GetChipId", &Cal::GetChipId)
        .def("SetEdgeMode", &Cal::SetEdgeMode)
        .def("GetEdgeMode", &Cal::GetEdgeMode)
        .def("SetEdgeDelay", &Cal::SetEdgeDelay)
        .def("GetEdgeDelay", &Cal::GetEdgeDelay)
        .def("SetEdgeWidth", &Cal::SetEdgeWidth)
        .def("GetEdgeWidth", &Cal::GetEdgeWidth)
        .def("SetAuxMode", &Cal::SetAuxMode)
        .def("GetAuxMode", &Cal::GetAuxMode)
        .def("SetAuxDelay", &Cal::SetAuxDelay)
        .def("GetAuxDelay", &Cal::GetAuxDelay);

    py::class_<ECR, Command>(m, "ECR")
        .def(py::init<>())
        .def(py::init<ECR*>())
        .def("Clone", &ECR::Clone)
        .def("ToString", &ECR::ToString)
        .def("UnPack", &ECR::UnPack)
        .def("Pack", &ECR::Pack)
        .def("GetType", &ECR::GetType);

    py::class_<Noop, Command>(m, "Noop")
        .def(py::init<>())
        .def(py::init<Noop*>())
        .def("Clone", &Noop::Clone)
        .def("ToString", &Noop::ToString)
        .def("UnPack", &Noop::UnPack)
        .def("Pack", &Noop::Pack)
        .def("GetType", &Noop::GetType);

    py::class_<Pulse, Command>(m, "Pulse")
        .def(py::init<>())
        .def(py::init<uint32_t, uint32_t>())
        .def(py::init<Pulse*>())
        .def("Clone", &Pulse::Clone)
        .def("ToString", &Pulse::ToString)
        .def("UnPack", &Pulse::UnPack)
        .def("Pack", &Pulse::Pack)
        .def("GetType", &Pulse::GetType)
        .def("SetChipId", &Pulse::SetChipId)
        .def("GetChipId", &Pulse::GetChipId)
        .def("SetLength", &Pulse::SetLength)
        .def("GetLength", &Pulse::GetLength);

    py::class_<RdReg, Command>(m, "RdReg")
        .def(py::init<>())
        .def(py::init<uint32_t, uint32_t>())
        .def(py::init<RdReg*>())
        .def("Clone", &RdReg::Clone)
        .def("ToString", &RdReg::ToString)
        .def("UnPack", &RdReg::UnPack)
        .def("Pack", &RdReg::Pack)
        .def("GetType", &RdReg::GetType)
        .def("SetChipId", &RdReg::SetChipId)
        .def("GetChipId", &RdReg::GetChipId)
        .def("SetAddress", &RdReg::SetAddress)
        .def("GetAddress", &RdReg::GetAddress);

    py::class_<Sync, Command>(m, "Sync")
        .def(py::init<>())
        .def(py::init<Sync*>())
        .def("Clone", &Sync::Clone)
        .def("ToString", &Sync::ToString)
        .def("UnPack", &Sync::UnPack)
        .def("Pack", &Sync::Pack)
        .def("GetType", &Sync::GetType);

    py::class_<Trigger, Command>(m, "Trigger")
        .def(py::init<>())
        .def(py::init<Trigger*>())
        .def(py::init<uint32_t, uint32_t>())
        .def(py::init<bool, bool, bool, bool, uint32_t>())
        .def("Clone", &Trigger::Clone)
        .def("ToString", &Trigger::ToString)
        .def("UnPack", &Trigger::UnPack)
        .def("Pack", &Trigger::Pack)
        .def("GetType", &Trigger::GetType)
        .def("SetPattern", &Trigger::SetPattern)
        .def("GetPattern", &Trigger::GetPattern)
        .def("SetTag", &Trigger::SetTag)
        .def("GetTag", &Trigger::GetTag)
        .def("SetTrigger", &Trigger::SetTrigger)
        .def("GetTrigger", &Trigger::GetTrigger);

    py::class_<WrReg, Command>(m, "WrReg")
        .def(py::init<>())
        .def(py::init<uint32_t, uint32_t, uint32_t>())
        .def(py::init<WrReg*>())
        .def("Clone", &WrReg::Clone)
        .def("ToString", &WrReg::ToString)
        .def("UnPack", &WrReg::UnPack)
        .def("Pack", &WrReg::Pack)
        .def("GetType", &Command::GetType)
        .def("SetChipId", &WrReg::SetChipId)
        .def("GetChipId", &WrReg::GetChipId)
        .def("SetAddress", &WrReg::SetAddress)
        .def("GetAddress", &WrReg::GetAddress)
        .def("SetValue", &WrReg::SetValue)
        .def("GetValue", &WrReg::GetValue)
        .def("SetMode", &WrReg::SetMode)
        .def("GetMode", &WrReg::GetMode);

    py::class_<Encoder>(m, "Encoder")
      .def(py::init<>())
      .def("AddBytes",&Encoder::AddBytes)
      .def("SetBytes",&Encoder::SetBytes)
      .def("AddCommand",&Encoder::AddCommand)
      .def("ClearBytes",&Encoder::ClearBytes)
      .def("ClearCommands",&Encoder::ClearCommands)
      .def("Clear",&Encoder::Clear)
      .def("GetByteString",&Encoder::GetByteString)
      .def("GetBytes",&Encoder::GetBytes)
      .def("GetLength",&Encoder::GetLength)
      .def("Encode",&Encoder::Encode)
      .def("Decode",&Encoder::Decode)
      .def("GetCommands",&Encoder::GetCommands);

    py::class_<Decoder>(m, "Decoder")
      .def(py::init<>())
      .def("AddBytes",&Decoder::AddBytes)
      .def("SetBytes",&Decoder::SetBytes)
      .def("AddFrame",&Decoder::AddFrame)
      .def("ClearBytes",&Decoder::ClearBytes)
      .def("ClearFrames",&Decoder::ClearFrames)
      .def("Clear",&Decoder::Clear)
      .def("GetByteString",&Decoder::GetByteString)
      .def("GetBytes",py::overload_cast<uint8_t*,uint32_t&,bool>(&Decoder::GetBytes))
      .def("GetLength",&Decoder::GetLength)
      .def("Encode",&Decoder::Encode)
      .def("Decode",&Decoder::Decode)
      .def("GetFrames",&Decoder::GetFrames);

    //Available Handlers
    py::class_<DigitalScan, Handler>(m, "DigitalScan")
        .def(py::init<>());

    py::class_<AnalogScan, Handler>(m, "AnalogScan")
        .def(py::init<>());

    py::class_<ThresholdScan, Handler>(m, "ThresholdScan")
        .def(py::init<>());

    py::class_<TestScan, Handler>(m, "TestScan")
        .def(py::init<>());

    py::class_<SensorScan, Handler>(m, "SensorScan")
        .def(py::init<>());

    py::class_<ConfigScan, Handler>(m, "ConfigScan")
        .def(py::init<>());

    py::class_<ToTScan, Handler>(m, "ToTScan")
        .def(py::init<>());

    py::class_<PreampTune, Handler>(m, "PreampTune")
        .def(py::init<>());

    py::class_<MaskCheck, Handler>(m, "MaskCheck")
        .def(py::init<>());

    py::class_<GlobalThresholdTune, Handler>(m, "GlobalThresholdTune")
        .def(py::init<>());

    py::class_<PixelThresholdTune, Handler>(m, "PixelThresholdTune")
        .def(py::init<>());

    py::class_<NoiseScan, Handler>(m, "NoiseScan")
        .def(py::init<>());

    py::class_<SourceScan, Handler>(m, "SourceScan")
        .def(py::init<>());

    py::class_<ExternalTriggerScan, Handler>(m, "ExternalTriggerScan")
        .def(py::init<>());
}
