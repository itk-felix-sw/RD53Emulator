#include "RD53Emulator/TestScan.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53A;

TestScan::TestScan(){
  m_mask_step = 1; //0,1,2,4,8,16
  m_cc_max = 4; //0=40, 1=4, 2=8, 3=1
  m_ntrigs = 100;

  if(m_verbose){
    std::cout << "TestScan::Run: m_mask_step = " << m_mask_step << std::endl;
    std::cout << "TestScan::Run: m_cc_max = " << m_cc_max << std::endl;
    std::cout << "TestScan::Run: m_ntrigs = " << m_ntrigs << std::endl;
  }

}

TestScan::~TestScan(){}

void TestScan::PreRun(){
  cout << "TestScan::PreRun" << endl;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,1);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,56);
    fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0x4000);
    fe->ProcessCommands();
    Send(fe);

    
    cout << "TestScan::PreRun - Sync " << endl;
    fe->SetGlobalThreshold(Pixel::Sync,5);
    fe->ProcessCommands();
    Send(fe);
    cout << "TestScan::PreRun - Lin " << endl;
    fe->SetGlobalThreshold(Pixel::Lin, 5);
    fe->ProcessCommands();
    Send(fe);
    cout << "TestScan::PreRun - Diff " << endl;
    fe->SetGlobalThreshold(Pixel::Diff,5);
    fe->ProcessCommands();
    Send(fe);
    

    m_occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row",Matrix::NUM_COLS,0.5,Matrix::NUM_COLS+0.5,Matrix::NUM_ROWS,0.5,Matrix::NUM_ROWS+0.5);
    m_tid[fe->GetName()]=new TH1I(("tid_"+fe->GetName()).c_str(),";Trigger ID",16,-0.5,16.5);
    m_ttag[fe->GetName()]=new TH1I(("ttag_"+fe->GetName()).c_str(),";Trigger Tag",16,-0.5,16.5);
    m_bcid[fe->GetName()]=new TH1I(("bcid_"+fe->GetName()).c_str(),";BCID",0xFFF,-0.5,0xFFF+0.5);

    m_logFile << "\"m_mask_step\": " << m_mask_step << endl;
    m_logFile << "\"m_cc_max\": " << m_cc_max << endl;
    m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
    if(m_verbose) std::cout << "TestScan::Run: added front-end with chip ID " << fe->GetChipID() << std::endl;
  }
  cout << "TestScan::PreRun end" << endl;

}
void TestScan::Run(){
  
  sleep(1);
  //Prepare the trigger
  cout << "TestScan::Run: Prepare the TRIGGER" << endl;
  
  for(uint32_t delay=50; delay<51;delay++){ 
    
    cout << "Delay: " << delay << endl;
    PrepareTrigger(delay,5,true,true,true,true);
  
  for(auto fe : GetFEs()){
    //enable core col 33
    cout << "Select core column" << endl;
    // for (int c= 0; c<10; c++) fe->EnableCoreColumn(c,true);
    // for (int c=10; c<20; c++) fe->EnableCoreColumn(c,false);
    // for (int c=20; c<30; c++) fe->EnableCoreColumn(c,true);
    // for (int c=30; c<40; c++) fe->EnableCoreColumn(c,false);
    for (int c=40; c<50; c++) fe->EnableCoreColumn(c,true);
    fe->ProcessCommands();
    Send(fe);
    sleep(1);

    cout << "Enable calibration pulse" << endl;
    // for (int c=  0; c< 20; c++) fe->EnableCalDoubleColumn(c,true);
    // for (int c= 20; c< 40; c++) fe->EnableCalDoubleColumn(c,false);
    // for (int c= 40; c< 60; c++) fe->EnableCalDoubleColumn(c,true);
    // for (int c= 60; c< 80; c++) fe->EnableCalDoubleColumn(c,false);
    // for (int c= 80; c<100; c++) fe->EnableCalDoubleColumn(c,true);
    // for (int c=100; c<120; c++) fe->EnableCalDoubleColumn(c,false);
    // for (int c=120; c<140; c++) fe->EnableCalDoubleColumn(c,true);
    // for (int c=140; c<160; c++) fe->EnableCalDoubleColumn(c,false);
    for (int c=160; c<180; c++) fe->EnableCalDoubleColumn(c,true);
    for (int c=180; c<200; c++) fe->EnableCalDoubleColumn(c,true);
    fe->ProcessCommands();
    Send(fe);
    sleep(1);
    
    //cout << "Set pixel mask enable" << endl;
    for(int c=264; c<400; c++){
      for(int r=0; r<192; r++){
        fe->SetPixelEnable(c,r,true);
        fe->SetPixelInject(c,r,true);
      }
      fe->ProcessCommands();
      Send(fe);
    }
    sleep(1);
    
    uint32_t nhits2=0;

    //Trigger and read-out
    for(uint32_t trig = 0;trig < m_ntrigs; trig++){

      cout << "TestScan::Run: Clear hits" << endl;
      for(auto fe : GetFEs()){
	fe->ClearHits();
      }

      cout << "TestScan::Run: Trigger" << endl;
      Trigger();

      auto start = chrono::steady_clock::now();

      //histogram hit
      uint32_t nhits=0;
      while(true){
        
          for(auto fe : GetFEs()){
            if(!fe->HasHits()) continue;
            Hit* hit = fe->GetHit();
            cout << hit->ToString() << endl;
            if(hit!=0){
              m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
              m_tid[fe->GetName()]->Fill(hit->GetTID());
              m_ttag[fe->GetName()]->Fill(hit->GetTTag());
              m_bcid[fe->GetName()]->Fill(hit->GetBCID());
              nhits++;
            }
            fe->NextHit();
          }
          auto end = chrono::steady_clock::now();
          uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
          if(ms>500){break;}
        }
        nhits2+=nhits;
        //cout << __PRETTY_FUNCTION__ << "NHits per trigger: " << nhits << endl;
      }
      cout << "TestScan::Run: NHits in step: " << nhits2 << endl;
      for(auto fe : GetFEs()){
        Send(fe);
      }
  }
  }
}

void TestScan::Analysis(){
  for(auto fe : GetFEs()){
    m_occ[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    delete m_occ[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
  }
}                                                                                                                                        
