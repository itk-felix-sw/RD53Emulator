#include "RD53Emulator/HitFrame.h"
#include <sstream>
#include <iomanip>
#include <bitset>
#include <iostream>

using namespace std;
using namespace RD53A;

HitFrame::HitFrame(){
  m_ccol = 0;
  m_crow = 0;
  m_creg = 0;
  m_tot[0] = 0;
  m_tot[1] = 0;
  m_tot[2] = 0;
  m_tot[3] = 0;
}

HitFrame::HitFrame(uint32_t core_col, uint32_t core_row, uint32_t core_region, uint32_t tot1, uint32_t tot2, uint32_t tot3, uint32_t tot4){
  SetHit(core_col, core_row, core_region, tot1, tot2, tot3, tot4);
}

HitFrame::~HitFrame(){}

uint32_t HitFrame::UnPack(uint8_t * bytes, uint32_t maxlen){
  // //Original
  m_ccol   = (bytes[0]&0xFC)>>2; //11111100 >> 2 First 6
  m_crow   = (bytes[0]&0x03)<<4; //00000011 << 4 Last 2 +
  m_crow  |= (bytes[1]&0xF0)>>4; //11110000 >> 4 First 4 = 6
  m_creg   = (bytes[1]&0x0F)<<0; //00001111 What is this frame
  m_tot[3] = ((((bytes[2]&0xF0)>>4)+1)&0xF);
  m_tot[2] = ((((bytes[2]&0x0F)<<0)+1)&0xF);
  m_tot[1] = ((((bytes[3]&0xF0)>>4)+1)&0xF);
  m_tot[0] = ((((bytes[3]&0x0F)<<0)+1)&0xF);

  if(bytes[0]==0 && bytes[1]==0 && bytes[2]==0 && bytes[3]==0){return 0;}
  if(m_ccol>49 || m_crow>23 ){return 0;}
  return 4;
}

string HitFrame::ToString(){
  ostringstream os;
  os << "HitFrame "
     << " Col:" << GetCol()
     << " Row:" << GetRow()
     << " CCol: 0x" << hex << m_ccol << dec
     << " CRow: 0x" << hex << m_crow << dec
     << " CReg: 0x" << hex << m_creg << dec
     << " TOT1 0x" << hex << m_tot[0] << dec
     << " TOT2 0x" << hex << m_tot[1] << dec
     << " TOT3 0x" << hex << m_tot[2] << dec
     << " TOT4 0x" << hex << m_tot[3] << dec;
  return os.str();
}

uint32_t HitFrame::Pack(uint8_t * bytes){

  bytes[0]  = (m_ccol  <<2)&0xFC;
  bytes[0] |= (m_crow  >>4)&0x03;
  bytes[1]  = (m_crow  <<4)&0xF0;
  bytes[1] |= (m_creg  >>0)&0x0F;
  bytes[3]  = ((m_tot[0]==0?0xF:(m_tot[0]-1))<<4)&0xF0;
  bytes[3] |= ((m_tot[1]==0?0xF:(m_tot[1]-1))>>0)&0x0F;
  bytes[2]  = ((m_tot[2]==0?0xF:(m_tot[2]-1))<<4)&0xF0;
  bytes[2] |= ((m_tot[3]==0?0xF:(m_tot[3]-1))>>0)&0x0F;

  return 4;
}

uint32_t HitFrame::GetType(){
  return Frame::HIT;
}

void HitFrame::SetHit(uint32_t core_col, uint32_t core_row, uint32_t core_region, uint32_t tot1, uint32_t tot2, uint32_t tot3, uint32_t tot4){
  m_ccol   = core_col&0x3F;
  m_crow   = core_row&0x3F;
  m_creg   = core_region&0x0F;
  m_tot[0] = tot1&0x0F;
  m_tot[1] = tot2&0x0F;
  m_tot[2] = tot3&0x0F;
  m_tot[3] = tot4&0x0F;
}

void HitFrame::SetHit(uint32_t quad_col, uint32_t row, uint32_t * tot){
  m_ccol = (quad_col>>1)&0x3F;
  m_crow = (row>>3)&0x3F;
  m_creg = ((row<<1)|(quad_col&0x1))&0x0F;
  m_tot[0] = tot[0]&0x0F;
  m_tot[1] = tot[1]&0x0F;
  m_tot[2] = tot[2]&0x0F;
  m_tot[3] = tot[3]&0x0F;
}

void HitFrame::SetCoreCol(uint32_t ccol) {
  m_ccol = ccol&0x3F;
}

void HitFrame::SetCoreRow(uint32_t crow) {
  m_crow = crow&0x1FF;
}

void HitFrame::SetCoreReg(uint32_t creg) {
  m_creg = creg&0x01;
}

void HitFrame::SetTOT(uint32_t idx, uint32_t tot) {
  m_tot[idx]=tot;
}

uint32_t HitFrame::GetCoreCol() {
  return m_ccol;
}

uint32_t HitFrame::GetCoreRow() {
  return m_crow;
}

uint32_t HitFrame::GetCoreReg() {
  return m_creg;
}

uint32_t HitFrame::GetTOT(uint32_t idx) {
  return m_tot[idx];
}

uint32_t HitFrame::GetCol() {
  return m_ccol*8+(m_creg&0x1)*4;
}

uint32_t HitFrame::GetRow() {
  return m_crow*8+((m_creg>>1)&0x7);
}
