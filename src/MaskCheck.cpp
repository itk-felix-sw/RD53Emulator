#include "RD53Emulator/MaskCheck.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53A;

MaskCheck::MaskCheck(){
  m_mask_step = 4;
  m_cc_max = 4;
  m_ntrigs = 5;
  m_TriggerLatency = 58;

  if(m_verbose){
    std::cout << __PRETTY_FUNCTION__ << ": m_mask_step = " << m_mask_step << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_cc_max = " << m_cc_max << std::endl;
    std::cout << __PRETTY_FUNCTION__ << ": m_ntrigs = " << m_ntrigs << std::endl;
  }
}

MaskCheck::~MaskCheck(){}

void MaskCheck::PreRun(){
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,1);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0x4000);
    fe->WriteGlobal();
    //fe->SetMask(0,0);
    Send(fe);

    m_logFile << "\"m_mask_step\": " << m_mask_step << endl;
    m_logFile << "\"m_cc_max\": " << m_cc_max << endl;
    m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": added front-end with chip ID " << fe->GetChipID() << std::endl;
  }
  m_enableMap = new TH2I**[m_mask_step];
  for(uint32_t ix=0; ix<m_mask_step; ix++){
    m_enableMap[ix]=new TH2I*[m_cc_max];
    for(uint32_t iy=0; iy<m_cc_max; iy++){
      char name[1000];
      sprintf(name, "EnabledPixels_%d_%d", ix, iy);
      char title[100];
      sprintf(title, "Enabled Pixels; Column; Row");
      m_enableMap[ix][iy] = new TH2I(name,title,Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }
  }

}
void MaskCheck::Run(){

  //Prepare the trigger
  cout << __PRETTY_FUNCTION__ << "Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency);

  //change the mask
  for(uint32_t mask = 0; mask < m_mask_step; mask++){
    cout << __PRETTY_FUNCTION__ << "Mask: " << (mask+1) << "/" << m_mask_step << endl;
    
    //select the core column
    for(uint32_t cc = 0; cc < m_cc_max; cc++){
      cout << __PRETTY_FUNCTION__ << "Core Column: " << (cc+1) << "/" << m_cc_max << endl;
      for(auto fe : GetFEs()){
	fe->SetCoreColumn(m_cc_max,cc,"all");
	Send(fe);
      }

      uint32_t nhits2=0;

      //Trigger and read-out
      for(uint32_t trig = 0; trig < m_ntrigs; trig++){
        Trigger();
        auto start = chrono::steady_clock::now();
        //histogram hit
        uint32_t nhits=0;
        while(true){
          
          for(auto fe : GetFEs()){
            if(!fe->HasHits()) continue;
            Hit* hit = fe->GetHit();
            if(hit!=0){
	      m_enableMap[mask][cc]->Fill(hit->GetCol(),hit->GetRow());
              nhits++;
            }
            fe->NextHit();
          }
          auto end = chrono::steady_clock::now();
          uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
          if(ms>1000){break;}
        }
        nhits2+=nhits;
      }
      cout << __PRETTY_FUNCTION__ << "NHits in step: " << nhits2 << endl;
      for(auto fe : GetFEs()){
        Send(fe);
      }
    }
  }
}

void MaskCheck::Analysis(){
  for(uint32_t mask = 0; mask < m_mask_step; mask++){
    for(uint32_t cc = 0; cc < m_cc_max; cc++){
      m_enableMap[mask][cc]->Write();
      delete m_enableMap[mask][cc];
    }
  }
}
