#include "RD53Emulator/PixelThresholdTune.h"
#include "RD53Emulator/Tools.h"

#include <iostream>
#include <sstream>
#include <chrono>

using namespace std;
using namespace RD53A;

PixelThresholdTune::PixelThresholdTune(){
  m_mask_step = 4;
  m_cc_max = 4;
  m_ntrigs = 10;
  m_TriggerLatency = 48;
  m_chargeMin = 800;
  m_chargeMax = 1200;
  m_chargeStep = 100;
  m_nSteps = 1 + (m_chargeMax - m_chargeMin) / m_chargeStep;
  m_meanThFit = 0.;
  m_sigmaThFit = 0.;
  m_colMin = 0;
  m_colMax = 400;
  m_vcalMed = 500;

  if(m_verbose){
    cout << __PRETTY_FUNCTION__ << ": m_mask_step = " << m_mask_step << endl;
    cout << __PRETTY_FUNCTION__ << ": m_cc_max = " << m_cc_max << endl;
    cout << __PRETTY_FUNCTION__ << ": m_ntrigs = " << m_ntrigs << endl;
    cout << __PRETTY_FUNCTION__ << ": m_nSteps = " << m_nSteps << endl;
  }
}

PixelThresholdTune::~PixelThresholdTune(){}

void PixelThresholdTune::PreRun(){
  m_thresholdTarget = m_threshold;
  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Sync));
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Lin));
    m_mem_thres[fe->GetName()].push_back(fe->GetGlobalThreshold(Pixel::Diff));
    if(m_scan.find("PixLin")!=string::npos){
      m_colMin = 128; m_colMax = 264; m_whichfe = "lin";
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
    }
    else if(m_scan.find("PixDiff")!=string::npos){
      m_colMin = 264; m_colMax = 400; m_whichfe = "diff";
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
    }
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->WriteGlobal();

    Send(fe);

    m_occ[fe->GetName()]=new TH2I*[m_nSteps];
    for(uint32_t iStep = 0; iStep < m_nSteps; iStep++){
      uint32_t charge = m_chargeMin + iStep * m_chargeStep;
      m_occ[fe->GetName()][iStep] = new TH2I(("occ_"+to_string(iStep)+"_"+fe->GetName()).c_str(),("Occupancy map, Q="+to_string(charge)+" e-;Column;Row;Number of hits").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }
    m_histoGaus[fe->GetName()]=new TH1F***[2];
    m_sCurve[fe->GetName()]=new TGraph***[2];
    m_meanThresholdHisto[fe->GetName()]=new TH2F*[2];
    m_meanThresholdFit[fe->GetName()]=new TH2F*[2];
    m_sigmaThresholdHisto[fe->GetName()]=new TH2F*[2];
    m_sigmaThresholdFit[fe->GetName()]=new TH2F*[2];
    for(uint32_t bitStep=0; bitStep<2; bitStep++){
      m_histoGaus[fe->GetName()][bitStep]=new TH1F**[Matrix::NUM_ROWS];
      m_sCurve[fe->GetName()][bitStep]=new TGraph**[Matrix::NUM_ROWS];
      m_meanThresholdHisto[fe->GetName()][bitStep] = new TH2F(("meanThresholdHisto_"+to_string(bitStep)+"_"+fe->GetName()).c_str(),("Mean threshold (histogram), step="+to_string(bitStep)+";Column;Row").c_str(),Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
      m_meanThresholdFit[fe->GetName()][bitStep] = new TH2F(("meanThresholdFit_"+to_string(bitStep)+"_"+fe->GetName()).c_str(),("Mean threshold (fit), step="+to_string(bitStep)+";Column;Row").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
      m_sigmaThresholdHisto[fe->GetName()][bitStep] = new TH2F(("sigmaThresholdHisto_"+to_string(bitStep)+"_"+fe->GetName()).c_str(),("Sigma threshold (histogram), step="+to_string(bitStep)+";Column;Row").c_str(),Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
      m_sigmaThresholdFit[fe->GetName()][bitStep] = new TH2F(("sigmaThresholdFit_"+to_string(bitStep)+"_"+fe->GetName()).c_str(),("Sigma threshold (fit), step="+to_string(bitStep)+";Column;Row").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
      for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
        m_histoGaus[fe->GetName()][bitStep][row]=new TH1F*[Matrix::NUM_COLS];
        m_sCurve[fe->GetName()][bitStep][row]=new TGraph*[Matrix::NUM_COLS];
        for(uint32_t col = m_colMin; col < m_colMax; col++){
          m_histoGaus[fe->GetName()][bitStep][row][col] = new TH1F(("histogaus_"+to_string(bitStep)+"_"+to_string(row)+"_"+to_string(col)+"_"+fe->GetName()).c_str(),("Mean threshold distribution, row: "+to_string(row)+", col: "+to_string(col)+";Charge [e^{-}]").c_str(), m_nSteps-1, m_chargeMin, m_chargeMax);
          m_sCurve[fe->GetName()][bitStep][row][col] = new TGraph();
          m_sCurve[fe->GetName()][bitStep][row][col]->SetNameTitle(("sCurve_"+to_string(bitStep)+"_"+to_string(row)+"_"+to_string(col)+"_"+fe->GetName()).c_str(),("S curve, row: "+to_string(row)+", col: "+to_string(col)).c_str());
        }
      }
    }

    m_logFile << "\"m_mask_step\": " << m_mask_step << endl;
    m_logFile << "\"m_cc_max\": " << m_cc_max << endl;
    m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
    m_logFile << "\"m_chargeMin\": " << m_chargeMin << endl;
    m_logFile << "\"m_chargeMax\": " << m_chargeMax << endl;
    m_logFile << "\"m_chargeStep\": " << m_chargeStep << endl;
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": added front-end with chip ID " << fe->GetChipID() << std::endl;
  }


  std::cout<<"Setting up mask for: "<<m_colMin<<" - "<<m_colMax<<std::endl;
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax,192);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency((m_colMax-m_colMin)/4);
  GetMasker()->Build();
}

void PixelThresholdTune::Run(){
  cout << __PRETTY_FUNCTION__ << "Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // The Analog scans requirs many mo
  for(uint32_t iStepBit = 0; iStepBit < 2; iStepBit++){
    uint32_t adjBit = 15 * iStepBit;
    //Setting the threshold adjustment DAC
    std::cout << __PRETTY_FUNCTION__ << ": threshold adjustment value: = " << adjBit << std::endl;
    for(auto fe : GetFEs()){
      for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
	for(uint32_t col = m_colMin; col < m_colMax; col++){
	  fe->SetPixelThreshold(col, row, adjBit);
	}
      }
      fe->WriteGlobal();
      Send(fe);
    }

    for(uint32_t iStep = 0; iStep < m_nSteps; iStep++){

      uint32_t charge = m_chargeMin + iStep * m_chargeStep;
      m_chargeVec.push_back(charge);
      double vcalDiff = Tools::injToVcal(charge);
      uint32_t vcalHigh = round(m_vcalMed + vcalDiff);

      std::cout << __PRETTY_FUNCTION__ << ": injecting charge Q = " << charge << " e-" << std::endl;

      //setting VCAL
      for(auto fe : GetFEs()){
	fe->GetConfig()->SetField(Configuration::VCAL_HIGH,vcalHigh);
	fe->WriteGlobal();
	Send(fe);
	if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": written inj charge " << charge << " for chip ID " << fe->GetChipID() << std::endl;
      }



      for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
      
	//cout << "AnalogScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
	GetMasker()->GetStep(st);
	ConfigMask();


	uint32_t nhits2=0;

	//Trigger and read-out
	for(uint32_t trig=0;trig<m_ntrigs; trig++){

	  Trigger();

	  auto start = chrono::steady_clock::now();

	  //histogram hit
	  uint32_t nhits=0;
	  while(true){
          
	    for(auto fe : GetFEs()){
	      if(!fe->HasHits()) continue;
	      Hit* hit = fe->GetHit();
	      if(hit!=0 && hit->GetTOT()!=0x0 ){
		m_occ[fe->GetName()][iStep]->Fill(hit->GetCol(),hit->GetRow());
		nhits++;
	      }
	      fe->NextHit();
	    }
	    auto end = chrono::steady_clock::now();
	    uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	    if(ms>100){break;}
	  }
	  nhits2+=nhits;
	}
	cout << __PRETTY_FUNCTION__ << "NHits in step: " << nhits2 << endl;
	UnconfigMask();
      }
    }

    // Results

    for(auto fe : GetFEs()){
      for(uint32_t iStep = 0; iStep < m_nSteps; iStep++){
	m_occ[fe->GetName()][iStep]->Write();
      }
      for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){                                                                                          
	for(uint32_t col = m_colMin; col < m_colMax; col++){
	  for(uint32_t iStep = 0; iStep < m_nSteps; iStep++){
	    uint32_t hits = m_occ[fe->GetName()][iStep]->GetBinContent(col+1,row+1);
	    double occupancy = 100 * hits/m_ntrigs;   
	    m_sCurve[fe->GetName()][iStepBit][row][col]->SetPoint(iStep,m_chargeVec[iStep],occupancy);
	    if(iStep>=1){
	      m_histoGaus[fe->GetName()][iStepBit][row][col]->SetBinContent(iStep,(m_sCurve[fe->GetName()][iStepBit][row][col]->GetPointY(iStep)-m_sCurve[fe->GetName()][iStepBit][row][col]->GetPointY(iStep-1)));
	    }
	  }
	  float meanThHisto = m_histoGaus[fe->GetName()][iStepBit][row][col]->GetMean();
	  float sigmaThHisto = m_histoGaus[fe->GetName()][iStepBit][row][col]->GetStdDev();
	  m_meanThresholdHisto[fe->GetName()][iStepBit]->Fill(col,row,meanThHisto);
	  m_sigmaThresholdHisto[fe->GetName()][iStepBit]->Fill(col,row,sigmaThHisto);
	  if(meanThHisto!=0 and sigmaThHisto!=0){
	    TF1 * func=new TF1("scurve","[0]*0.5*(1+erf((x-[1])/([2]*sqrt(2))))",m_chargeVec[0],m_chargeVec[m_nSteps-1]);
	    func->SetParameter(0,m_ntrigs);
	    func->SetParameter(1,meanThHisto);
	    func->SetParameter(2,sigmaThHisto);
	    m_sCurve[fe->GetName()][iStepBit][row][col]->Fit("scurve","Q && R");
	    m_meanThFit = func->GetParameter(1);
	    m_sigmaThFit = func->GetParameter(2);
	    delete func;
	  }
	  else{ 
	    m_meanThFit=0; 
	    m_sigmaThFit=0;
	  }
	  if(iStepBit == 0 and row==191) {
	    cout << "Row: " << row << " col: " << col << " meanThHisto: " << meanThHisto << " sigmaThHisto: " << sigmaThHisto << " meanThFit: " << m_meanThFit << "sigmaThFit: " << m_sigmaThFit << endl;
	    m_sCurve[fe->GetName()][iStepBit][row][col]->Write();
	    m_histoGaus[fe->GetName()][iStepBit][row][col]->Write();
	  }
	  m_meanThresholdFit[fe->GetName()][iStepBit]->Fill(col,row,m_meanThFit);
	  m_sigmaThresholdFit[fe->GetName()][iStepBit]->Fill(col,row,m_sigmaThFit);
	  //cout << "TDAC: " << fe->GetPixelThreshold(col,row) << endl;
	}
      }
    }
  
  }
}

void PixelThresholdTune::Analysis(){

  for(auto fe : GetFEs()){
    for(uint32_t iStepBit = 0; iStepBit < 2; iStepBit++){
      fe->SetGlobalThreshold(Pixel::Sync,m_mem_thres[fe->GetName()][0]);
      fe->SetGlobalThreshold(Pixel::Lin,m_mem_thres[fe->GetName()][1]);
      fe->SetGlobalThreshold(Pixel::Diff,m_mem_thres[fe->GetName()][2]);      
      m_meanThresholdHisto[fe->GetName()][iStepBit]->Write();
      m_meanThresholdFit[fe->GetName()][iStepBit]->Write();
      m_sigmaThresholdHisto[fe->GetName()][iStepBit]->Write();
      m_sigmaThresholdFit[fe->GetName()][iStepBit]->Write();
    }

    uint32_t DAC_targ = 0;
    uint32_t untuned = 0;
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
      for(uint32_t col = m_colMin; col < m_colMax; col++){
	double th0 = m_meanThresholdHisto[fe->GetName()][0]->GetBinContent(col,row);
	double th1 = m_meanThresholdHisto[fe->GetName()][1]->GetBinContent(col,row);
	if(th0 != 0 and th1 != 0 and th0 != th1){
	  DAC_targ = round(15 + 15*(m_thresholdTarget - th1)/(th1 - th0));
	  if(DAC_targ<0) DAC_targ = 0;
	  else if(DAC_targ>15) DAC_targ = 15;
	}
	else {
	  DAC_targ = fe->GetPixelThreshold(col, row);
	  untuned++;
	}
	//cout << "th0: " << th0 << " th1: " << th1 << " DAC_target: " << DAC_targ << endl;
	fe->SetPixelThreshold(col, row, DAC_targ);
	std::cout<<"DAC Tag: "<<DAC_targ<<std::endl;
	fe->WriteGlobal();
	Send(fe);
      }
    }
    for(uint32_t iStepBit = 0; iStepBit < 2; iStepBit++){  
      delete m_occ[fe->GetName()][iStepBit];
      delete m_meanThresholdHisto[fe->GetName()][iStepBit];
      delete m_meanThresholdFit[fe->GetName()][iStepBit];
      delete m_sigmaThresholdHisto[fe->GetName()][iStepBit];
      delete m_sigmaThresholdFit[fe->GetName()][iStepBit];
    }
    delete[] m_occ[fe->GetName()];
    delete[] m_meanThresholdHisto[fe->GetName()];
    delete[] m_meanThresholdFit[fe->GetName()];
    delete[] m_sigmaThresholdHisto[fe->GetName()];
    delete[] m_sigmaThresholdFit[fe->GetName()];
  }
}

