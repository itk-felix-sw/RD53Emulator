#include "RD53Emulator/Decoder.h"
#include "RD53Emulator/Encoder.h"
#include "RD53Emulator/Emulator.h"
#include <cmdl/cmdargs.h>
#include <netio/netio.hpp>
#include <map>
#include <chrono>
#include <signal.h>
#include <mutex>

namespace felix::base{
  struct FromFELIXHeader {
    FromFELIXHeader(uint16_t len, uint32_t elink) :
      length(len+8), status(0), elinkid(elink){};
    uint16_t length; uint16_t status; uint32_t elinkid;
  };
  struct ToFELIXHeader {uint32_t length; uint32_t reserved; uint64_t elinkid; };
  struct L1AMsg {
    L1AMsg(uint32_t elink, uint32_t l1):length(sizeof(L1AMsg)),elinkid(elink),bcid(0),l1id(l1){};
    uint16_t length; uint16_t status; uint32_t elinkid;
    uint16_t fmt:16; uint8_t len:4; uint16_t bcid:12; uint32_t l1id;
    uint32_t orbit; uint16_t ttype; uint16_t res:16; uint32_t l0id;
  };
}

using namespace std;
using namespace RD53A;

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){
  
  cout << "#####################################" << endl
       << "# Welcome to felix_emulator_rd53a   #" << endl
       << "#####################################" << endl;
  
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgInt  cCmdPort(  'c',"cmdport","number","Command port number. Default 12340");
  CmdArgInt  cDataPort( 'd',"dataport","number","Data port number. Default 12350");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgInt  cMaxRate(  'm',"max-rate","rate","max rate in Hz. Default 5 Hz");
  CmdArgInt  cOutMode(  'o',"mode","number","0=all, 1=only data, 2=only service. Default 0");
  CmdArgBool cRdmTh(    'r',"rdm-threshold","enables matrix init with random thresholds (Gaussian)");
  CmdArgBool cPixNoise( 'n',"pixel-noise","enables pixel electronic noise (Gaussian)");
  
  CmdLine cmdl(*argv,&cVerbose,&cCmdPort,&cDataPort,&cBackend,&cMaxRate,&cOutMode,&cRdmTh,&cPixNoise,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  uint32_t port_cmd=(cCmdPort.flags()&CmdArg::GIVEN?cCmdPort:12340);
  uint32_t port_data=(cDataPort.flags()&CmdArg::GIVEN?cDataPort:12350);
  uint32_t max_rate=(cMaxRate.flags()&CmdArg::GIVEN?cMaxRate:5);
  uint32_t milis=1000./(float)max_rate;
  uint32_t out_mode=(cOutMode.flags()&CmdArg::GIVEN?cOutMode:0);

  netio::context ctx(backend.c_str());
  netio::sockcfg cfg = netio::sockcfg::cfg();
  cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, 16);
  cfg(netio::sockcfg::PAGESIZE, 2000);
 
  map<netio::tag,netio::endpoint> elinks;
  map<netio::tag,Emulator*> frontends;
  mutex lock;
  
  cout << "Felix emulator: Create a publish socket in port: " << port_data << endl;
  netio::publish_socket * data_socket=new netio::publish_socket(&ctx, port_data, cfg);
  
  data_socket->register_subscribe_callback([&](netio::tag t, netio::endpoint ep){
      cout << "Felix emulator: Subscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
      lock.lock();
      elinks[t]=ep;
      frontends[t]=new Emulator(0,out_mode);
      frontends[t]->SetVerbose(cVerbose);  
      frontends[t]->SetRandomThresholds(cRdmTh);  
      frontends[t]->SetPixelNoise(cPixNoise);  
      lock.unlock();
    });

  data_socket->register_unsubscribe_callback([&](netio::tag t, netio::endpoint ep){
      cout << "Felix emulator: Unsubscription from endpoint " << ep.address() << ":" << ep.port() << " for elink " << t << endl;
      lock.lock();
      elinks.erase(t);
      delete frontends[t];
      frontends.erase(t);
      lock.unlock();
    });

  cout << "Felix emulator: Create a receiver socket in port: " << port_cmd << endl;
  netio::low_latency_recv_socket cmd_socket(&ctx, port_cmd, [&](netio::endpoint &ep, netio::message &msg){
      if(cVerbose) cout << "Felix emulator: New message received" << endl;
      vector<uint8_t> cmd = msg.data_copy();
      felix::base::ToFELIXHeader hdr; 
      memcpy(&hdr,(const void*)&cmd[0], sizeof(hdr));
      uint32_t pos=sizeof(hdr);
      if(frontends.count(hdr.elinkid)==0){return;}
      if(cVerbose) cout << "Felix emulator: Handle command" << endl;
      frontends[hdr.elinkid]->HandleCommand(&cmd[pos],cmd.size()-pos);
    });
  
  g_cont=true;
  signal(SIGINT,handler);
  signal(SIGTERM,handler);

  //Context for netio
  cout << "Felix emulator: Create the context for netio" << endl;
  std::thread t_context([&ctx](){ctx.event_loop()->run_forever();});

  //Data sender
  cout << "Felix emulator: Loop" << endl;
  while(g_cont){
    lock.lock();
    map<netio::tag,Emulator*>::iterator it;
    for(it=frontends.begin();it!=frontends.end();it++){  
      Emulator * emu=it->second;
      //      emu->SetVerbose(1);
      emu->ProcessQueue();
      //cout << "Emu Length : " << emu->GetLength() << endl;
      if(emu->GetLength()==0){continue;}
      if(cVerbose) cout << "Felix emulator: Send data size " << emu->GetLength() << endl;
      felix::base::FromFELIXHeader hdr(emu->GetLength(),it->first);
      netio::message msg;      
      msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
      //GetBytes(reversed)
      msg.add_fragment(emu->GetBytes(true), emu->GetLength());      
      data_socket->publish(it->first,msg); 
    }
    lock.unlock();
    if(cVerbose) cout << "Felix emulator: Sleep for: " << milis << " ms" << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(milis));
  }
  
  cout << "Felix emulator: Clean the house" << endl;
  //Cannot delete the data socket until netio is not fixed
  //cout << __PRETTY_FUNCTION__ << "delete data socket" << endl;
  //delete data_socket;
  cout << "Felix emulator: Stop event loop" << endl;
  ctx.event_loop()->stop();
  cout << "Felix emulator: Join context" << endl;
  t_context.join();  
  
  cout << "Felix emulator: Delete front-ends" << endl;
  map<netio::tag,Emulator*>::iterator it;
  for(it=frontends.begin();it!=frontends.end();it++){  
    delete it->second;
    frontends.erase(it);
  }
  
  cout << "Felix emulator: Have a nice day" << endl;
  return 0;
}
