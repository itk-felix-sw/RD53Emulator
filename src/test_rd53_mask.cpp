#include <cmdl/cmdargs.h>
#include <iostream>
#include <RD53Emulator/Masker.h>

using namespace std;
using namespace RD53A;

int main(int argc, char *argv[]){

  CmdArgIntList cRange( 'r',"range","pixel","Range for the scan: px0 py0 px1 py1");
  CmdArgIntList cShape( 's',"shape","pixel","Size of the shape in pixels: dx dy");
  CmdArgInt cFreq( 'f',"freq","value","Frequency");
  CmdArgInt cStep( 'S',"step","value","Step. Default all");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");

  cFreq = 1;
  
  CmdLine cmdl(*argv,&cVerbose,&cRange,&cShape,&cFreq,&cStep,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  if(cRange.count()<4 or cRange.count()>4){
    cout << "Not enough parameters" << endl;
    return 0;
  }
  if(cRange[0]>400 or cRange[1]>192 or cRange[2]>400 or cRange[3]>192){
    cout << "Out of range" << endl;
    return 0;    
  }
  cout << "Create Masker" << endl;
  Masker * mask = new Masker();
  mask->SetVerbose(cVerbose);

  cout << "Range: " << cRange[0] << "," << cRange[1] << "," << cRange[2] << "," << cRange[3] << endl;
  cout << "Shape: " << cShape[0] << "," << cShape[1] << endl;
  cout << "Freq: " << cFreq << endl;

  mask->SetRange(cRange[0],cRange[1],cRange[2],cRange[3]);
  mask->SetShape(cShape[0],cShape[1]);
  mask->SetFrequency(cFreq);

  cout << "Build the mask" << endl;
  mask->Build();

  cout << "Calculated number of steps: " << mask->GetNumSteps() << endl;
  for(uint32_t i=0;i<mask->GetNumSteps();i++){
    if(cStep.flags()&&CmdArg::GIVEN){
      if(i!=(uint32_t)cStep){continue;}
    }
    cout << "Step: " << i << endl;
    mask->GetStep(i);
    
    for(auto cc: mask->GetCoreColumns()){
      cout << "CC: " << cc << endl;
    }
    for(auto dc: mask->GetDoubleColumns()){
      cout << "DC: " << dc << endl;
    }
    for(auto pp: mask->GetPixels()){
      cout << "PP: " << pp.first << "," << pp.second << endl;
    }
  }

  cout << "Cleanup the house" << endl;
  delete mask;

  cout << "Have a nice day" << endl;
  return 0;
}
