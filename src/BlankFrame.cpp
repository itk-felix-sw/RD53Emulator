#include "RD53Emulator/BlankFrame.h"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53A;

BlankFrame::BlankFrame(){
  m_aheader = 0x2;
  m_data[0] = 0x00;
  m_data[1] = 0x00;
  m_data[2] = 0x00;
  m_data[3] = 0x00;
  m_data[4] = 0x00;
  m_data[5] = 0x00;
  m_data[6] = 0x00;
  m_data[7] = 0x00;
}

BlankFrame::~BlankFrame(){}

string BlankFrame::ToString(){
  ostringstream os;
  os << "Blank " << hex;
  for(uint32_t i=0;i<8;i++){
    os << m_data[i];
  }
  os << dec;
  return os.str();
}

uint32_t BlankFrame::UnPack(uint8_t * bytes, uint32_t maxlen){
  for(uint32_t i=0;i<8;i++){
    if(bytes[i]!=m_data[i]){//cout << "Blank::SetBytes Decoding error" << endl;
      return 0;}
  }
  return 8; 
}

uint32_t BlankFrame::Pack(uint8_t * bytes){
  for(uint32_t i=0;i<8;i++){
    bytes[i]=m_data[i];
  }
  return 8;
}

uint32_t BlankFrame::GetType(){
  return Frame::BLANK;
}
