#include "RD53Emulator/ThresholdScan.h"
#include "RD53Emulator/Tools.h"
#include "RD53Emulator/SCurveFitter.h"

#include <iostream>
#include <sstream>
#include <chrono>

#include "TCanvas.h"
#include "TROOT.h"

using namespace std;
using namespace RD53A;

ThresholdScan::ThresholdScan(){
  m_ntrigs = 100; //100
  m_TriggerLatency = 48; //52

  m_vcalMed = 1000;
  
  m_vcalDiffStart= 0 ;
  m_vcalDiffEnd  = 400. ; //used to be 600
  m_vcalStep     = 10.; //used to be 5

  m_chargeMin = Tools::injToCharge(m_vcalDiffStart);
  m_chargeMax = Tools::injToCharge(m_vcalDiffEnd);
  m_chargeStep= Tools::injToCharge(m_vcalStep);
  m_nSteps = (m_vcalDiffEnd - m_vcalDiffStart) / m_vcalStep;
}

ThresholdScan::~ThresholdScan(){}

void ThresholdScan::PreRun(){


  for(auto fe : GetFEs()){

    fe->BackupConfig();
    

    if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){
      cout << "ThresholdScan: Configure Sync+Lin+Diff for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      GetMasker()->SetRange(Matrix::SYN_COL0,0,Matrix::DIF_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::SYN_COL0)/2);
    }
    else if((m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){ //syn
      cout << "ThresholdScan: Configure Sync for " << fe->GetName() << endl; 
      m_vcalDiffStart=000 ;
      m_vcalDiffEnd  =400 ;
      m_vcalStep     = 10;
      //Autozeroing 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      //Injection charge
      fe->SetGlobalThreshold(Pixel::Lin,Tools::chargeToThr(500,Pixel::Lin));
      fe->SetGlobalThreshold(Pixel::Diff,Tools::chargeToThr(500,Pixel::Diff));
      //Disable LIN & DIFF
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1 ,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2 ,0);
      //Masker
      GetMasker()->SetRange(Matrix::SYN_COL0,0,Matrix::SYN_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::SYN_COLN+1-Matrix::SYN_COL0)/8);
    }
    else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){ //lin
      cout << "ThresholdScan: Configure Lin for " << fe->GetName() << endl;
      m_vcalDiffStart=000 ;
      m_vcalDiffEnd  =400 ;
      m_vcalStep     = 10.;
      //Reset
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      //Injection charge
      fe->SetGlobalThreshold(Pixel::Sync,Tools::chargeToThr(500,Pixel::Sync));
      fe->SetGlobalThreshold(Pixel::Diff,Tools::chargeToThr(500,Pixel::Diff));
      //Disable SYN & DIFF
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
      //Masker
      GetMasker()->SetRange(Matrix::LIN_COL0,0,Matrix::LIN_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::LIN_COLN+1-Matrix::LIN_COL0)/4);

    }
    else if(!(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){ //diff
      cout << "ThresholdScan: Configure Diff for " << fe->GetName() << endl;
      m_vcalDiffStart=000 ;
      m_vcalDiffEnd  =500 ;
      m_vcalStep     = 10.;
      //Reset
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      //Injection charge
      fe->SetGlobalThreshold(Pixel::Sync,Tools::chargeToThr(500,Pixel::Sync));
      fe->SetGlobalThreshold(Pixel::Lin,Tools::chargeToThr(500,Pixel::Lin));
      //Masker
      GetMasker()->SetRange(Matrix::DIF_COL0,0,Matrix::DIF_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::DIF_COL0)/4);//This needs to be 4 or our Mask Shape of 8,2 doesn't work for differential. Blame the chip
     //Disable SYN & LIN
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC ,0);

    }
    else{
      cout << "ThresholdScan: Configure Lin+Diff for " << fe->GetName() << endl;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,Tools::chargeToThr(500,Pixel::Sync));
      GetMasker()->SetRange(Matrix::LIN_COL0,0,Matrix::DIF_COLN+1,Matrix::NUM_ROWS);
      GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::LIN_COL0)/4);//This needs to be 4 or our Mask Shape of 8,2 doesn't work for differential. Blame the chip
    }

    //Configure the options for an threshold scan
    cout << "ThresholdScan: Configure analog injection for " << fe->GetName() << endl;
    
    m_nSteps = (m_vcalDiffEnd - m_vcalDiffStart) / m_vcalStep;
    m_chargeMin = Tools::injToCharge(m_vcalDiffStart);
    m_chargeMax = Tools::injToCharge(m_vcalDiffEnd);
    m_chargeStep= Tools::injToCharge(m_vcalStep);

    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::INJ_MODE_ANA,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalMed+m_vcalDiffStart);
    
       
    fe->WriteGlobal();
    Send(fe);

    cout << "ThresholdScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);


    //Create the histograms for the threshold scan of each pixel
    cout << "ThresholdScan: Create histograms per pixel for " << fe->GetName() << endl;
    
    m_scur[fe->GetName()]=new TH1I( ("scur_"+fe->GetName()).c_str(),
				    ("scur_"+fe->GetName()+";#DeltaVcal;FE occupancy").c_str(),
				    m_nSteps,m_vcalDiffStart,m_vcalDiffEnd);

    m_scur2D[fe->GetName()]=new TH2F( ("scur2D_"+fe->GetName()).c_str(),
    				      ("scur2D_"+fe->GetName()+"#DeltaVcal;pixel occupancy").c_str(),
    				      m_nSteps,m_vcalDiffStart,m_vcalDiffEnd,
    				      m_ntrigs+1,-0.5,m_ntrigs+0.5);

    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
        
	m_curves[fe->GetName()][col][row]= new TH1I(("scurve_"+fe->GetName()+std::to_string(col)+"_"+std::to_string(row)).c_str(),
						   ("scurve_"+fe->GetName()+std::to_string(col)+"_"+std::to_string(row)).c_str(),
									  m_nSteps,m_vcalDiffStart,m_vcalDiffEnd);
      }
    }
    m_occ[fe->GetName()]=new TH2I*[m_nSteps];
    for(uint32_t iStep=0; iStep<m_nSteps; iStep++){
      stringstream iStep_ss;
      iStep_ss << iStep;
      stringstream charge_ss;
      //charge_ss << (m_chargeMin + iStep * m_vcalStep);
      charge_ss << (iStep * m_vcalStep);
      m_occ[fe->GetName()][iStep] = new TH2I(("occ_"+fe->GetName()+"_"+iStep_ss.str()).c_str(),
					     (fe->GetName()+", #Delta VCAL="+charge_ss.str()+";Column;Row").c_str(),
					     Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);

    }
    
    //Store hits
    m_hits[fe->GetName()]= new HitTree("tree_"+fe->GetName(),"HitTree");

  }

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_chargeMin\": " << m_chargeMin << endl;
  m_logFile << "\"m_chargeMax\": " << m_chargeMax << endl;
  m_logFile << "\"m_vcalStep\": " << m_chargeStep << endl;
  m_logFile << "\"m_nSteps\": " << m_nSteps << endl;


}

void ThresholdScan::Run(){

  //Prepare the trigger
  cout << "ThresholdScan: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // The Analog scans requirs many more triggers due to timming issues

  cout << "ThresholdScan: Prepare the mask" << endl;
  GetMasker()->SetShape(8,2);
  GetMasker()->Build();
  bool cutBCID=( m_scan_fe & SyncFE ); 


  cout << "ThresholdScan: Loop over " << m_nSteps << " vcal steps" << endl;
  for(uint32_t iStep=0; iStep<m_nSteps; iStep++){

    unsigned vcalDiff = m_vcalDiffStart + iStep * m_vcalStep;
    unsigned vcalHigh = m_vcalMed + vcalDiff;

    double charge = Tools::injToCharge(m_vcalDiffStart + iStep * m_vcalStep);
     

    cout << "ThresholdScan: injecting charge Q=" << charge << "e-" << " vcalMed "<<m_vcalMed<<" vcalHigh "<<vcalHigh<< " DeltaVCal: " << m_vcalDiffStart + iStep * m_vcalStep  << endl;

    // setting VCAL
    for(auto fe : GetFEs()){
      fe->GetConfig()->SetField(Configuration::VCAL_HIGH,vcalHigh);
      fe->WriteGlobal();
      Send(fe);
      if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": written inj charge " << charge << " for chip ID " << fe->GetChipID() << std::endl;
      if(m_storehits) m_hits[fe->GetName()]->setVcal(vcalDiff);
    }

    for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
      //cout << "ThresholdScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
      GetMasker()->GetStep(st);
    
      //Do the mask step
      ConfigMask();
      
      uint32_t expected=GetMasker()->GetPixels().size();

      //Trigger 
      for(uint32_t trig=0;trig<m_ntrigs; trig++){
	Trigger();
      }
      auto start = chrono::steady_clock::now();
	
      //Readout the hit histogram hit
      bool LastLoop=false;
      uint32_t nhits=0;
      uint32_t nRhits=0;
      uint32_t nGoodHists=0;

      while(true){
	for(auto fe : GetFEs()){
	  if(!fe->HasHits()) continue;
	  LastLoop=false;
	  start = chrono::steady_clock::now();
	  Hit* hit = fe->GetHit();
	  if(hit!=0){
	    if(hit->GetTOT()!=0x0 and (cutBCID? hit->GetBCID()==0:1)){
	      //cout << " Good hit: " << fe->GetName() << ": " << hit->ToString() << endl;
	      if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
		cout << "Hit is not expected in the current mask step: " << hit->ToString() << endl;
	      } else {
		nGoodHists++;
		//if( !fe->GetPixelMask(hit->GetCol(),hit->GetRow())){
		m_occ[fe->GetName()][iStep]->Fill(hit->GetCol(),hit->GetRow());
		//m_scur[fe->GetName()]->Fill(vcalDiff);
		m_curves[fe->GetName()][hit->GetCol()][hit->GetRow()]->Fill(vcalDiff);
		//Store the hits!
		if(m_storehits) m_hits[fe->GetName()]->Fill(hit);
		nRhits++;
	      }
	      nhits++;
	    }	      
	  }
	  fe->NextHit();
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if(LastLoop) break;
	if(ms>50 or nGoodHists>=m_ntrigs*expected*GetFEs().size() ){LastLoop=true;}
      }
      cout << "ThresholdScan: NHeaders in step: " << nhits <<" / "<<m_ntrigs*GetMasker()->GetPixels().size()<<" NHits: "<<nRhits<<" / "<<m_ntrigs*GetMasker()->GetPixels().size()<< endl;
	
      //Undo the mask step
      UnconfigMask();
    }

  }
}

void ThresholdScan::Analysis(){

  gROOT->SetBatch();
  TCanvas* can=new TCanvas("plot","plot",800,600);
  cout << "Analysis" << endl;
  
  time_t start_time;
  time(&start_time);
  m_logFile << "\"start time analyis\": " << ctime(&start_time) << endl;

  map<string,TH2F*> thres;
  map<string,TH2F*> noise;
  map<string,TH1F*> thres_1d;
  map<string,TH1F*> noise_1d;
  map<string,TH2F*> thres_Q;
  map<string,TH2F*> noise_Q;
  map<string,TH1F*> thres_Q_1d;
  map<string,TH1F*> noise_Q_1d;


  map<string,TH2I*> occ_vs_vcal;
  for(auto fe : GetFEs()){
    fe->RestoreBackupConfig();

    thres[fe->GetName()]=new TH2F(("thres_Vcal_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    noise[fe->GetName()]=new TH2F(("noise_Vcal_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    thres_1d[fe->GetName()]=new TH1F(("thres_Vcal_1d_"+fe->GetName()).c_str(),";VCalDiff",m_nSteps*2,m_vcalDiffStart,m_vcalDiffEnd);
    noise_1d[fe->GetName()]=new TH1F(("noise_Vcal_1d_"+fe->GetName()).c_str(),";VcalDiff",40,0,30);

    thres_Q[fe->GetName()]=new TH2F(("thres_Q_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    noise_Q[fe->GetName()]=new TH2F(("noise_Q_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    thres_Q_1d[fe->GetName()]=new TH1F(("thres_Q_1d_"+fe->GetName()).c_str(),";Charge",m_nSteps*2, m_chargeMin, m_chargeMax);
    //noise_Q_1d[fe->GetName()]=new TH1F(("noise_Q_1d_"+fe->GetName()).c_str(),";Charge",30,50,400);
    noise_Q_1d[fe->GetName()]=new TH1F(("noise_Q_1d_"+fe->GetName()).c_str(),";Charge",40, 0, Tools::injToCharge(30));
  }

  //fitting of s-curves
  Fitter::SCurveFitter scf;

  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
	if (m_curves[fe->GetName()][col][row]->GetEntries()<1){continue;}

	scf.setDataFromHist(m_curves[fe->GetName()][col][row]);
	scf.guessInitialParameters();
	scf.fit();

        float mean = scf.getSCurve().mu;
        float sigma = scf.getSCurve().sigma;

        float mean_Q=Tools::injToCharge(mean);
	float sigma_Q=Tools::injToCharge(mean+sigma) - Tools::injToCharge(mean); //injToCharge is a non-linear transformation

        thres[fe->GetName()]->Fill(col,row,mean);
        noise[fe->GetName()]->Fill(col,row,sigma);
        thres_1d[fe->GetName()]->Fill(mean);
        noise_1d[fe->GetName()]->Fill(sigma);

        thres_Q[fe->GetName()]->Fill(col,row,mean_Q);
        noise_Q[fe->GetName()]->Fill(col,row,sigma_Q);
        thres_Q_1d[fe->GetName()]->Fill(mean_Q);
        noise_Q_1d[fe->GetName()]->Fill(sigma_Q);
	
	for (int binV=0; binV<=m_curves[fe->GetName()][col][row]->GetNbinsX()+1; binV++) {
	  m_scur2D[fe->GetName()]->Fill( m_curves[fe->GetName()][col][row]->GetXaxis()->GetBinCenter(binV), 
					 m_curves[fe->GetName()][col][row]->GetBinContent(binV) );
	}
	
        if (((col*Matrix::NUM_ROWS)+row)%3200==0){
          m_curves[fe->GetName()][col][row]->Write();
        }
      }
    }

    time_t stop_time;
    time(&stop_time);
    m_logFile << "\"stop time analyis\": " << ctime(&stop_time) << endl;

    thres[fe->GetName()]->Write();
    noise[fe->GetName()]->Write();
    thres_1d[fe->GetName()]->Write();
    noise_1d[fe->GetName()]->Write();
    thres_Q[fe->GetName()]->Write();
    noise_Q[fe->GetName()]->Write();
    thres_Q_1d[fe->GetName()]->Write();
    noise_Q_1d[fe->GetName()]->Write();

    m_scur[fe->GetName()]->Write();
    m_scur2D[fe->GetName()]->Write();

    //Write the hits
    if(m_storehits) m_hits[fe->GetName()]->Write();

    thres_Q_1d[fe->GetName()]->Draw("HIST");
    //can->SetLogy(false);
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Thres_1D.pdf").c_str() );

    noise_Q_1d[fe->GetName()]->Draw("HIST");
    //can->SetLogy(false);
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Noise_1D.pdf").c_str() );

    m_scur2D[fe->GetName()]->Draw("COLZ");
    can->SetLogz();
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Scurve_2D.pdf").c_str() );

  }


  for(auto fe : GetFEs()){
    for(uint32_t iStep=0; iStep<m_nSteps; iStep++){
      m_occ[fe->GetName()][iStep]->Write();
      delete m_occ[fe->GetName()][iStep];
    }
    delete[] m_occ[fe->GetName()];
    delete m_scur[fe->GetName()];
    delete m_scur2D[fe->GetName()];

    delete thres[fe->GetName()];
    delete noise[fe->GetName()];
    delete thres_1d[fe->GetName()];
    delete noise_1d[fe->GetName()];
    delete thres_Q[fe->GetName()];
    delete noise_Q[fe->GetName()];
    delete thres_Q_1d[fe->GetName()];
    delete noise_Q_1d[fe->GetName()];

    // if this ever causes problems, write brian.moser@cern.ch

    // for( auto i : m_curves[fe->GetName()] ){
    //   for(auto j : i.second){
    // 	delete j.second;
    //   }
    // }

  }
  
}
