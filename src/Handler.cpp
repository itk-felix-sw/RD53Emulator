#include "RD53Emulator/Handler.h"
#include "RD53Emulator/RunNumber.h"
#include "RD53Emulator/Tools.h"
#include "netio/netio.hpp"
#include <json.hpp>
#include <iostream>
#include <fstream>
#include <mutex>
#include <chrono>
#include <sstream>
#include <iomanip>
#include "TFile.h"
#include "TROOT.h"

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;
using namespace std;
using namespace RD53A;

struct FelixDataHeader{
  uint16_t length;
  uint16_t status;
  uint32_t elink;
};

struct FelixCmdHeader{
  uint32_t length;
  uint32_t reserved;
  uint64_t elink;
};

Handler::Handler(){
  //gROOT->SetBatch(true); //This is a must otherwise runs hang
  string itkpath = getenv("ITK_PATH");
  m_config_path.push_back("./tuned/");
  m_config_path.push_back("");
  m_config_path.push_back("./");
  m_config_path.push_back(itkpath+"/");
  m_config_path.push_back(itkpath+"/tuned/");
  m_config_path.push_back(itkpath+"/config/");
  m_config_path.push_back(itkpath+"/installed/share/data/config/");



  m_verbose = false;
  m_exit    = false;
  m_backend = "posix";
  m_retune = false;
  m_rootfile = 0;
  m_outpath = (getenv("ITK_DATA_PATH")?string(getenv("ITK_DATA_PATH")):".");
  m_rn = new RunNumber();
  m_output = true;
  m_fullOutPath = "";
  m_masker = new Masker();
  m_scan_fe = SyncFE | LinFE | DiffFE;
  m_storehits = false;
}

Handler::~Handler(){
  while(!m_fes.empty()){
    FrontEnd* fe=m_fes.back();
    m_fes.pop_back();
    delete fe;
  }
  m_fes.clear();
  if(m_rootfile) delete m_rootfile;
  delete m_rn;
  delete m_masker;
}

void Handler::SetVerbose(bool enable){
  m_verbose=enable;
  for(auto fe: m_fes){
    fe->SetVerbose(enable);
  }
}

void Handler::SetContext(string context){
  m_backend = context;
}

void Handler::SetInterface(string interface){
  m_interface = interface;
}

void Handler::SetRetune(bool enable){
  m_retune=enable;
}

void Handler::SetEnableOutput(bool enable){
  m_output=enable;
}

bool Handler::GetEnableOutput(){
  return m_output;
}

void Handler::SetCharge(uint32_t charge){
  m_charge=charge;
}

uint32_t Handler::GetCharge(){
  return m_charge;
}

void Handler::SetMaskOpt(uint32_t maskopt){
  m_maskopt=maskopt;
 }

uint32_t Handler::GetMaskOpt(){
  return m_maskopt;
}

uint32_t Handler::GetToT(){
  return m_ToT;
}

void Handler::SetToT(uint32_t ToT){
  m_ToT=ToT;
}

void Handler::SetScan(string scan){
  m_scan=scan;
}

void Handler::SetScanFE(string scan_fe){
  m_scan_fe=0;
  if(scan_fe.find("syn")!=string::npos){m_scan_fe|=FrontEndTypes::SyncFE;}
  if(scan_fe.find("lin")!=string::npos){m_scan_fe|=FrontEndTypes::LinFE;}
  if(scan_fe.find("dif")!=string::npos){m_scan_fe|=FrontEndTypes::DiffFE;}
  if(scan_fe.find("all")!=string::npos){m_scan_fe|=SyncFE|LinFE|DiffFE;}
}

void Handler::StoreHits(bool enable){
  m_storehits = enable;
}

uint32_t Handler::GetThreshold(){
  return m_threshold;
}

void Handler::SetThreshold(uint32_t threshold){
  m_threshold=threshold;
}

void Handler::SetLatency(uint32_t latency){
  m_latency = latency;
}

uint32_t Handler::GetLatency(){
  return m_latency;
}

void Handler::SetOutPath(string path){
  m_outpath=path;
}

std::string Handler::GetOutPath() {
  return m_outpath;
}

std::string Handler::GetFullOutPath(){
  return m_fullOutPath;
}

void Handler::SetCommandLine(string commandLine){
  m_commandLine = commandLine;
}

void Handler::InitRun(){
  cout << "Handler::InitRun" << endl;

  //Start a new run
  cout << "Starting run number: " << m_rn->GetNextRunNumberString(6,true) << endl;

  //Create the output directory
  if(m_verbose) cout << "Handler::InitRun Creating output directory (if doesn't exist yet)" << endl;
  ostringstream cmd;
  
  string tmpPath= m_outpath + "/" + m_rn->GetRunNumberString(6) + "_" + m_scan;
  if (       (m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE) ) tmpPath+="Syn";
  else if ( !(m_scan_fe & SyncFE) and  (m_scan_fe & LinFE) and  (m_scan_fe & DiffFE) ) tmpPath+="LinDiff";
  else if ( !(m_scan_fe & SyncFE) and  (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE) ) tmpPath+="Lin";
  else if ( !(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and  (m_scan_fe & DiffFE) ) tmpPath+="Diff";

  cmd << "mkdir -p " << tmpPath; //m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan;
  system(cmd.str().c_str());
  m_fullOutPath = tmpPath;       //m_outpath + "/" + m_rn->GetRunNumberString(6) + "_" + m_scan;

  //Open the output log file
  ostringstream logFilePath;
  logFilePath << tmpPath << "/" << "logFile.txt";
  //logFilePath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan << "/" << "logFile.txt";
  
  m_logFile.open(logFilePath.str().c_str());
  if(m_verbose) cout << "Handler::InitRun Created log file " << logFilePath.str().c_str() << endl;
  m_logFile << "\"exec\": \"" << m_commandLine << "\"" << endl;
  m_logFile << "\"runNumber\": " << m_rn->GetRunNumberString(6) << endl;
  time_t startTime;
  time(&startTime);
  m_logFile << "\"startTime\": " << ctime(&startTime) << endl;

  //Copy the connectivity file to the log!!!! FIXME
  std::string line;
  std::ifstream connectivityFile(m_connectivityPath);
  while(std::getline(connectivityFile, line)){
    m_logFile << line << endl;
  }
  connectivityFile.close();

  //give up if somebody disabled the output
  if(!m_output) return;

  //Open the output ROOT file
  ostringstream opath;
  opath << tmpPath << "/" << "output.root";
  //opath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan << "/" << "output.root";
  if(m_verbose) cout << "Handler::InitRun Creating root file " << opath.str().c_str() << endl;
  m_rootfile=TFile::Open(opath.str().c_str(),"RECREATE");
  if(m_verbose) cout << "Handler::InitRun Created root file " << opath.str().c_str() << endl;

}

void Handler::SetMapping(string mapping, bool auto_load){

  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Handler::SetMapping" << " Testing: " << sdir << mapping << endl;
   m_connectivityPath = sdir+mapping;
   std::ifstream fr(m_connectivityPath);
   if(!fr){continue;}
   cout << "Handler::SetMapping" << " Loading: " << m_connectivityPath << endl;
   json config;
   fr >> config;
   for(auto node : config["connectivity"]){
     AddMapping(node["name"],node["config"],node["tx"],node["rx"],node["host"],node["cmd_port"],node["host"],node["data_port"]);
     if(auto_load and node["enable"]!=0){
       cout << "Handler::SetMapping Adding front end " << node["name"] << endl;
       AddFE(node["name"]);
       if(!node["dcs_node"].is_null()){GetFE(node["name"])->SetDcsNode(node["dcs_node"]);}
     }else if(auto_load){
       cout << "Handler::SetMapping Front end is disabled: " << node["name"] << endl;
     }
   }
   fr.close();
   break;
  }

}

FrontEnd* Handler::GetFE(string name){
  return m_fe[name];
}

uint32_t Handler::GetFEDataElink(string name){
  return m_fe_rx[name];
}

uint32_t Handler::GetFECmdElink(string name){
  return m_fe_tx[name];
}

vector<FrontEnd*> Handler::GetFEs(){
  return m_fes;
}

bool Handler::GetRetune(){
  return m_retune;
}

void Handler::AddMapping(string name, string config, uint32_t cmd_elink, uint32_t data_elink, string cmd_host, uint32_t cmd_port, string data_host, uint32_t data_port){
  if(m_verbose){
    cout << "Handler::AddMapping Front-End: "
         << "name: " << name << ", "
         << "config: " << config << ", "
         << "cmd_host: " << cmd_host << ", "
         << "cmd_port: " << cmd_port << ", "
         << "cmd_elink: " << cmd_elink << ", "
         << "data_host: " << data_host << ", "
         << "data_port: " << data_port << ", "
         << "data_elink: " << data_elink << " "
         << endl;
  }
  m_fe[name]=0;
  m_enabled[name]=false;
  m_configs[name]=config;
  m_fe_rx[name]=data_elink;
  m_fe_tx[name]=cmd_elink;
  m_data_port[data_elink]=data_port;
  m_data_port[data_elink+1]=data_port; //Service Frame data_port
  m_data_host[data_elink]=data_host;
  m_data_host[data_elink+1]=data_host; //Service Frrame data_host
  m_cmd_host[cmd_elink]=cmd_host;
  m_cmd_port[cmd_elink]=cmd_port;
}

void Handler::AddFE(string name, FrontEnd* fe){

  fe->SetName(name);
  fe->SetActive(true);
  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;
}

void Handler::AddFE(string name, string path){

  if(path==""){path=m_configs[name];}
  if(path==""){path=name+".json";}

  FrontEnd * fe = 0;
  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Handler::AddFE" << " Testing: " << sdir << path << endl;
   std::ifstream fr(sdir+path);
   if(!fr){
     //if(m_verbose) cout << "Handler::AddFE" << " File not found: " << sdir << path << endl;
     continue;
   }
   cout << "Handler::AddFE" << " Loading: " << sdir << path << endl;
   json config;
   fr >> config;
   //Create the front-end
   if(m_verbose) cout << "Handler::AddFE" << " Create front-end" << endl;
   fe = new FrontEnd();
   fe->SetVerbose(m_verbose);
   fe->SetName(name);

   if(m_verbose) cout << "Handler::AddFE" << " Reading configuration file" << endl;
   
   //Actually read the file
   fe->SetChipID(config["RD53A"]["Parameter"]["ChipId"]);
   fe->SetActive(true); 

   if(m_verbose) cout << "Handler::AddFE" << " Reading global registers" << endl;
   fe->SetGlobalConfig(config["RD53A"]["GlobalConfig"]);

   if(m_verbose) cout << "Handler::AddFE" << " Reading pixel bits" << endl;

   for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
     for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
       // EJS 2020-10-27, json.hpp has problems dealing with booleans, had to workaround it like this
       fe->SetPixelMask(col, row,  (config["RD53A"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
       fe->SetPixelEnable(col,row, (config["RD53A"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
       fe->SetPixelHitbus(col,row, (config["RD53A"]["PixelConfig"][col]["Hitbus"][row] == 0?false:true));
       fe->SetPixelInject(col,row, (config["RD53A"]["PixelConfig"][col]["InjEn"][row] == 0?false:true));
       fe->SetPixelThreshold(col,row, config["RD53A"]["PixelConfig"][col]["TDAC"][row]);
     }
   }

   //Force the global registers are compatible with FELIX
   /*
   fe->GetConfig()->SetField(Configuration::CDR_SEL_SER_CLK,0);
   fe->GetConfig()->SetField(Configuration::CML_EN,15);
   fe->GetConfig()->SetField(Configuration::OUT_ACTIVE_LANES,1);
   fe->GetConfig()->SetField(Configuration::SER_SEL_OUT_0,1);
   fe->GetConfig()->SetField(Configuration::SER_SEL_OUT_1,3);
   fe->GetConfig()->SetField(Configuration::SER_SEL_OUT_2,3);
   fe->GetConfig()->SetField(Configuration::SER_SEL_OUT_3,3);
   */

   m_fe[name]=fe;
   m_fes.push_back(fe);
   m_enabled[name]=true;

   if(m_fe_rx.count(name)==0 or m_fe_tx.count(name)==0){
     cout << "Handler::AddFE Configuration error. Connectivity file does not contains FE: " << name << endl;
     m_enabled[name]=false;
   }

   //File was found, no need to keep looking for it
   break;
  }
  if(!fe){cout << "Handler::AddFE Front-end not added. File not found: " << path << endl;}
  else{
    if(m_verbose) cout << "Handler::AddFE File correctly loaded: " << path << endl;
  }
}

void Handler::AddFEbyString(string name, string config_str){
  cout << "Handler::AddFEbyString" << " Json config given as string --> Parsing..."<< endl;
  
  json config;
  std::stringstream(config_str) >> config;

  //Create the front-end
  if(m_verbose) cout << "Handler::AddFE" << " Create front-end" << endl;
  FrontEnd * fe = new FrontEnd();
  fe->SetVerbose(m_verbose);
  fe->SetName(name);

  if(m_verbose) cout << "Handler::AddFE" << " Reading configuration file" << endl;

  //Actually read the file
  fe->SetChipID(config["RD53A"]["Parameter"]["ChipId"]);
  fe->SetActive(true);

  if(m_verbose) cout << "Handler::AddFE" << " Reading global registers" << endl;
  fe->SetGlobalConfig(config["RD53A"]["GlobalConfig"]);

  if(m_verbose) cout << "Handler::AddFE" << " Reading pixel bits" << endl;

  for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      // EJS 2020-10-27, json.hpp has problems dealing with booleans, had to workaround it like this
      fe->SetPixelMask(col, row,  (config["RD53A"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
      fe->SetPixelEnable(col,row, (config["RD53A"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
      fe->SetPixelHitbus(col,row, (config["RD53A"]["PixelConfig"][col]["Hitbus"][row] == 0?false:true));
      fe->SetPixelInject(col,row, (config["RD53A"]["PixelConfig"][col]["InjEn"][row] == 0?false:true));
      fe->SetPixelThreshold(col,row, config["RD53A"]["PixelConfig"][col]["TDAC"][row]);
    }
  }

  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;

  if(m_fe_rx.count(name)==0 or m_fe_tx.count(name)==0){
    cout << "Handler::AddFE Configuration error. Connectivity file does not contains FE: " << name << endl;
    m_enabled[name]=false;
  }
}

void Handler::SaveFE(FrontEnd * fe, string path){

  cout << "Handler::SaveFE " << fe->GetName() << " in: " << path << endl;
  json config;
  config["RD53A"]["name"]=fe->GetName();
  config["RD53A"]["Parameter"]["ChipId"]=fe->GetChipID();
  config["RD53A"]["GlobalConfig"]=fe->GetGlobalConfig();

  for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      config["RD53A"]["PixelConfig"][col]["Enable"][row]=(uint32_t)fe->GetPixelEnable(col,row);
      config["RD53A"]["PixelConfig"][col]["Hitbus"][row]=(uint32_t)fe->GetPixelHitbus(col,row);
      config["RD53A"]["PixelConfig"][col]["InjEn"][row]=(uint32_t)fe->GetPixelInject(col,row);
      config["RD53A"]["PixelConfig"][col]["TDAC"][row]=fe->GetPixelThreshold(col,row);
    }
  }

  ofstream fw(path);
  fw << setw(4) << config;
  fw.close();

}

void Handler::Connect(){

  //Connect to FELIX
  cout << "Handler::Connect Create the context" << endl;
  m_context = new netio::context(m_backend.c_str());
  m_context_thread = thread([&](){m_context->event_loop()->run_forever();});

  //TX
  for(auto it : m_fe_tx){
    if(m_enabled[it.first]==false){continue;}
    uint32_t tx_elink = it.second;
    if(m_tx.count(tx_elink)==0){
      cout << "Handler::Connect Connect to cmd elink: " << tx_elink << " at " << m_cmd_host[tx_elink] << ":" << m_cmd_port[tx_elink] << endl;
      m_tx[tx_elink]=new netio::low_latency_send_socket(m_context);
      m_tx[tx_elink]->connect(netio::endpoint(m_cmd_host[tx_elink],m_cmd_port[tx_elink]));
    }
    m_tx_fes[tx_elink].push_back(m_fe[it.first]);
  }

  //RX
  for(auto it : m_fe_rx){
    if(m_enabled[it.first]==false){continue;}
    uint32_t rx_elink = it.second;
    m_rx_fe[rx_elink] = m_fe[it.first];
    m_mutex[rx_elink].unlock();

    //The service frames comes with e_linkID + 1, so we subscribe to the +1 socket as well. 
    for (short Ind=0;Ind<=1;Ind++){
      
      m_rx[rx_elink + Ind] = new netio::low_latency_subscribe_socket(m_context, [&,rx_elink,Ind](netio::endpoint& ep, netio::message& msg){
	  if(m_verbose) cout << "Handler::Connect Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
	  m_mutex[rx_elink].lock();
	  vector<uint8_t> data = msg.data_copy();
	  //We should remove any potential header before decoding
	  FelixDataHeader hdr;
	  memcpy(&hdr,(const void*)&data[0], sizeof (hdr));
	  //Here the bool(Ind) changes the data-handler behaviour to handle register data which comes in 64bits
	  
	  m_rx_fe[rx_elink]->HandleData(&data[sizeof(hdr)],data.size()-sizeof(hdr), bool(Ind)); 
	  m_mutex[rx_elink].unlock();
	});

      cout << "Handler::Connect Subscribe to data elink: " << rx_elink + Ind << " at " << m_data_host[rx_elink + Ind] << ":" << m_data_port[rx_elink + Ind] << endl;
      m_rx[rx_elink + Ind]->subscribe(rx_elink + Ind, netio::endpoint(m_data_host[rx_elink + Ind], m_data_port[rx_elink + Ind]));

    }


  }
}


void Handler::Config(){

  cout<< "Handler::Config" << endl;

  for(auto fe : m_fes){
    //Reset the front-end in steps
    for (int step = 0; step <= 7; step++){
      fe->Reset(step);
      Send(fe);
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    //We should do this here -- we think this is disabling the clock
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_SYNC)->GetValue();
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_LIN_1)->GetValue();
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_LIN_2)->GetValue();
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_DIFF_1)->GetValue();
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_DIFF_2)->GetValue();
  
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_SYNC)->SetValue(0);
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_LIN_1)->SetValue(0);
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_LIN_2)->SetValue(0);
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_DIFF_1)->SetValue(0);
    fe->GetConfig()->GetField(Configuration::EN_CORE_COL_DIFF_2)->SetValue(0);

    cout << "Global configuration: " << fe->GetName() << endl;
    //Write the global registers one by one
    for(uint32_t address = Configuration::REGISTER0; address < Configuration::NUM_REGISTERS; address++){
      fe->WriteRegister(address);
      Send(fe);
      //Add this line to slow down the configuration
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    cout << "Pixel configuration: " << fe->GetName() << endl;
    //configure pixels in pairs
    for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
      for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
        fe->WritePixelPair(dcol,row);
        if((row+1)%3==0){Send(fe);}
      }
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    
  }

}

void Handler::ConfigurePixels(){
  cout<< "Handler::ConfigurePixels" << endl;
  for(auto fe : m_fes){
    ConfigurePixels(fe);
  }
}

void Handler::ConfigurePixels(FrontEnd * fe){
  cout << "Handler::ConfigurePixels " << fe->GetName() << endl;
  //configure pixels in pairs 
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      fe->WritePixelPair(dcol,row);
      if(row%50==0) Send(fe);
      std::this_thread::sleep_for(std::chrono::microseconds(20));
    }
    Send(fe);
  }
}

void Handler::PrepareTrigger(uint32_t cal_delay, uint32_t triggerMultiplier, bool bc1, bool bc2, bool bc3, bool bc4){
  cout << "Handler::PrepareTrigger: Preparing trigger with delay " << cal_delay << endl;
  for(auto it : m_tx_fes){
    if(m_verbose) cout << "Handler::PrepareTrigger: Composing trigger message for tx " << it.first << endl;
    FrontEnd * fe = it.second.at(0);
    fe->Trigger(cal_delay, triggerMultiplier, bc1, bc2, bc3, bc4);
    fe->Encode();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<fe->GetLength();i++){m_trigger_msgs[it.first].push_back(fe->GetBytes()[i]);}
    fe->Clear();
  }
}

void Handler::PrepareTrigger(uint32_t latency, vector<uint32_t> trigger){
  Encoder encoder;
  encoder.AddCommand(new Sync());
  encoder.AddCommand(new Sync());

  encoder.AddCommand(new Pulse(8,8));
  //encoder.AddCommand(new Sync());
  //encoder.AddCommand(new Sync());
  for(uint32_t i=0;i<32;i++)  encoder.AddCommand(new Noop());
  
  //Inject
  if (m_scan_fe & SyncFE) encoder.AddCommand(new ECR());
  encoder.AddCommand(new BCR());
  for(uint32_t i=0;i<10;i++)  encoder.AddCommand(new Noop());
  
  encoder.AddCommand(new Noop); // Call words are 6Bytes, rounding up to 8 with noop.   
  encoder.AddCommand(new Cal(8,0,0,1,0,0));
  //Since each Noop spans 4 BCs we add a whole number of Noops                                             
  cout << "NUMBER OF Noop is: " << (int)(latency/4-3) << endl;
  for(uint32_t i=0;i<(latency/4-3);i++){
    encoder.AddCommand(new Noop());
  }

  if(trigger.size()%4!=0){
    std::cout<<"Prepare Trigger: Trigger size is not a multiple of 4. Please add up 0's at the end of trigger to make sure trigger size is a multiplier of 4. "<<std::endl;
    exit(0);
  }

  for(uint32_t i=0;i<trigger.size()/4;i++){
    //std::cout<<"Trigger :"<<trigger[i]<<trigger[i+1]<<trigger[i+2]<<trigger[i+3]<<std::endl;
    encoder.AddCommand(new RD53A::Trigger(trigger[i], trigger[i+1], trigger[i+2], trigger[i+3], i+5));
  }
  //arm the injection
  encoder.AddCommand(new Noop); // Call words are 6Bytes, rounding up to 8 with noop.   
  encoder.AddCommand(new Cal(8,1,0,0,0,0));

  //if(m_scan_fe&SyncFE){
  //encoder.AddCommand(new Pulse());
  //}

  //Prepare the trigger byte streams
  PrepareTrigger(&encoder);
}

void Handler::PrepareTrigger(Encoder * encoder){
  cout << "Handler::PrepareTrigger" << endl;
  for(auto it : m_tx_fes){
    if(m_verbose) cout << "Handler::PrepareTrigger: Composing trigger message for tx " << it.first << endl;
    encoder->Encode();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<encoder->GetLength();i++){m_trigger_msgs[it.first].push_back(encoder->GetBytes()[i]);}
  }
}

void Handler::Trigger(){

  for(auto it : m_tx){
    if(m_verbose) cout << "Handler::Trigger: Trigger! for tx " << it.first << endl;

    FelixCmdHeader hdr;
    hdr.elink=it.first;
    hdr.length=m_trigger_msgs[it.first].size();
    netio::message msg;
    msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
    msg.add_fragment(m_trigger_msgs[it.first].data(), m_trigger_msgs[it.first].size());
    //send message
    if (it.second->is_closed()){
      std::cout<<"Problem occured with the felixcore ... consider , killing the run"<<std::endl;
      m_exit=true;
      //exit(0);
      return;
    }

    it.second->send(msg);

    if(m_verbose){
      cout << "Handler::Trigger: Message: 0x" << hex;
      for(uint32_t i=0;i<msg.size();i++){
        cout << setw(2) << setfill('0') << ((uint32_t) msg[i]);
      }
      cout << dec << endl;
    }

    //std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
}

void Handler::SendPulse(){
  Encoder encoder;
  //encoder.AddCommand(new Pulse(8,8));
  encoder.AddCommand(new ECR());
  encoder.Encode();
  for(auto it : m_tx){
    FelixCmdHeader hdr;
    hdr.elink=it.first;
    hdr.length=encoder.GetLength();
    netio::message msg;
    msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
    msg.add_fragment(encoder.GetBytes(), encoder.GetLength());
    //send message
    if (it.second->is_closed()){
      std::cout<<"Problem occured with the felixcore, killing the run"<<std::endl;
       m_exit=true;
       return;
      //exit(0);
    }
    it.second->send(msg);
    if(m_verbose){
      cout << "Handler::Trigger: Message: 0x" << hex;
      for(uint32_t i=0;i<msg.size();i++){
        cout << setw(2) << setfill('0') << ((uint32_t) msg[i]);
      }
      cout << dec << endl;
    }
  }
}

void Handler::Send(FrontEnd * fe){
  //process the commands from the front-end
  fe->Encode();
  //quick return
  if(fe->GetLength()==0){return;}
  if(!m_enabled[fe->GetName()]){return;}
  //figure out the tx_elink
  uint32_t tx_elink=m_fe_tx[fe->GetName()];
  //copy the message
  vector<uint8_t> payload(fe->GetLength());
  for(uint32_t i=0;i<fe->GetLength();i++){payload[i]=fe->GetBytes()[i];};
  //build message
  FelixCmdHeader hdr;
  hdr.elink=tx_elink;
  hdr.length=fe->GetLength();
  netio::message msg;
  msg.add_fragment((uint8_t*)&hdr,sizeof(hdr));
  //std::cout<<"Message with size :"<<fe->GetLength()<<std::endl;
  //msg.add_fragment(fe->GetBytes(), fe->GetLength());
  msg.add_fragment(payload.data(), payload.size());
  //send message

  if (m_tx[tx_elink]->is_closed()){
    std::cout<<"Problem occured with the felixcore, killing the run"<<std::endl;
    //exit(0);
    m_exit=true;
    return;
  }
  m_tx[tx_elink]->send(msg); // EJS: this one hangs, probably because there's no active listener
  //std::this_thread::sleep_for(std::chrono::milliseconds(1));
  fe->Clear();

}

void Handler::Disconnect(){

  if (m_exit) return;

  sleep(3); // 2020-11-06: EJS hack to avoid crash at the end of Dig scan (waiting for all data buffers to empty)

  cout << "Handler::Disconnect Stop event loop" << endl;
  m_context->event_loop()->stop();

  for(auto it : m_tx){
    cout << "Handler::Disconnect Disconnect from cmd elink: " << it.first << " at " << m_cmd_host[it.first] << ":" << m_cmd_port[it.first] << endl;
    it.second->disconnect();
    delete it.second;
  }
  m_tx.clear();

  for(auto it : m_rx){
    cout << "Handler::Disconnect Disconnect from data elink: " << it.first << " at " << m_data_host[it.first] << ":" << m_data_port[it.first] << endl;
    it.second->unsubscribe(it.first, netio::endpoint(m_data_host[it.first], m_data_port[it.first]));
    delete it.second;
  }
  m_rx.clear();


  cout << "Handler::Disconnect Join context thread" << endl;
  m_context_thread.join();

  cout << "Handler::Disconnect Delete the context" << endl;
  delete m_context;

}

void Handler::PreRun(){}

void Handler::SaveConfig(std::string configuration){
  if(!m_output) return;

  if(configuration=="tuned"){
    system("mkdir -p ./tuned");
  }

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}
    ostringstream os;
    if(configuration!="tuned"){
      os << m_fullOutPath << "/" << m_configs[fe->GetName()] << "_" << configuration;
    }else{
      string path = m_configs[fe->GetName()].substr(0, m_configs[fe->GetName()].find_last_of("/"));
      if(path != m_configs[fe->GetName()]){
	system(("mkdir -p ./tuned/"+path).c_str());
      }
      os << "./tuned/" << m_configs[fe->GetName()];
    }
    if (m_configs[fe->GetName()].find(".json")==string::npos)
      os<<".json";
    SaveFE(fe,os.str());
  }
}

void Handler::Run(){
  cout << __PRETTY_FUNCTION__ << "FIXME: Handler::Run is supposed to be extended" << endl;
}

void Handler::Analysis(){}

void Handler::Save(){
  if(!m_output) return;


  SaveConfig("tuned");

  time_t endTime;
  time(&endTime);
  m_logFile << "\"endTime\": " << ctime(&endTime) << endl;
  m_logFile.close();
  cout << "Save log file: " << m_fullOutPath + "/logFile.txt" << endl;
  //Lines below not useful with real chip
  std::ifstream logEmulator("logFileEmulator.txt");
  if(logEmulator){
  ostringstream cmdmv;
  cmdmv << "mv ./logFileEmulator.txt " << m_fullOutPath << "/";
  system(cmdmv.str().c_str());
  }
  logEmulator.close();
  std::ifstream trueThrFile("trueThreshold.txt");
  if(trueThrFile) {
    trueThrFile.close();
    ostringstream cmdtmp;
    cmdtmp << "mv ./trueThreshold.txt " << m_fullOutPath << "/";
    system(cmdtmp.str().c_str());
  }
  trueThrFile.close();
  //Lines above not useful with real chip

  cout << "Save ROOT file: " << m_rootfile->GetName() << endl;
  m_rootfile->Close();
    
}

Masker * Handler::GetMasker(){
  return m_masker;
}

bool ValerioStar=false;
void Handler::ConfigMask(bool enable){

  for(auto fe : GetFEs()){
    for(auto cc : m_masker->GetCoreColumns()){
      fe->EnableCoreColumn(cc,enable);
      fe->EnableCalCoreColumn(cc,enable);
    }
    for(auto dc : m_masker->GetDoubleColumns()){
      fe->EnableCalDoubleColumn(dc,enable);
    }
    fe->ProcessCommands();
    Send(fe);
    unsigned Counter=0;
    for(auto pixel : m_masker->GetPixels()){
      Counter++;
      if (ValerioStar==false) fe->SetPixelInject(pixel.first,pixel.second,enable);
      else {
	if (pixel.second>0)                  fe->SetPixelInject(pixel.first,pixel.second-1,enable);
	if (pixel.second<Matrix::NUM_ROWS-1) fe->SetPixelInject(pixel.first,pixel.second+1,enable);
	if (pixel.first>0)                   fe->SetPixelInject(pixel.first-1,pixel.second,enable);
	if (pixel.first<Matrix::NUM_COLS-1)  fe->SetPixelInject(pixel.first+1,pixel.second,enable);
      }
      
      fe->SetPixelEnable(pixel.first,pixel.second,enable);
      if(Counter%10==0){
	fe->ProcessCommands();
	Send(fe);
	std::this_thread::sleep_for(std::chrono::microseconds(100));
	
      }
    }
    fe->ProcessCommands();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  
}

void Handler::UnconfigMask(){
  ConfigMask(false);
}


std::string Handler::GetConfig(std::string configuration){
  //if(!m_output) return ;

  json config;
  std::string strConfig;
  std::string name;

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}

    name = "RD53A_"+fe->GetName();
    //config["RD53A"]["name"]=fe->GetName() + configuration;
    config[name]["Parameter"]["ChipId"]=fe->GetChipID();
    config[name]["GlobalConfig"]=fe->GetGlobalConfig();

    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
          config[name]["PixelConfig"][col]["Enable"][row]=(uint32_t)fe->GetPixelEnable(col,row);
          config[name]["PixelConfig"][col]["Hitbus"][row]=(uint32_t)fe->GetPixelHitbus(col,row);
          config[name]["PixelConfig"][col]["InjEn"][row]=(uint32_t)fe->GetPixelInject(col,row);
          config[name]["PixelConfig"][col]["TDAC"][row]=fe->GetPixelThreshold(col,row);
        }
      }
      std::string str = config.dump();
      //std::string str;
      //config.dump(str);
      strConfig.append(str);
    }
  return strConfig;
}

std::string Handler::GetResults(){
  //if(!m_output) return;

  time_t endTime;
  time(&endTime);
  m_logFile << "\"endTime\": " << ctime(&endTime) << endl;
  m_logFile.close();
  cout << "Save log file: " << m_fullOutPath + "/logFile.txt" << endl;

  std::string strResults = Tools::RootToJson(m_rootfile);

  cout << "Save ROOT file: " << m_rootfile->GetName() << endl;
  m_rootfile->Close();
    
  return strResults;
}

float Handler::GetElapsedTime(string frontEndName){ //AAA
  return m_elapsedTime[frontEndName];
}

void Handler::SetElapsedTime(string frontEndName, float elapsedTime){ //AAA
  m_elapsedTime[frontEndName] = elapsedTime;
}
