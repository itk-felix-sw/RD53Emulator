#include <RD53Emulator/Handler.h>
#include <RD53Emulator/FastDigitalScan.h>
#include <RD53Emulator/DigitalScan.h>
#include <RD53Emulator/AnalogScan.h>
#include <RD53Emulator/ThresholdScan.h>
#include <RD53Emulator/TestScan.h>
#include <RD53Emulator/SensorScan.h>
#include <RD53Emulator/ToTScan.h>
#include <RD53Emulator/MaskCheck.h>
#include <RD53Emulator/GlobalThresholdTune.h>
#include <RD53Emulator/PreampTune.h>
#include <RD53Emulator/PixelThresholdTune.h>
#include <RD53Emulator/NoiseScan.h>
#include <RD53Emulator/ReadRegisterScan.h>
#include <RD53Emulator/SourceScan.h>
#include <RD53Emulator/ExternalTriggerScan.h>
#include <RD53Emulator/ConfigScan.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>

using namespace std;
using namespace RD53A;

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_rd53a     #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names");
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default network.json");
  CmdArgInt  cCharge(   'q',"charge","electrons","Injection charge");
  CmdArgInt  cMaskOpt(  'm',"maskfile","Type","If 0 creates mask, if 1 apply mask");
  CmdArgBool cRetune(   'r',"retune","enable retune mode");
  CmdArgStr  cOutPath(  'o',"outpath","path","output path. Default $ITK_DATA_PATH");
  CmdArgInt  cToT(      't',"ToT","bc","target ToT");
  CmdArgInt  cThreshold('a',"threshold","electrons","threshold target");
  CmdArgInt  cWait(     'w',"wait","seconds","time to wait between config and run. Default: 3");
  CmdArgStr  cFrontEnd( 'f',"frontend","type","sync, lin, diff, all. Some scans accept frontend range");
  CmdArgBool cStoreHits('H',"hits","turn on the store hits flag");
  CmdArgInt  cLatency(  'L',"latency","integer","latency");
  
  CmdLine cmdl(*argv,&cScan,&cFrontEnd,&cConfs,&cVerbose,&cBackend,&cInterface,&cNetFile,&cCharge,&cMaskOpt,&cRetune,&cOutPath,&cToT,&cThreshold,&cWait,&cStoreHits,&cLatency,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  std::string commandLineStr="";
  for (int i=1;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));

  string scan(cScan);
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"enp24s0f0");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity-emu-rd53a.json");

  Handler * handler = 0;
  if(scan.find("FastDigitalScan")!=string::npos)   {handler=new FastDigitalScan();}
  else if(scan.find("DigitalScan")!=string::npos)  {handler=new DigitalScan();}
  else if(scan.find("Ana")!=string::npos)          {handler=new AnalogScan();}
  else if(scan.find("ThrScan")!=string::npos)      {handler=new ThresholdScan();}
  else if(scan.find("Test")!=string::npos)         {handler=new TestScan();}
  else if(scan.find("Sensor")!=string::npos)       {handler=new SensorScan();}
  else if(scan.find("Config")!=string::npos)       {handler=new ConfigScan();}
  else if(scan.find("ToT")!=string::npos)          {handler=new ToTScan();
    if(!(cCharge.flags()&CmdArg::GIVEN)){cout << "Injection charge required (-q option)." << endl; return 0;
    }
  }
  else if(scan.find("PreampSyn")!=string::npos or 
	  scan.find("PreampLin")!=string::npos or 
	  scan.find("PreampDiff")!=string::npos)   {handler=new PreampTune(); 
      if(!(cToT.flags()&CmdArg::GIVEN)){cout << "ToT target required (-t option)." << endl; return 0;}          
      if(!(cCharge.flags()&CmdArg::GIVEN)){cout << "Injection charge required (-q option)." << endl; return 0;} 
  }
  else if(scan.find("MaskCheck")!=string::npos)    {handler=new MaskCheck();}
  else if(scan.find("Glob")!=string::npos )     {handler=new GlobalThresholdTune();
    if(!(cThreshold.flags()&CmdArg::GIVEN)){cout << "Threshold target required (-a option)." << endl; return 0;}
  }
  else if(scan.find("PixLin")!=string::npos or
	  scan.find("PixDiff")!=string::npos)      {handler=new PixelThresholdTune();
    if(!(cThreshold.flags()&CmdArg::GIVEN)){cout << "Threshold target required (-a option)." << endl; return 0;}
  }
  else if(scan.find("Noise")!=string::npos)        {handler=new NoiseScan();}
  else if(scan.find("Source")!=string::npos)       {handler=new SourceScan();}
  else if(scan.find("ReadRegister")!=string::npos)       {handler=new ReadRegisterScan();}
  else if(scan.find("ExternalTrigger")!=string::npos)       {handler=new ExternalTriggerScan();}

  else{
    cout << "Scan Manager: Scan not recognized: " << scan << endl;
    return 0;
  }

  handler->SetVerbose(cVerbose);
  handler->SetContext(backend);
  handler->SetInterface(interface);
  handler->SetMapping(netfile,(cConfs.count()==0));
  handler->SetRetune(cRetune);
  handler->SetScan(scan);
  handler->SetCommandLine(commandLineStr);
  if(cLatency.flags()&CmdArg::GIVEN && scan.find("ExternalTrigger")!=string::npos ) {
    handler->SetLatency(cLatency);
  }

  for(uint32_t i=0;i<cConfs.count();i++){
    cout << "Scan Manager: Adding front end #" << i << " " << string(cConfs[i]) << endl;
    handler->AddFE(string(cConfs[i]));
  }
  
  cout << "Scan Manager: Configure the scan" << endl;
  if(cOutPath.flags()&CmdArg::GIVEN){
    handler->SetOutPath(string(cOutPath));
  }
  if(cCharge.flags()&CmdArg::GIVEN){
    handler->SetCharge(cCharge);
  }
  if(cToT.flags()&CmdArg::GIVEN){
    handler->SetToT(cToT);
  }
  if(cThreshold.flags()&CmdArg::GIVEN){
    handler->SetThreshold(cThreshold);
  }
  if(cMaskOpt.flags()&CmdArg::GIVEN){
     handler->SetMaskOpt(cMaskOpt);
  }
  if(cFrontEnd.flags()&CmdArg::GIVEN){
    handler->SetScanFE(string(cFrontEnd));
  }
  if(cStoreHits.flags()&CmdArg::GIVEN){
    handler->StoreHits(cStoreHits);
  }

  cout << "Scan Manager: Connect" << endl;
  handler->Connect();

  cout << "Scan Manager: InitRun" << endl;
  handler->InitRun();

  cout << "Scan Manager: Config" << endl;
  handler->Config();

  cout << "Scan Manager: PreRun" << endl;
  handler->PreRun();
  
  cout << "Scan Manager: Save configuration before the run" << endl;
  handler->SaveConfig("before");

  cout << "Scan Manager: Wait for run to start: " << endl;
  sleep(cWait);

  cout << "Scan Manager: Run" << endl;
  handler->Run();

  cout << "Scan Manager: Analysis" << endl;
  handler->Analysis();

  cout << "Scan Manager: Disconnect" << endl;
  handler->Disconnect();

  cout << "Scan Manager: Save configuration after the run" << endl;
  handler->SaveConfig("after");

  cout << "Scan Manager: Save the tuned files" << endl;
  if(!cMaskOpt){
    handler->SaveConfig("tuned");
  }

  cout << "Scan Manager: Save the histograms" << endl;
  handler->Save();

  cout << "Scan Manager: Cleaning the house" << endl;
  delete handler;

  cout << "Scan Manager: Have a nice day" << endl;
  return 0;
}
