#include "RD53Emulator/ConfigScan.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53A;

ConfigScan::ConfigScan(){}

ConfigScan::~ConfigScan(){}

void ConfigScan::PreRun(){
  cout << "The following FrontEnds should be configured:" << endl;
  for(auto fe : GetFEs()){
    cout << fe->GetName() << endl;
    registerMap = fe->GetConfig()->GetRegisters();
    for(auto reg : registerMap){
      cout << "Register: " << reg.first << " -> value: " << reg.second << endl;
    }
 }

}
void ConfigScan::Run(){

  cout << "ConfigScan::Run Nothing to do" << endl;

}

void ConfigScan::Analysis(){
  cout << "ConfigScan::Analysis Nothing to do" << endl;
}                                                                                                                                        

