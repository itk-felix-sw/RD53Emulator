#include "RD53Emulator/Masker.h"
#include "RD53Emulator/FrontEnd.h"
#include <set>
#include <algorithm>

using namespace std;
using namespace RD53A;

Masker::Masker(){
  m_px0=0;
  m_pxN=400;
  m_py0=0;
  m_pyN=192;
  m_size_x=4;
  m_size_y=1;
  m_gap=8;
  m_freq=1;
  m_cur_step=0;
  m_num_steps=0;
  m_verbose=false;
}

Masker::~Masker(){
  m_double_cols.clear();
  m_core_cols.clear();
  m_pixels.clear();
}

void Masker::SetVerbose(bool verbose){
  m_verbose=verbose;
}

void Masker::SetRange(uint32_t px0, uint32_t py0, uint32_t pxN, uint32_t pyN){
  m_px0=px0;
  m_pxN=pxN;
  m_py0=py0;
  m_pyN=pyN;
}

void Masker::SetShape(uint32_t size_x, uint32_t size_y){
  if(size_x==0)  size_x=1;
  if(size_x>400) size_x=400;
  if(size_y==0)  size_y=1;
  if(size_y>192) size_y=192;
  m_size_x = size_x;
  m_size_y = size_y;
}

void Masker::SetFrequency(uint32_t freq){
  m_freq=freq;
}

void Masker::SetGap(uint32_t gap){
  m_gap=gap;
}

void Masker::Build(){
  if(m_verbose){cout << "Masker::Build" << endl;}
  m_num_steps_y = (m_pyN-m_py0)/(float)m_size_y;
  m_num_steps_x = (m_pxN-m_px0)/(float)m_size_x;
  if(m_num_steps_y<1) m_num_steps_y=1;
  if(m_num_steps_x<1) m_num_steps_x=1;
  m_num_steps=m_num_steps_y*m_num_steps_x/m_freq;
  m_cur_step=0;
}

uint32_t Masker::GetNumSteps(){
  return m_num_steps;
}

void Masker::GetStep(uint32_t step){
  if(m_verbose) {cout << "Masker::GetStep" << endl;}
  m_pixels.clear();
  m_double_cols.clear();
  m_core_cols.clear();
  set<uint32_t> ccs;
  set<uint32_t> dcs;
  for(uint32_t f=0;f<m_freq;f++){
    uint32_t step2=step+f*m_num_steps;
    uint32_t py1 = m_size_y*(step2%m_num_steps_y)+m_py0;
    uint32_t px1 = m_size_x*(step2/m_num_steps_y)+m_px0;
    if(m_verbose) cout << "Masker::GetStep start at px1:" << px1 << ", py1:" << py1 << endl;
    for(uint32_t dy=0; dy<m_size_y; dy++){
      for(uint32_t dx=0; dx<m_size_x; dx++){
	if(m_verbose) cout << "Masker::GetStep add pixel:" << (px1+dx) << ", " << (py1+dy) << endl;
	m_pixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx,py1+dy));
	dcs.insert((px1+dx)/2);
	ccs.insert((px1+dx)/8);
      }
    }
  }
  for(auto cc : ccs){
    if(m_verbose) cout << "Masker::GetStep add cc:" << cc << endl;
    m_core_cols.push_back(cc);
  }
  for(auto dc : dcs){
    if(m_verbose) cout << "Masker::GetStep add dc:" << dc << endl;
    m_double_cols.push_back(dc);
  }
}

void Masker::GetNextStep(){
  if(m_cur_step>=m_num_steps) return;
  m_cur_step++;
  GetStep(m_cur_step);
}

vector<uint32_t> & Masker::GetCoreColumns(){
  return m_core_cols;
}

vector<uint32_t> & Masker::GetDoubleColumns(){
  return m_double_cols;
}

vector<pair<uint32_t,uint32_t> > & Masker::GetPixels(){
  return m_pixels;
}

void Masker::MaskFE(FrontEnd*  fe){
  for(auto cc : GetCoreColumns()){
    fe->EnableCoreColumn(cc,true);
    fe->EnableCalCoreColumn(cc,true);
  }
  for(auto dc : GetDoubleColumns()){
    fe->EnableCalDoubleColumn(dc,true);
  }
  for(auto pixel : GetPixels()){
    fe->SetPixelEnable(pixel.first, pixel.second,true);
    fe->SetPixelInject(pixel.first, pixel.second,true);
  }
  if(m_verbose){cout << "Masker::MaskFE ProcessCommands" << endl;}
  fe->ProcessCommands();
}

void Masker::UnMaskFE(FrontEnd* fe){
  for(auto cc : GetCoreColumns()){
    fe->EnableCoreColumn(cc,false);
    fe->EnableCalCoreColumn(cc,false);
  }
  for(auto dc : GetDoubleColumns()){
    fe->EnableCalDoubleColumn(dc,false);
  }
  for(auto pixel : GetPixels()){
    fe->SetPixelEnable(pixel.first, pixel.second,false);
    fe->SetPixelInject(pixel.first, pixel.second,false);
  }
  if(m_verbose){cout << "Masker::UnMaskFE ProcessCommands" << endl;}
  fe->ProcessCommands();
}

bool Masker::Contains(uint32_t col, uint32_t row){
  pair<uint32_t,uint32_t> pp(col,row);
  return (std::find(m_pixels.begin(), m_pixels.end(), pp) != m_pixels.end());
}
